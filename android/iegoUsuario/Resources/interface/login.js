function login(){
	 var window,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	 
	window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg'});
    window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];

 	lblUser = Titanium.UI.createLabel({text:'Usuario',center:0,top:'20%',color:'white'});
    txtUser = Titanium.UI.createTextField({hintText:'mail@ejemplo.com',color:'#ffffff',opacity:0.3,center:0,top:'26%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
    lblpass= Titanium.UI.createLabel({text:'Contraseña',center:0,top:'36%',color:'white'});
    txtPass = Titanium.UI.createTextField({passwordMask:true,hintText:'********',color:'#ffffff',opacity:0.3,center:0,top:'42%',width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
    btnlogin = Titanium.UI.createButton({title:'Iniciar Sesión',color:'white',borderRadius:30,borderColor:'white',top:'55%',width:'85%',height:60,backgroundColor:'#757575'});
    lblreset = Titanium.UI.createLabel({text:'Recuperar Contraseña',color:'#c2c2c2',top:'70%',center:0});
    loginReq = Titanium.Network.createHTTPClient();
    resetReq = Titanium.Network.createHTTPClient();
    
    txtUser.addEventListener('focus',function(){
			txtUser.value = '';
			txtUser.opacity=1;
    	});
    	txtPass.addEventListener('focus',function(){
        	txtPass.value = '';
        	txtPass.opacity=1;
        	
    	});
    	
    	btnlogin.addEventListener('click',function(e){
            if (txtUser.value != '' && txtPass.value != ''){
                    loginReq.open("POST","http://iego.com.mx/webservices/loginResquest.php");
                    params = {
                        username: txtUser.value,
                        password: Ti.Utils.md5HexDigest(txtPass.value),
                        identificador:Titanium.Platform.id
                    };
                    loginReq.send(params);
            }else{
                    alert("Usuario y password son requeridos");
			}
    });
    loginReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	
            	var rs= response.razon;
            	if(rs==null){
            		rsS='';
            	}
            	else{var rsS= rs.substring(0,20);}
	  			//alert(Titanium.Platform.id);

            	
            	Ti.App.Properties.setString("identificador",Titanium.Platform.id);
                Ti.App.Properties.setInt("id_usuario", response.id_usuario);
                Ti.App.Properties.setInt("id_cliente", response.id_cliente);
                Ti.App.Properties.setString("usuario", response.usuario);
                Ti.App.Properties.setString("nombre", response.nombre);
                 Ti.App.Properties.setString("apellido", response.apellido);
                 Ti.App.Properties.setString("email", response.email_factura);
                Ti.App.Properties.setString("telefono", response.telefono);
                Ti.App.Properties.setString("rfc", response.rfc);
                Ti.App.Properties.setString("razon",rsS);
                 Ti.App.Properties.setString("id_biling",response.id_biling);
                	  mapa = require('/interface/mapa');
                  new mapa().open({modal:true});
                  window.close();
            }else{
                    alert(response.message);
             }
    	};
	lblreset.addEventListener('click', function(){
        	if (txtUser.value == ''){
                alert('Se requiere su usuario para restaurar la contraseña');
            }else{
              Ti.Platform.openURL("https://iego.com.mx/landing/restablecer.php");
            }
    	});
	resetReq.onload = function(){
            	json = this.responseText;
            	response = JSON.parse(json);
            	if (response.mail == true){
                	alert('Se le ha enviado un correo para restaurar su contraseña');
            	}else{
            	    alert(response.message);    
            	}
    	};
	resetReq.onerror = function(event){
            		alert('Error de conexion: ' + JSON.stringify(event));
    	};
    	
    	window.addEventListener("open", function(evtop) { 
        		var theActionBar = window.activity.actionBar; 
        		if (theActionBar != undefined) {
                    theActionBar.backgroundColor = '#373136';
        			 theActionBar.title = "Inicio de sesion";
         		}
            window.activity.onCreateOptionsMenu = function(emenu) { 
            	menu = emenu.menu;
                itemSearch = menu.add({
                title:'Registro',
                showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
            });
                itemMapa = menu.add({
                title:'Mapa',
                showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
            }); 
                itemSearch.addEventListener('click', function(){        
                menu1 = require('interface/registro');
                new menu1().open();
                window.close();
            });
                itemMapa.addEventListener('click', function(){      
                mapa = require('interface/mapa');
                new mapa().open();
                window.close();
            });
           };
    });
    
    window.add(lblUser);
    	window.add(txtUser);
    	window.add(lblpass);
    	window.add(txtPass);
   	window.add(lblreset);
    	window.add(btnlogin);
    	
    	return window;
};
module.exports = login;