function factura(){
	var window,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	
	
	window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	lblUser = Titanium.UI.createLabel({text:'RFC',center:0,top:'5%',color:'white'});
	txtUser = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'',center:0,top:'9%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblpass= Titanium.UI.createLabel({text:'Razón Social',center:0,top:'19%',color:'white'});
	txtPass = Titanium.UI.createTextField({hintText:'',center:0,top:'23%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblDFiscal= Titanium.UI.createLabel({text:'Dirección fiscal',center:0,top:'33%',color:'white'});
	txtDFiscal = Titanium.UI.createTextField({hintText:'',center:0,top:'37%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblCodigoP= Titanium.UI.createLabel({text:'Código postal',center:0,top:'47%',color:'white'});
	txtCodigoP= Titanium.UI.createTextField({hintText:'',center:0,top:'51%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblCorreo= Titanium.UI.createLabel({text:'E-mail',center:0,top:'61%',color:'white'});
	txtCorreo= Titanium.UI.createTextField({hintText:'',center:0,top:'65%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	
	btnlogin = Titanium.UI.createButton({title:'Registrarme',color:'white',borderRadius:30,borderColor:'white',top:'85%',width:'85%',height:60,backgroundColor:'#a00a1c'});
	chkFactura = Ti.UI.createSwitch({value:false,top:'78%',onTintColor:'#62D337',tintColor:'#D90019',left:'10%'});
	lblFactura = Titanium.UI.createLabel({text:'Acepto términos y condiciones',right:'10%',top:'79%',color:'white'});
	registerReq = Titanium.Network.createHTTPClient();
	
	lblFactura.addEventListener('click',function(){
   	Ti.Platform.openURL("http://iego.com.mx/terminosycondiciones/index.html");
   });
	
	function checkemail(emailAddress){
    		var str = emailAddress;
    		var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    		if (filter.test(str)){
        		testresults = true;
    		}else{
        		testresults = false;
    		}
    		return (testresults);
	};
	
		txtUser.addEventListener('focus',function(){
			txtUser.value = '';
		});
	
		txtPass.addEventListener('focus',function(){
			txtPass.value = '';
		});
	
			window.addEventListener('foo', function(e){ 
				
				registerReq.onload = function(){
					json = this.responseText;
    				response = JSON.parse(json);
    			if (response.message == "Insert failed" || response.message == 'El usuario o email ya existen'){
        			
        			alert(response.message);
    			}else{
    			
    				  var rs= txtPass.value;
	  				  var rsS= rs.substring(0,20);
	  					Ti.App.Properties.setString("identificador",Titanium.Platform.id);
    					Ti.App.Properties.setString("usuario", Ti.App.Properties.getString("usuario1"));
    					Ti.App.Properties.setString("email", Ti.App.Properties.getString("email1"));
    					Ti.App.Properties.setString("telefono", Ti.App.Properties.getString("telefono1"));
    					Ti.App.Properties.setString("rfc", txtUser.value);
    					Ti.App.Properties.setString("razon", rsS);
    					Ti.App.Properties.setInt("id_usuario", response.id_usuario);
    					Ti.App.Properties.setString("nombre",Ti.App.Properties.getString("nombre1"));
    					Ti.App.Properties.setString("apellido", Ti.App.Properties.getString("apellido1"));
    					paypal = require('/interface/paypal');
						new paypal().open({modal:true});
						window.close();
    			
    			}
		};
	
		btnlogin.addEventListener('click',function(){
			if (txtUser.value != '' && txtPass.value != '' && txtDFiscal.value != '' && txtCodigoP.value != '' && txtCorreo.value != ''){
				   
				   if (!checkemail(txtCorreo.value)){
                        alert("Introduce un email valido");
                    }else{
                    	if (chkFactura.value==true){
                    		registerReq.open("POST","http://iego.com.mx/webservices/registerResquest.php");
                			params = {
                    		username: e.username,
                    		password: Ti.Utils.md5HexDigest(e.password),
                    		correo: e.correo,
                    		rfc:txtUser.value,
                    		razon_social:txtPass.value,
                    		telefono:e.telefono,
                    	    nombre:e.nombre,
                            apellido_p:e.apellido_p,
                            apellido_m:e.apellido_m,
                            direccion_fiscal:txtDFiscal.value,
                            email_factura:txtCorreo.value,
                            cp:txtCodigoP.value,
                            identificador:Titanium.Platform.id,
                           
                           	};
                		registerReq.send(params);
                		}else{
                			alert('Debe aceptar los terminos y condiciones');
                		}
				 }		
			}else{
				alert('Todos los campos son obligatorios');
			 }
		});
	});
		
		window.addEventListener("open", function(evtop) { 
			var theActionBar = window.activity.actionBar; 
			if (theActionBar != undefined) {
		
				theActionBar.backgroundColor = '#373136';
        		 theActionBar.title = "Facturación";
         	}
			
		window.activity.onCreateOptionsMenu = function(emenu) { 
				menu = emenu.menu;
			   	itemSearch = menu.add({
				title:'Inicio de sesion',
				showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});
				itemMapa = menu.add({
				title:'Mapa',
				showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});	
	         	itemSearch.addEventListener('click', function(){		
				menu1 = require('interface/login');
				new menu1().open();
				window.close();
				});
				itemMapa.addEventListener('click', function(){		
				mapa = require('interface/mapa');
				new mapa().open();
				window.close();
				});
		};
	});
		
		scrollView.add(lblUser);
		scrollView.add(txtUser);
		scrollView.add(lblpass);
		scrollView.add(txtPass);
		scrollView.add(lblDFiscal);
		scrollView.add(txtDFiscal);
		scrollView.add(lblCodigoP);
		scrollView.add(txtCodigoP);
		scrollView.add(lblCorreo);
		scrollView.add(txtCorreo);
		scrollView.add(chkFactura);
		scrollView.add(lblFactura);
		scrollView.add(btnlogin);
		window.add(scrollView);
	
		return window;
};
module.exports = factura;