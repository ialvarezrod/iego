	function registro(){
	var window,tc,lblUser,lblCorreo,lblPass,lblPass2,txtUser,txtCorreo,txtPass,txtPass2,chkFactura,chkTerminos,btnRegistrar,lblTermino,lblFactura,scrollView,registerReq;
	var json,response,alertDialog;
	
	
	window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});

	lblUser = Titanium.UI.createLabel({text:'Usuario',center:0,top:'5%',color:'white'});
	txtUser = Titanium.UI.createTextField({color:'#c2c2c2',center:0,top:'10%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent'});
	
	//vista nombre y apellido
	vista1 = Ti.UI.createView({backgroundColor:'transparent',height:'70%',zIndex:10,top:0});
	vista2 = Ti.UI.createView({backgroundColor:'transparent',height:'30%',zIndex:10,top:'70%'});
	
	scrollView2 = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false,overScrollMode:Titanium.UI.Android.OVER_SCROLL_ALWAYS});
	lblUsuario = Titanium.UI.createLabel({text:'Usuario',center:0,top:'5%',color:'white'});
	txtUsuario = Titanium.UI.createTextField({color:'#c2c2c2',center:0,top:'10%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent'});
	
	lblnombres = Titanium.UI.createLabel({text:'Nombre(s)',center:0,top:'5%',color:'white'});
	txtnombres = Titanium.UI.createTextField({opacity:0.2,hintText:'Escriba su(s) nombre(s)',color:'#ffffff',center:0,top:'10%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblapellidoP = Titanium.UI.createLabel({text:'Apellido Paterno',center:0,top:'21%',color:'white'});
	txtapellidoP = Titanium.UI.createTextField({opacity:0.2,hintText:'Escriba apellido paterno',color:'#ffffff',center:0,top:'26%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblapellidoM = Titanium.UI.createLabel({text:'Apellido Materno',center:0,top:'37%',color:'white'});
	txtapellidoM = Titanium.UI.createTextField({opacity:0.2,hintText:'Escriba apellido materno',color:'#ffffff',center:0,top:'42%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblCorreo = Titanium.UI.createLabel({text:'Correo electrónico',center:0,top:'53%',color:'white'});
	txtCorreo = Titanium.UI.createTextField({hintText:'mail@ejemplo.com',color:'#ffffff',opacity:0.2,center:0,top:'58%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	lblPass = Titanium.UI.createLabel({text:'Contraseña',center:0,top:'69%',color:'white'});
	txtPass = Titanium.UI.createTextField({hintText:'*************',passwordMask:true,color:'#ffffff',opacity:0.2,center:0,top:'74%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});	
	lblPass2 = Titanium.UI.createLabel({text:'Teléfono',center:0,top:'85%',color:'white'});
	txtPass2 = Titanium.UI.createTextField({hintText:'0000000000',color:'#ffffff',opacity:0.2,center:0,top:'90%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
	chkTerminos = Ti.UI.createSwitch({value:false,top:'5%',onTintColor:'#62D337',tintColor:'#D90019',left:'20%'});
	lblTermino = Titanium.UI.createLabel({text:'Deseo facturar',left:'48%',top:'6%',color:'white'});
	chkFactura = Ti.UI.createSwitch({value:false,top:'18%',onTintColor:'#62D337',tintColor:'#D90019',left:'10%'});
	lblFactura = Titanium.UI.createLabel({text:'Acepto términos y condiciones',right:'10%',top:'19%',color:'white'});
	btnRegistrar = Titanium.UI.createButton({title:'Registrarse',color:'white',borderRadius:30,borderColor:'white',backgroundColor:'#a00a1c',top:'45%',width:'85%', height:60});
	registerReq = Titanium.Network.createHTTPClient();
	
   lblFactura.addEventListener('click',function(){
   Ti.Platform.openURL("http://iego.com.mx/terminosycondiciones/index.html");
   });
   
		
	txtnombres.addEventListener('focus',function(){
		txtnombres.value = '';
		txtnombres.opacity=1;
		
	});
	txtapellidoP.addEventListener('focus',function(){
		txtapellidoP.value = '';
		txtapellidoP.opacity=1;
		
	});
	txtapellidoM.addEventListener('focus',function(){
		txtapellidoM.value = '';
		txtapellidoM.opacity=1;
		
	});
	
	txtCorreo.addEventListener('focus',function(){
	txtCorreo.value = '';
	txtCorreo.opacity=1;
	});
	
	txtPass.addEventListener('focus',function(){
		txtPass.value = '';
		txtPass.opacity=1;
	});
	txtPass2.addEventListener('focus',function(){
		txtPass2.value = '';
		txtPass2.opacity=1;
	});
	
		function checkemail(emailAddress){
    		var str = emailAddress;
    		var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    		if (filter.test(str)){
        		testresults = true;
    		}else{
        		testresults = false;
    		}
    		return (testresults);
	};
	
	registerReq.onload = function(){
		json = this.responseText;
    		response = JSON.parse(json);
    		if (response.message == "Insert failed"){
        		btnRegistrar.enabled = true;
        		btnRegistrar.opacity = 1;
        		alert(response.message);
    		}else{
    				Ti.App.Properties.setString("usuario", txtUser.value);
    				Ti.App.Properties.setString("nombre", txtnombres.value);
    				Ti.App.Properties.setString("apellido", txtapellidoP.value);
    				 Ti.App.Properties.setString("email", response.email_factura);
    				Ti.App.Properties.setInt("id_usuario", response.id_usuario);
    				Ti.App.Properties.setInt("id_cliente", response.id_cliente);
    				Ti.App.Properties.setString("telefono", response.telefono);
    		   		Ti.App.Properties.setString("identificador",Titanium.Platform.id);
                          		
    			 paypal= require('/interface/paypal');
        		 new paypal().open({modal:true});
        		 window.close();
    	
    		}
	};

	chkTerminos.addEventListener('change',function(){
		   if (chkTerminos.value == true) {
              btnRegistrar.title='Siguiente';
          } else if (chkTerminos.value == false) {
              btnRegistrar.title='Registrarse';
          }
	});
	btnRegistrar.addEventListener('click',function(e){
                if (txtnombres.value != '' && txtapellidoP.value != '' && txtapellidoM.value != '' && txtCorreo.value != '' && txtPass.value != '' && txtPass2.value != '' ){
              
                    if (!checkemail(txtCorreo.value)){
                        alert("Introduce un email valido");
                    }else{
                    		txtUser.value=txtCorreo.value;
                        if (chkTerminos.value == true) {
                        	Ti.App.Properties.setString("usuario1", txtUser.value);
         					Ti.App.Properties.setString("nombre1", txtnombres.value);
    						Ti.App.Properties.setString("email1", txtCorreo.value);
    						Ti.App.Properties.setString("telefono1", txtPass2.value);
    	 					Ti.App.Properties.setString("apellido1", txtapellidoP.value);
    	 					factura = require('/interface/factura');
                            winNav = factura();
                            winNav.fireEvent('foo',{
                                	username: txtUser.value,
                                    password: txtPass.value,
                                    correo: txtCorreo.value,
                                    telefono:txtPass2.value,
                                    nombre:txtnombres.value,
                                    apellido_p:txtapellidoP.value,
                                    apellido_m:txtapellidoM.value,
                                    identificador:Titanium.Platform.id
                               
                   	    	});
                               winNav.open();
                        }else{ 
                        	
                        	if (chkFactura.value==true){
                            registerReq.open("POST","http://iego.com.mx/webservices/registerResquest.php");
                                params = {
                                    username: txtUser.value,
                                    password: Ti.Utils.md5HexDigest(txtPass.value),
                                    correo: txtCorreo.value,
                                    telefono:txtPass2.value,
                                    nombre:txtnombres.value,
                                    apellido_p:txtapellidoP.value,
                                    apellido_m:txtapellidoM.value,
                                    rfc:'',
                                    razon_social:'',
                                    direccion_fiscal:'',
                                    email_factura:'',
                                    cp:'',
                                    identificador:Titanium.Platform.id
                                };
                                registerReq.send(params);
                        	}else{
                        		alert('Debe aceptar los terminos y condiciones');
                        	}
                        }
                    }
                
            }else{
                alert('Todos los campos son obligatorios');
            }
      
    });
    
    
    	window.addEventListener("open", function(evtop) { 
			var theActionBar = window.activity.actionBar; 
			if (theActionBar != undefined) {
		
				theActionBar.backgroundColor = '#373136';
        		 theActionBar.title = "Registro";
         	}
			
		window.activity.onCreateOptionsMenu = function(emenu) { 
				menu = emenu.menu;
			   	itemSearch = menu.add({
				title:'Inicio de sesion',
				showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});
				itemMapa = menu.add({
				title:'Mapa',
				showAsAction : Ti.Android.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW
				});	
	         	itemSearch.addEventListener('click', function(){		
				menu1 = require('interface/login');
				new menu1().open();
				window.close();
				});
				itemMapa.addEventListener('click', function(){		
				mapa = require('interface/mapa');
				new mapa().open();
				window.close();
				});
		};
	});
    
    //scrollView.add(txtUser);

	vista1.add(lblnombres);
	vista1.add(txtnombres);
	vista1.add(lblapellidoP);
	vista1.add(txtapellidoP);
	vista1.add(lblapellidoM);
	vista1.add(txtapellidoM);
	vista1.add(lblCorreo);
	vista1.add(txtCorreo);
	vista1.add(lblPass);
	vista1.add(txtPass);
	vista1.add(lblPass2);
	vista1.add(txtPass2);
	vista2.add(chkTerminos);
	vista2.add(lblTermino);
	vista2.add(chkFactura);
	vista2.add(lblFactura);
	vista2.add(btnRegistrar);
	scrollView.add(vista1);
	scrollView.add(vista2);
	
	window.add(scrollView);
	//window.add(vistaN);
	
	return window;
};
module.exports = registro;

