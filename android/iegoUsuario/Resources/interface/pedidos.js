function pedidos(){
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;
	
	    window= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	// Vista Lateral
	menuView = Ti.UI.createView({backgroundImage:'/images/bgmenu.png',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'red',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'});	
	lblCerrarSesion = Titanium.UI.createLabel({color:'white',text:'Cerrar sesión',top:'94%',left:'13%'});
	deleteServiciosReq = Titanium.Network.createHTTPClient();
	closeSesion=Titanium.Network.createHTTPClient();	
	window.addEventListener('android:back', function(e){
    	
    });
	
	lblCerrarSesion.addEventListener('click', function(ed){
		closeSesion.open("POST","http://iego.com.mx/webservices/statususuario.php");
                		params = {
                			id_usuario:Ti.App.Properties.getInt('id_usuario'),
                		
                        	
                    };
            closeSesion.send(params);	
	   		
			Titanium.App.Properties.removeProperty('id_usuario');
			Titanium.App.Properties.removeProperty('usuario');
			Titanium.App.Properties.removeProperty('nombre');
			Titanium.App.Properties.removeProperty('apellido');
			Titanium.App.Properties.removeProperty('email');
			Titanium.App.Properties.removeProperty('telefono');
			Titanium.App.Properties.removeProperty('rfc');
			Titanium.App.Properties.removeProperty('razon');
			Titanium.App.Properties.removeProperty('numero_tarjeta');
			inicio = require('/interface/inicio');
			new inicio().open({modal:true});
			window.close();
	});
	
	lblHome.addEventListener('click',function(){
		mapa = require('/interface/mapa');
		new mapa().open({modal:true});
		window.close();
	});
	
	lblMiPerfil.addEventListener('click',function(){
		perfil = require('/interface/perfil');
		new perfil().open({modal:true});
		window.close();});
	
		lblPromociones.addEventListener('click', function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
		alert('Muy pronto, esperalo...');
		/*promo= require('/interface/promociones');
		new promo().open({modal:true});
		window.close();*/
		}
		else{
			alert('Solo usuarios registrados');
		}
	});	
	lblPagos.addEventListener('click', function(){
		paypal= require('/interface/paypal');
		new paypal().open({modal:true});
		window.close();
	});	
	
	lblMisPedidos.addEventListener('click',function(){
		clickMenu(null);	
		});
		 
		 
	centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
	header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '10%',right:0,top: 0,width:'100%',zIndex: 10});
	iconHeader = Ti.UI.createButton({	backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
	iconEdit = Ti.UI.createButton({	backgroundImage:'/images/editarP.png', left:'88%',zIndex:2,height:'53%',width:'8%'});
  	titulo=Ti.UI.createLabel({text:'Mis Pedidos', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
  	iconEdit2 = Ti.UI.createButton({	backgroundImage:'/images/palomita.png', left:'88%',zIndex:2,height:'53%',width:'8%',visible:false,enabled:false});
	
	fondoP = Ti.UI.createView({backgroundImage: '/images/logo3.png',height: '24%',left:0,top:'10%',width:'auto',zIndex: 2});
	
	args = arguments[0] || {};url = "http://iego.com.mx/webservices/pedidos.php";db = {};
		
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					alert('Comprueba tu conexion a internet');
				
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			var params = {
    				id_usuario:Ti.App.Properties.getInt('id_usuario')
    			};
			xhr.send(params);
		}
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				row = Ti.UI.createTableViewRow({bottom:'5%',height:"100dp",backgroundColor:"white",backgroundSelectedColor:"blue"});
				horario_inicio = Ti.UI.createLabel({text:db.servicios[i].hora_inicio,color:'#151431',left:'10%',top:'45%',font:{fontSize:15}});
				vistaimg= Ti.UI.createView({width:65,height:65,borderRadius:30, top:'15%', bottom:15, left:'35%'});
				imgChofer = Ti.UI.createImageView({image:"http://iego.com.mx/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20'),width:65,height:97});
				folio = Ti.UI.createLabel({text:'FOLIO: '+db.servicios[i].folio,color:'#151431',left:'55%',top:'15%',font:{fontSize:12,fontWeight:'bold'}});
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].lugar_destino,color:'#151431',left:'55%',top:'28%',font:{fontSize:12}});
				nombre_chofer = Ti.UI.createLabel({text:'Conductor: '+db.servicios[i].nombre_chofer,color:'#a2a1b8',left:'55%',top:'82%',font:{fontSize:12}});
				status = Ti.UI.createView({width:15,height:15,borderRadius:7,top:'34%', left:'2%'});
				fecha = Ti.UI.createLabel({text:db.servicios[i].fecha,color:'#151431 ',left:'10%',top:'30%',font:{fontSize:15,fontWeight:'bold'}});
                precio = Ti.UI.createLabel({text:'$ '+db.servicios[i].costo + ' MXN',color:'#151431 ',left:'10%',top:'60%',font:{fontSize:14}});
				if (db.servicios[i].status_servicio == 1) {
						status.backgroundColor = '#09c863';
				}else if (db.servicios[i].status_servicio == 0){
						status.backgroundColor = '#a00a1c';
				}
				
				vistaimg.add(imgChofer);
				row.add(folio);
				row.add(status);
				row.add(vistaimg);
				row.add(fecha);
				row.add(precio);
				row.add(horario_inicio);
				row.add(lugar_origen);
				row.add(nombre_chofer);
				tableData.push(row);
			}
			tableView = Ti.UI.createTableView({data:tableData,top:'34%',bottom:0,scrollable:true});
			centerView.add(tableView);
			
			tableView.addEventListener('delete',function(e){
				deleteServiciosReq.open("POST","http://iego.com.mx/webservices/deleteservicios.php");
        			params = {
            			id_servicio:e.rowData.postIdServicio
        			};
        			deleteServiciosReq.send(params);
			});	
			
		}
		deleteServiciosReq.onload = function(){
    			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.logged == true){
    				
    			}else{
        			alert(response.message);
    			}
		};
		
		  iconEdit.addEventListener('click',function(){
	  	
	  	if (Ti.App.Properties.hasProperty('id_usuario')) {
	  	  iconEdit2.visible=true;
	  	  iconEdit2.enabled=true;
	  	  iconEdit.visible=false;
	  	  iconEdit.enabled=false;
	  	 
	  	       } else{alert('Solo usuarios registrados');}
	  
	  	  });
	  	  
	  	  iconEdit2.addEventListener('click',function(){
	  	  iconEdit.visible=true;
	  	   iconEdit.enabled=true;
	      iconEdit2.visible=false;
	      iconEdit2.enabled=false;
	              
	    });
	    
	  iconHeader.addEventListener('click',function(){
    		clickMenu(null);
		});
		centerView.addEventListener('swipe',function(event){
	if(event.direction=='left' || event.direction=='right' ){
		clickMenu(null);
	}	});
				
	function clickMenu(direction){
	 if(menuView._toggle === false && (direction==null || direction=='right')){
		centerView.animate({
				right:'-75%' ,
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
			menuView.animate({
				left: 0,
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
			
			menuView._toggle=true;
		}else if(direction==null || direction=='left'){
			centerView.animate({
				right:0 ,
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});
			menuView.animate({
				left: '-75%',
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
			});		menuView._toggle=false;
		};
		}
	
		header.add(iconHeader);
		//header.add(iconEdit);
			//header.add(iconEdit2);
	   	header.add(titulo);
	  	centerView.add(fondoP); 
	  	centerView.add(header);
		menuView.add(logoLeft);
		menuView.add(lblHome);
		menuView.add(lblMisPedidos);
		menuView.add(lblMiPerfil);
		menuView.add(lblPromociones);
		menuView.add(lblPagos);
		menuView.add(lblCerrarSesion);
		window.add(menuView);
	   	window.add(centerView);	
	
			getData();
	return window;
};
module.exports = pedidos;