function promociones(){
  
    var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta,centerView,titulo,btTodas,btNuevas;
    var tableData = [],tableView,centerView;
      
    window= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	
	/* Vista Principal */
	centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
	header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '19%',right:0,top: 0,width:'100%',zIndex: 3});
	iconHeader = Ti.UI.createButton({	backgroundImage:'/images/menuicon.png', top:'20%',left:'4%',zIndex:2,height:'22%',width:'9.5%'});
	titulo=Ti.UI.createLabel({text:'Promociones', color:'#232323',center:0,top:'15%',zIndex:2,font:{fontSize:'15.5%'}});
	btTodas = Ti.UI.createButton({backgroundColor:'#007aff',title:'Todas', top:'40%',left:'22.5%',zIndex:2,height:'25%',width:'28%'});
    btNuevas = Ti.UI.createButton({backgroundColor:'transparent',color:'#007aff',borderColor:'#007aff',title:'Nuevas', top:'40%',left:'49%',zIndex:2,height:'25%',width:'28%'});

		/* Menu Lateral */
	menuView = Ti.UI.createView({backgroundImage:'/images/bgmenu.png',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'white',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'red',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'});	
   lblCerrarSesion = Titanium.UI.createLabel({color:'white',text:'Cerrar sesión',top:'94%',left:'13%'});
	  	closeSesion=Titanium.Network.createHTTPClient(); 
   	window.addEventListener('android:back', function(e){
    });
   	lblHome.addEventListener('click',function(){
    	mapa = require('/interface/mapa');
  		new mapa().open({modal:true});
       	window.close();
 	});
 	
 	lblMisPedidos.addEventListener('click',function(){
        if (Ti.App.Properties.hasProperty('id_usuario')) {
            pedidos = require('/interface/pedidos');
           	new pedidos().open({modal:true});
            window.close();
        }else{
            alert('Solo usuarios registrados');
        }
    });
      
      lblMiPerfil.addEventListener('click',function(){
        	perfil = require('/interface/perfil');
       		new perfil().open({modal:true});  
    	   	window.close();
 	  });
   	
   		lblPagos.addEventListener('click',function(){
    		paypal = require('/interface/paypal');
       		new paypal().open({modal:true});
        	window.close();
   	  });
   	
       lblPromociones.addEventListener('click', function(){
     		clickMenu(null);
        }); 
 		lblCerrarSesion.addEventListener('click', function(ed){
 			closeSesion.open("POST","http://iego.com.mx/webservices/statususuario.php");
                		params = {
                			id_usuario:Ti.App.Properties.getInt('id_usuario'),
                		
                        	
                    };
            closeSesion.send(params);	
	   		
			Titanium.App.Properties.removeProperty('id_usuario');
			Titanium.App.Properties.removeProperty('usuario');
			Titanium.App.Properties.removeProperty('nombre');
			Titanium.App.Properties.removeProperty('apellido');
			Titanium.App.Properties.removeProperty('email');
			Titanium.App.Properties.removeProperty('telefono');
			Titanium.App.Properties.removeProperty('rfc');
			Titanium.App.Properties.removeProperty('razon');
			Titanium.App.Properties.removeProperty('numero_tarjeta');
			inicio = require('/interface/inicio');
			new inicio().open({modal:true});
			window.close();
		});
 		args = arguments[0] || {};url = "http://iego.com.mx/webservices/promociones.php";db = {};
        function getData(){
            var xhr = Ti.Network.createHTTPClient({
                onload: function() {
                    db = JSON.parse(this.responseText);
                    if(db.servicios){
                        setData(db);
                    }
                },
                onerror: function(e) {
                    Ti.API.debug("STATUS: " + this.status);
                    Ti.API.debug("TEXT:   " + this.responseText);
                    Ti.API.debug("ERROR:  " + e.error);
                    alert('Comprueba tu conexion a internet');
                },
                timeout:5000
            }); 
            xhr.open('POST', url);
            var params = {
                    id_cliente:Ti.App.Properties.getInt('id_cliente')
                };
            xhr.send(params);
        }
               function setData(db){
            for(var i = 0; i < db.servicios.length; i++){
                row = Ti.UI.createTableViewRow({bottom:'5%',height:"390dp",backgroundColor:"white",backgroundSelectedColor:"blue"});
                logo = Ti.UI.createImageView({image:'/images/logo2.png',width:'15%',height:'auto',left:'2%',top:'2%'});
                titulo = Ti.UI.createLabel({text:db.servicios[i].titulo_promocion,color:'#566673',left:'18%',top:'2%'});
                fecha = Ti.UI.createLabel({text:db.servicios[i].fecha_inicio,color:'#bac1c8',right:'2%',top:'2%'});
                politica = Ti.UI.createLabel({text:db.servicios[i].politicas_promocion,color:'#a00a1c',left:'18%',top:'8%'});
                imgPromo = Ti.UI.createImageView({image:"http://iego.com.mx/images/"+db.servicios[i].img_promocion.replace(/\s/g, '%20'),width:'95%',height:230,top:'20%',center:0});
                descPromo = Ti.UI.createLabel({text:db.servicios[i].desc_promocion,color:'#657582',left:'2%',top:'80%',right:'2%'});
                
                row.add(logo);
                row.add(titulo);
                row.add(fecha);
                row.add(politica);
                 row.add(imgPromo);
                row.add(descPromo);
                tableData.push(row);
            }
            tableView = Ti.UI.createTableView({data:tableData,top:'19%',bottom:0,scrollable:true});
            centerView.add(tableView);
        }
         getData();
         
         iconHeader.addEventListener('click',function(){
    		clickMenu(null);
		});
		centerView.addEventListener('swipe',function(event){
			if(event.direction=='left' || event.direction=='right' ){
			clickMenu(null);
			}	
		});
				
		function clickMenu(direction){
	 		if(menuView._toggle === false && (direction==null || direction=='right')){
				centerView.animate({right:'-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView.animate({left: 0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=true;
			}else if(direction==null || direction=='left'){
				centerView.animate({right:0,duration: 100,	curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT	});
				menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=false;
			};
		}
		 
		 header.add(iconHeader);
		 header.add(titulo);
         header.add(btTodas);
         header.add(btNuevas);
         centerView.add(header);
         centerView.add(titulo);
         window.add(menuView);
         window.add(centerView);
		menuView.add(logoLeft);
		menuView.add(lblHome);
		menuView.add(lblMisPedidos);
		menuView.add(lblMiPerfil);
		menuView.add(lblPromociones);
		menuView.add(lblPagos);
		menuView.add(lblCerrarSesion);
		return window;
}
module.exports = promociones;