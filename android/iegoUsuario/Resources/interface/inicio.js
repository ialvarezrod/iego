function inicio(){
	var window,logo,btnLogin,btnRegistrar,lblOmitir,slider1,slider2,slider3,scrollableView,navwin,login,winNav,registro,mapa,animateWin;
	
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar'});
    window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
    
    video1 = Titanium.Media.createVideoPlayer({top : 0,width : '100%',scalingMode : Titanium.Media.VIDEO_SCALING_MODE_FILL,url:'/images/v1.mp4',mediaControlStyle: Titanium.Media.VIDEO_CONTROL_HIDDEN});   
    logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'60%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
    btnLogin = Titanium.UI.createButton({height:'10%',width:'50%',left:0,title:'INICIAR SESIÓN',textAlign:'center',backgroundColor:'#cc4a4a',color:'white',bottom:0,zIndex:3});
    btnRegistrar = Titanium.UI.createButton({height:'10%',width:'50%',right:0,title:'REGISTRARSE',textAlign:'center',backgroundColor:'#a00a1c',color:'white',bottom:0,zIndex:3});
    lblOmitir = Titanium.UI.createButton({title:'Omitir',top:'5%',right:15,color:'white',backgroundColor:'transparent',zIndex:3});
    
    window.addEventListener('open',function(){
    		function getGeoPosition(_event) {
            
    		}
    		Ti.Geolocation.purpose = "Please allow us to find you.";
    		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
    		Ti.Geolocation.distanceFilter = 10;
    		function geolocation() {
    			var hasLocationPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);
    			if (hasLocationPermissions) {
        			Titanium.Geolocation.getCurrentPosition(getGeoPosition);
    			}
    			Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function(e) {
        			if (e.success) {
        			}else if (Ti.Platform.osname == 'android') {       
            			alert('Debe permitir a la aplicacion acceder a su localización.');
        			}else {
            			Ti.UI.createAlertDialog({title : 'Has denegado el permiso de localizacion.',
                			message : e.error
            			}).show();
        			}
    			});
		}
		geolocation();

		animateLogo = Titanium.UI.createAnimation();
        animateLogo.top = '16%';
        animateLogo.duration = 2000;
        animateLogo.opacity = 1;
        logo.animate(animateLogo);
    });
   
	video1.addEventListener('complete', function(e){
		video1.play();
	});
	
	btnLogin.addEventListener('click', function(){
        login = require('/interface/login');
        new login().open({modal:true});
    });
    btnRegistrar.addEventListener('click', function(){
    		/* factura = require('/interface/factura');
            	new factura().open({modal:true});*/
    		registro = require('/interface/registro');
     	new registro().open({modal:true});
    });
    lblOmitir.addEventListener('click', function(){
        Titanium.App.Properties.removeProperty('id_cliente');
        Titanium.App.Properties.removeProperty('id_usuario');
        Titanium.App.Properties.removeProperty('usuario');
        Titanium.App.Properties.removeProperty('email');
        Titanium.App.Properties.removeProperty('telefono');
        Titanium.App.Properties.removeProperty('rfc');
        Titanium.App.Properties.removeProperty('razon');
        mapa = require('/interface/mapa');
        new mapa().open({modal:true});
    });
    
    	window.add(logo);
	window.add(lblOmitir);
	window.add(btnLogin);
	window.add(btnRegistrar);
	window.add(video1);
    
    return window;
};
module.exports = inicio;