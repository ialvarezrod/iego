
function pago(){

		var self,header,iconHeader,iconShare,menuView,logoLeft,lblHome,lblMisPedidos,lblMiPerfil,lblPromociones,lblPagos,lblCuenta,
		txtCuenta,lblVencimiento,txtVencimiento,lblTarjeta,txtTarjeta,btnCuenta,lblNCuenta,lblCVV,cardView,pagoReq;
	
		self= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundImage:'/images/bglogin.jpg'});
		self .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
		centerView =Ti.UI.createView({height: '100%',right:0,top: 0,width:'100%',zIndex:1});
		header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '10%',right:0,top: 0,width:'100%',zIndex: 3});
		cardView = Ti.UI.createView({backgroundColor:'#bec1ba',bottom:'-30%',height:'30%',width:'auto',zIndex:3});
		btCancelar = Ti.UI.createButton({title:'Cancelar',left:'2%',top:'4%', height:'auto',widht:'auto',backgroundColor:'transparent',color:'blue'});
		btAceptar =Ti.UI.createButton({title:'Aceptar',right:'2%',top:'4%', height:'auto',widht:'auto',backgroundColor:'transparent',color:'blue'});
				
		titulo=Ti.UI.createLabel({text:'Pagos', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
		iconHeader = Ti.UI.createButton({	backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
   		iconShare = Ti.UI.createButton({	backgroundImage:'/images/shareicon.png',  left:'88%',zIndex:2,height:'53%',width:'6%'});
	
		menuView = Ti.UI.createView({backgroundImage:'/images/bgmenu.png',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	 	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
		lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
		lblMisPedidos = Titanium.UI.createLabel({color:'white',text:'Mis Pedidos',top:'60%',left:'13%'});
		lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
		lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
		lblPagos = Titanium.UI.createLabel({color:'red',text:'Pagos',top:'84%',left:'13%'});	
		
		lblCuenta = Ti.UI.createLabel({text:'Número de cuenta',center:0,top:'25%',color:'white'});
		txtCuenta = Ti.UI.createTextField({color:'#c2c2c2',hintText:'64531-4300-8710-9922',center:0,top:'31%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"64531-4300-8710-9922",textAlign:'center',editable:false});
		lblVencimiento = Ti.UI.createLabel({text:'Vencimiento',center:0,top:'40%',color:'white',editable:false});
		txtVencimiento = Ti.UI.createTextField({color:'#c2c2c2',hintText:'05/19',center:0,top:'46%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"05/19",textAlign:'center',editable:false});
        lblTarjeta = Ti.UI.createLabel({text:'Tipo de tarjeta',center:0,top:'55%',color:'white'});
    	txtTarjeta = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Seleccionar',center:0,top:'61%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"Seleccionar",textAlign:'center',backgroundColor:'white',editable:false});
        btnCuenta = Titanium.UI.createButton({title:'Añadir metodo de pago',color:'white',borderRadius:10,borderColor:'white',backgroundColor:'#a00a1c',top:'80%',width:'75%',height:50});
        
        
        lblNCuenta = Ti.UI.createLabel({text:'Número de cuenta',center:0,top:'10%',color:'red',visible:false});
        txtNCuenta = Ti.UI.createTextField({color:'#c2c2c2',hintText:'64531-4300-8710-9922',center:0,top:'16%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"64531-4300-8710-9922",textAlign:'center',visible:false});
        lblMES = Ti.UI.createLabel({text:'Mes',left:'3%',top:'26%',color:'red',visible:false});
        txtMes = Ti.UI.createTextField({color:'#c2c2c2',hintText:'02',left:'3%',top:'31%',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,value:"02",textAlign:'center',visible:false});
        lblANIO = Ti.UI.createLabel({text:'Año',right:'3%',top:'26%',color:'red',visible:false});
        txtAnio = Ti.UI.createTextField({color:'#c2c2c2',hintText:'2029',right:'3%',top:'31%',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,value:"2029",textAlign:'center',visible:false});
        lblCVV = Ti.UI.createLabel({text:'CVV',left:'3%',top:'41%',color:'red',visible:false});
        txtCVV = Ti.UI.createTextField({color:'#c2c2c2',hintText:'568',left:'3%',top:'46%',width:'30%',leftButtonPadding:10,borderRadius:6,height:40,value:"568",textAlign:'center',visible:false});
       
        Ti.App.Properties.getInt('id_usuario');
        Ti.App.Properties.getString('usuario');
        Ti.App.Properties.getString('apellido');
        pagoReq = Titanium.Network.createHTTPClient();
  	
  	
  	    	var picker = Ti.UI.createPicker({opacity:0.5,center:0,top:'28%',top:50,opacity:0.3,useSpinner: true,visibleItems:2});
			picker.selectionIndicator = true;
			var banco = [ 'mastercard', 'visa', 'amex','paypal'];
			var column1 = Ti.UI.createPickerColumn({font:{fontSize:'38%'},width:'100%',backgroundColor:'#ffffff',height:'92%'});

				for(var i=0, ilen=banco.length; i<ilen; i++){
  					var row = Ti.UI.createPickerRow({
   				 	title: banco[i]
  					});
  					column1.addRow(row);
				}
			picker.setSelectedRow(0, 0, true);
			picker.addEventListener('change',function(e){
				txtTarjeta.value= picker.getSelectedRow(0).title;
				Ti.App.Properties.setString("tipo_tarjeta", picker.getSelectedRow(0).title);
	        });

			picker.add([column1]);
			cardView.add(picker);
 
 	 var cardio = require('com.likelysoft.cardio');
	 
	  txtCuenta.addEventListener('click', function() {
		  var dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Escanear', 'Manualmente'],message: '¿Como desea Ingresar los datos?',title: 'Call'});
  	  	  dialog.addEventListener('click', function(ed){
              if (ed.index == 0){
               	cardio.scanCard(function(data){
                if(data.success == 'true') {
                    entradaC = data.expiryYear.split('0');
                    mes = data.expiryMonth;
                    salida = entradaC[1];
                    if (salida < 10) {
                        salida = salida+'0';
                    }
                    if (mes < 10) {
                        mes = '0'+mes;
                    }
                    var cardN=data.cardNumber.split(' ');
                    var cardN3 = cardN[3];
                    Ti.API.info(cardN[0]);
                    Ti.API.info(cardN[1]);
                    Ti.API.info(cardN[2]);
                    Ti.API.info(cardN[3]);
                    txtCuenta.value = '***************'+cardN3;
                    txtVencimiento.value = mes+'/'+salida;
                    txtNCuenta.value = data.cardNumber;
                    txtCVV.value = data.cvv;
                    txtMes.value = mes;
                    txtAnio.value = data.expiryYear;
                    Ti.API.info("Card number: " + data.cardNumber);
                    Ti.API.info("Redacted card number: " + data.redactedCardNumber);
                    Ti.API.info("Expiration month: " + data.expiryMonth);
                    Ti.API.info("Expiration year: " + data.expiryYear);
                    Ti.API.info("CVV code: " + data.cvv);
                }else {     
                }
            });	
            }else  if (ed.index == 1){
            		       		
            		txtCuenta.visible = false;
            		txtVencimiento.visible = false;
            		lblCuenta.visible = false;
            		lblVencimiento.visible = false;
            		
            		lblNCuenta.visible = true;
            		txtNCuenta.visible = true;
            		lblMES.visible = true;
            		txtMes.visible = true;
            		lblANIO.visible = true;
            		txtAnio.visible = true;
            		lblCVV.visible = true;
            		txtCVV.visible = true;
            		
            		txtNCuenta.focus();
           	}
           });
              dialog.show();
          });
          
		txtAnio.addEventListener('focus',function(){
            txtAnio.value = '';
        	});
        	txtNCuenta.addEventListener('focus',function(){
            txtNCuenta.value = '';
        	});
        	txtCVV.addEventListener('focus',function(){
            txtCVV.value = '';
        	});
        	txtMes.addEventListener('focus',function(){
            txtMes.value = '';
        	});
        	txtCuenta.addEventListener('focus',function(){
            txtCuenta.value = '';
        	});
       
       	lblHome.addEventListener('click', function(){
			home= require('/interface/mapa');
			new home().open({modal:true});
			self.close();		
		});
		
		lblMisPedidos.addEventListener('click', function(){
			if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos= require('/interface/pedidos');
			new pedidos().open({modal:true});
			self.close();
			}else{
			alert('Solo usuarios registrados');
			}   
		});	

		lblMiPerfil.addEventListener('click', function(){
			perfil= require('/interface/perfil');
			new perfil().open({modal:true});
			self.close();
		});
		
		lblPromociones.addEventListener('click', function(){
			if (Ti.App.Properties.hasProperty('id_usuario')) {
		/*	promo= require('/interface/promociones');
			new promo().open({modal:true});
			self.close();*/
			alert('Promociones no disponible');
			}else{
			alert('Solo usuarios registrados');
			}
		});	
	
		btnCuenta.addEventListener('click', function(){
			if (txtTarjeta.value == 'Seleccionar') {
				alert('Selecciona un tipo de tarjeta');
			}else if(txtAnio.value < 2016){
				alert('Año Invalido');
			}else{
				
				
			Ti.API.info("numero_tarjeta: " + txtNCuenta.value);
             Ti.API.info("tipo_tarjeta: " + Ti.App.Properties.getString('tipo_tarjeta'));
             Ti.API.info("Expiration month: " + txtMes.value);
             Ti.API.info("Expiration year: " + txtAnio.value);
             Ti.API.info("CVV code: " + txtCVV.value);
             Ti.API.info("name: " + Ti.App.Properties.getString('nombre'));
             Ti.API.info("lastname: " + Ti.App.Properties.getString('apellido'));
             Ti.API.info("id_usuario: " + Ti.App.Properties.getInt('id_usuario'));
             
		pagoReq.open("POST","http://subtmx.com/webservices/card.php");
                    params = {
                        tipo_tarjeta: Ti.App.Properties.getString('tipo_tarjeta'),
                        numero_tarjeta: txtNCuenta.value,
                        month_expire: txtMes.value,
                        year_expire:txtAnio.value,
                        cvv: txtCVV.value,
                        name: Ti.App.Properties.getString('nombre'),
                        lastname: Ti.App.Properties.getString('apellido'),
                        id_usuario: Ti.App.Properties.getInt('id_usuario')
                    };
           	pagoReq.send(params);	
		}
			
		});	
		pagoReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            		Ti.App.Properties.setString("numero_tarjeta", txtNCuenta.value);
            		Ti.App.Properties.setString("month_expire", txtMes.value);
            		Ti.App.Properties.setString("year_expire", txtAnio.value);
            		Ti.App.Properties.setString("cvv", txtCVV.value);
            		Ti.App.Properties.setString("id_car", response.id_car);
            		mapa = require('/interface/mapa');
                new mapa().open();
                self.close();
            }else{
            		alert("No se pudo agregar la tarjeta");
            }
    		};
	
	     lblPagos.addEventListener('click', function(){
     		clickMenu(null);
        }); 
		txtTarjeta.addEventListener('click',function(event){
				cardView.animate({
				bottom: 0,
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
				});		cardView._toggle=true;
		});	
			
		btAceptar.addEventListener('click',function(event){
				cardView.animate({
				bottom: '-30%',
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
				});		cardView._toggle=false;
		});	
	
		btCancelar.addEventListener('click',function(event){
				cardView.animate({
				bottom: '-30%',
				duration: 100,
				curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
				});		cardView._toggle=false;
		});	
			
		iconHeader.addEventListener('click',function(event){
				clickMenu(null);
		});
	
		centerView.addEventListener('swipe',function(event){
			if(event.direction=='left' || event.direction=='right' ){
				clickMenu(null);
			}	
		});
	
			function clickMenu(direction){
	 			if(menuView._toggle === false && (direction==null || direction=='right')){
					centerView.animate({right:'-75%' ,	duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
					menuView.animate({left: 0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
					menuView._toggle=true;
				}else if(direction==null || direction=='left'){
					centerView.animate({right:0,duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT	});
					menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
					menuView._toggle=false;
				};
			}
	
			header.add(titulo);	
			header.add(iconHeader);
			header.add(iconShare);
			centerView.add(header);
			cardView.add(btAceptar);
			cardView.add(btCancelar);
			centerView.add(cardView);
			menuView.add(logoLeft);
			menuView.add(lblHome);
			menuView.add(lblMisPedidos);
			menuView.add(lblMiPerfil);
			menuView.add(lblPromociones);
			menuView.add(lblPagos);
			centerView.add(lblCuenta);
     		centerView.add(txtCuenta);
     		centerView.add(lblVencimiento);
     		centerView.add(txtVencimiento);
     		centerView.add(lblTarjeta);
     		centerView.add(txtTarjeta);
     		centerView.add(btnCuenta);
     		centerView.add(lblNCuenta);
            		centerView.add(txtNCuenta);
            		centerView.add(lblMES);
            		centerView.add(txtMes);
            		centerView.add(lblANIO);
            		centerView.add(txtAnio);
            		centerView.add(lblCVV);
            		centerView.add(txtCVV);
      		self.add(menuView);
			self.add(centerView);
			return self;
}

module.exports = pago;