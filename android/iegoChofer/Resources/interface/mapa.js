function mapa(){
	var Map = require('ti.map');
	var player,self,btn1,btn2,btn3,btn4,btn5,bandera,player2,mapview,id_chofer,nch,xhr,startAnnotation,endAnnotation,route,
		startAnnotation2,endAnnotation2,route2,banderaruta,chek,webview,nuevaSolicitud,sdial,geocoderlbl,minutos,id_user;
	
	window= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'black'});
    window.orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
    var viewWeb= Ti.UI.createView({backgroundColor:'WHITE',visible:false});
    centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
    header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '10%',right:0,top: 0,width:'100%',zIndex: 3});
    iconHeader = Ti.UI.createButton({   backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
    iconShare = Ti.UI.createButton({ backgroundImage:'/images/shareicon.png',  left:'88%',zIndex:2,height:'53%',width:'6%'});
    titulo=Ti.UI.createLabel({text:'Servicio', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
    vista = Ti.UI.createView({backgroundColor:'#dadbdc',top:0,height:'15%',width:'100%',zIndex:1});
    menuView = Ti.UI.createView({backgroundImage:'/images/bgmenu.png',height: 'auto',left: '-75%',  top:0,width: '75%',zIndex: 9,   _toggle: false});
    btnPedir = Titanium.UI.createButton({width:'90%',height:60,title:'Pedir Automóvil',textAlign:'center',center:0,bottom:'10%',color:'#007aff',zIndex:3,backgroundColor:'white',borderRadius:6});
    logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
    lblHome = Titanium.UI.createLabel({color:'red',text:'Home',top:'52%',left:'13%'});
    lblMisPedidos = Titanium.UI.createLabel({color:'white',text:'Historial',top:'60%',left:'13%'});
    lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
    lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
    disponible = Ti.UI.createLabel({color:'white',text:'Disponible',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'}); 
    updateChoferReq = Titanium.Network.createHTTPClient();
    updateAutoReq = Titanium.Network.createHTTPClient();
    updateAuReq	= Titanium.Network.createHTTPClient();
    chkTerminos = Ti.UI.createSwitch({value:false,top:'76%',onTintColor:'#1ED760',tintColor:'#D90019',left:'45%'});
    btnlogin = Titanium.UI.createButton({title:'Ya llegué',color:'white',borderColor:'white',bottom:'-50%',width:'85%',height:60,backgroundColor:'#a00a1c',visible:false,zIndex:6,opacity:0.1});
	btnViaje = Titanium.UI.createButton({title:'Iniciar viaje',color:'white',borderColor:'white',top:'-50%',width:'85%',height:60,backgroundColor:'#a00a1c',visible:false,zIndex:6,opacity:0.1});
	btnFinalizar = Titanium.UI.createButton({title:'Finalizar viaje',color:'white',borderColor:'white',bottom:'-50%',width:'85%',height:60,backgroundColor:'#a00a1c',visible:false,zIndex:6,opacity:0.1});
	player = Ti.Media.createSound({url:"/sound/sonido.mp3"});
	geocoderlbl = Titanium.UI.createLabel({color:'Black',text:'',top:'60%',center:0,visible:true,zIndex:6}); 
	animatellegue = Titanium.UI.createAnimation();animatellegue.bottom = '8%';animatellegue.duration = 2000;animatellegue.opacity = 1;
	animatellegueEnd = Titanium.UI.createAnimation();animatellegueEnd.bottom = '-50%';animatellegueEnd.duration = 2000;animatellegueEnd.opacity = 0.1;
	animateviaje = Titanium.UI.createAnimation();animateviaje.top = '80%';animateviaje.duration = 2000;animateviaje.opacity = 1;
	animateviajeEnd = Titanium.UI.createAnimation();animateviajeEnd.top = '-50%';animateviajeEnd.duration = 2000;animateviajeEnd.opacity = 0.1;
 	var latUs,longUS,latD,longD,canceladoC=0,bcenacelado=0;
 	banderaruta = false;chek=true;id_chofer =Ti.App.Properties.getInt('id_chofer');nch =Ti.App.Properties.getString('usuario');
 	id_chofer_Int = Ti.App.Properties.getInt('id_chofer');var sdial = false; var sdialC=false; var sdialSA = false;
 	minutos=0;
 	Ti.App.placa = chkTerminos;
 	viewText = Ti.UI.createView({layout:'horizontal',width:Ti.UI.SIZE,visible:false, borderRadius:5, height:Ti.UI.SIZE, center:0, bottom:'25%',zIndex:20000});
 	viewText1 = Ti.UI.createView({backgroundColor:'WHITE', width:'10%',height:60,left:0});
 	viewText2 = Ti.UI.createView({backgroundColor:'WHITE', width:'65%',height:60});
 	ubicacionopng=Titanium.UI.createImageView({image:'/images/ubicacion.png',width:'65%',height:'auto'});
 	leftButtonOrigen = Titanium.UI.createButton({backgroundImage:'/images/green.png',width:30,height:30,center:0});
    
 	viewText3 = Ti.UI.createView({backgroundColor:'WHITE', width:'15%',borderRadius:5,  height:60,left:5});
 	var txtdireccion=Ti.UI.createLabel({color:'black',left:10,right:10,text:'Río Nuevo 102, Jose Colomo, Villahermosa, Tab.'});	
	var viewPro= Ti.UI.createView({backgroundColor:'WHITE', widht:'100%',height:'100%',zIndex:10,visible:false});
	var display_lbl =  Titanium.UI.createLabel({	text:"00:40",font:{fontSize:'65%'},top:'55%',center:0,color:'#c2c2c2 ',borderRadius:10,textAlign:'center'});
	var buscando_lbl =  Titanium.UI.createLabel({text:"Trazando ruta...",font:{fontSize:'26%'},top:'40%',center:0,color:'#c2c2c2 ',	borderRadius:10,textAlign:'center'});
	viewText.add(viewText1);
	viewText.add(viewText2);
	viewText.add(viewText3);
	viewText1.add(leftButtonOrigen);
	viewText3.add(ubicacionopng);
	viewText2.add(txtdireccion);
	
	var vistarutaActual = Ti.UI.createView({backgroundColor:'WHITE', width:'90%',height:'70%',borderRadius:5,visible:false,zIndex:30,center:0,borderColor:'#999',borderWidth:6});
	var vistarutaActual2 = Ti.UI.createView({top:40,width:Ti.UI.SIZE,height:Ti.UI.SIZE,layout:'vertical'});
	var txtvistarutaActual=Ti.UI.createLabel({color:'black',center:0,font:{fontSize : '20dp' ,fontWeight:'Bold'}});	
   	var txtvistarutaActual2=Ti.UI.createLabel({color:'black',center:0,top:40,font:{fontSize :'20dp'},textAlign:'center',width:'80%'});	
	var botonvistarutaActual = Titanium.UI.createButton({title:'Cerrar',color:'white',borderRadius:5,width:'75%',height:60,backgroundColor:'#0080fc',bottom:25});
   
    vistarutaActual2.add(txtvistarutaActual);
    vistarutaActual2.add(txtvistarutaActual2);
    vistarutaActual.add(vistarutaActual2);
    
    vistarutaActual.add(botonvistarutaActual);
    window.add(vistarutaActual);
    
    botonvistarutaActual.addEventListener('click',function(){
	vistarutaActual.visible=false;
	});
    viewText2.addEventListener('click',function(){
	vistarutaActual.visible=true;
	});
	txtdireccion.addEventListener('click',function(){
	vistarutaActual.visible=true;
	});
    
	viewText3.addEventListener('click',function(){
		
		abrirmapa();
	});
	ubicacionopng.addEventListener('click',function(){
		
		abrirmapa();
	});
	/*	Ti.App.addEventListener('close', function(e) {
            Ti.App.fireEvent('fbController:disponibilidad',{dis:0,id:Ti.App.Properties.getInt('id_chofer')});
            Ti.App.fireEvent('fbController:removerGeo',{id:Ti.App.Properties.getInt('id_chofer')});
        });*/
       
    

		
		var countDown =  function(m,s,fn_tick,fn_end){
    		return {
     			total_sec:m*60+s,timer:this.timer,
                	set: function(m,s) {
                		this.total_sec = parseInt(m)*60+parseInt(s);this.time = {m:m,s:s};
                		return this;
            		},
                	start: function() {
                		var self1 = this;
                	this.timer = setInterval( function() {
                		if (self1.total_sec) {
                    		self1.total_sec--;
                    		self1.time = { m : parseInt(self1.total_sec/60), s: (self1.total_sec%60) };
                    		fn_tick();
                		} else {
                    		self1.stop();
                    		fn_end();
                		}
                	}, 1000 );
            				return this;
            		},
            		stop: function() {
            				clearInterval(this.timer);
            				this.time = {m:0,s:0};
            				this.total_sec = 0;
            				return this;
            		}
       	 	};
    		};
			var my_timer = new countDown(00,40,function() {
             	if (my_timer.time.s==0){
             		viewPro.visible=false;
             		abrirmapa();
             		viewText.visibe=true;
             		btnlogin.visible = true;
             		btnlogin.animate(animatellegue);
             		
                    	display_lbl.text = '0'+my_timer.time.m+":0"+my_timer.time.s;
                }else if (my_timer.time.s<10){    
                		display_lbl.text = '0'+my_timer.time.m+":0"+my_timer.time.s;
            		}else{
            			display_lbl.text = '0'+my_timer.time.m+":"+my_timer.time.s;
            		}
        	},function() {});
 			
 			
	var my_timer4 = new countDown(00, 59, function() {
		if (my_timer4.time.s == 0) {
			Ti.App.fireEvent('fbController:noaceptado', {
				id : id_chofer
			});
			Ti.API.info('0' + my_timer4.time.m + ":" + my_timer4.time.s);

			my_timer4.stop();
			my_timer4.set(00, 59);
					} else if (my_timer4.time.s <= 60) {

			if (my_timer4.time.s < 10) {
				Ti.API.info('0' + my_timer4.time.m + ":0" + my_timer4.time.s);
			} else {
				Ti.API.info('0' + my_timer4.time.m + ":" + my_timer4.time.s);
			}
		}

	}, function() {
	}); 

 

    
window.addEventListener('androidback', function(e){
  
  
   });
   
 
 	lblPagos.addEventListener('click', function(){
 			var viewChildren = viewWeb.children.slice(0);
					for (var i=0; i < viewChildren.length; i++) {
						var child = viewChildren[i];
						viewWeb.remove(child);	
					}
    		pago= require('/interface/pago');
        new pago().open({modal:true});
        window.close();
    }); 
    lblPromociones.addEventListener('click', function(){
    		var viewChildren = viewWeb.children.slice(0);
					for (var i=0; i < viewChildren.length; i++) {
						var child = viewChildren[i];
						viewWeb.remove(child);	
					}
    		if (Ti.App.Properties.hasProperty('id_usuario')) {
        		promo= require('/interface/promociones');
            new promo().open({modal:true});
            window.close();
        }else{
            alert('Solo usuarios registrados');
        }
    }); 
    lblMisPedidos.addEventListener('click',function(){
    		var viewChildren = viewWeb.children.slice(0);
					for (var i=0; i < viewChildren.length; i++) {
						var child = viewChildren[i];
						viewWeb.remove(child);	
					}
    		if (Ti.App.Properties.hasProperty('id_usuario')) {
            pedidos = require('/interface/pedidos');
            new pedidos().open({modal:true});
            window.close();
        }else{
            alert('Solo usuarios registrados');
       	}
    });
    lblMiPerfil.addEventListener('click', function(){
    		var viewChildren = viewWeb.children.slice(0);
					for (var i=0; i < viewChildren.length; i++) {
						var child = viewChildren[i];
						viewWeb.remove(child);	
			}
    		perfil= require('/interface/perfil');
        new perfil(id_chofer_Int).open();
        window.close();
    });
   	iconHeader.addEventListener('click',function(event){
    		clickMenu(null);
    });
    
    var mapview;var annotations = [];var annotation;var locationAdded = false;var latitude;var longitude;
    webview = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false});
    webview.addEventListener('load', function() {
		Ti.Geolocation.getCurrentPosition(function(e) {
		if (e.error){
        	alert('No se puede obtener ubicacion actual');
        	return;
    	}
			
		Ti.App.fireEvent('fbController:coordsChofer',{
						id:id_chofer,
						latIn:e.coords.latitude,
						lgnIn:e.coords.longitude
		});
			
			Ti.App.fireEvent('firebase:init', {
				latitude : e.coords.latitude,
				longitude : e.coords.longitude,
				id_chofer: id_chofer_Int
			});
		var timeStamp = String(new Date().getTime());	
		Ti.App.fireEvent('fbController:login',{
						id:id_chofer,
						id2:Ti.App.Properties.getInt("id_usuario"),
						nomuser:nch,
						timestamp:timeStamp
		});
		});
	});
	
	viewWeb.add(webview);
	function open() {
		Ti.Geolocation.getCurrentPosition(function(e) {
				if (e.error){
        			alert('No se puede obtener ubicacion actual');
        			return;
    			}
			mapview = Map.createView({mapType : Map.NORMAL_TYPE,
				region : {latitude:e.coords.latitude,longitude:e.coords.longitude,latitudeDelta:0.009,longitudeDelta:0.009,userLocation:true,visible:true},
				animate : true,
				regionFit : true,
				userLocation : true,
			});
			centerView.add(mapview);
			if (e.success || e.error) {
					Ti.API.info('error:' + JSON.stringify(e.error));
					return;
			}
			
		});
	}
	 
	 	Ti.App.addEventListener('fbController:addMarker', function(e){
			annotations[e.id] = Map.createAnnotation({
				driverId : e.id,
				latitude : e.latitude,
				longitude : e.longitude,
				image : '/images/autochofer.png',
				title : 'Chofer: ' + e.nombreUser
			});
			mapview.addAnnotation(annotations[e.id]);
		});
	var nuevaSolicitud=Ti.UI.createView({width:'90%',height:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false });
	var txtnuevaSolicitud=Ti.UI.createLabel({text:'Nuevo servicio',center:0,top:'10%',color:'black',font:{fontSize:16}});
	var txtnuevaSolicitud2=Ti.UI.createLabel({text:'Tienes una nueva solicitud de servicio.',center:0,top:'30%',color:'black',font:{fontSize:13},textAlign:'center'});
	var btnnuevaSolicitud=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#a00a1c',bottom:'5%',right:'10%',borderRadius:4});
	var btnnuevaSolicitud2=Ti.UI.createButton({title:'Cancelar',color:'white',backgroundColor:'#a00a1c',bottom:'5%',left:'10%',borderRadius:4});
	
	
		nuevaSolicitud.add(txtnuevaSolicitud);
		nuevaSolicitud.add(txtnuevaSolicitud2);
		nuevaSolicitud.add(btnnuevaSolicitud);
		nuevaSolicitud.add(btnnuevaSolicitud2);
	var viajecancelado=Ti.UI.createView({width:'90%',height:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false});
	var txtviajecancelado=Ti.UI.createLabel({text:'Viaje Cancelado',center:0,top:'10%',color:'black',font:{fontSize:16}});
	var txtviajecancelado2=Ti.UI.createLabel({text:'El usuario ha cancelado el viaje',center:0,top:'30%',color:'black',font:{fontSize:13},textAlign:'center'});
	var btnviajecancelado=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#a00a1c',bottom:'5%',center:0,borderRadius:4});
		
		viajecancelado.add(txtviajecancelado);
		viajecancelado.add(txtviajecancelado2);
		viajecancelado.add(btnviajecancelado);
	
	var saldoinsuficiente=Ti.UI.createView({width:'90%',height:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false});
	var txtsaldoinsuficiente=Ti.UI.createLabel({text:'Notificación del servicio',center:0,top:'10%',color:'black',font:{fontSize:16}});
	var txtsaldoinsuficiente2=Ti.UI.createLabel({text:'El servicio no pudo ser procesado, debido a que el usuario no cuenta con saldo suficiente',center:0,top:'30%',color:'black',font:{fontSize:13},textAlign:'center'});
	var btnsaldoinsuficiente=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#a00a1c',bottom:'5%',center:0,borderRadius:4});
		
		saldoinsuficiente.add(txtsaldoinsuficiente);
		saldoinsuficiente.add(txtsaldoinsuficiente2);
		saldoinsuficiente.add(btnsaldoinsuficiente);
	
	var noaceptado=Ti.UI.createView({width:'90%',height:'30%',top:'35%',bottom:'35%',zIndex:30,backgroundColor:'white',borderColor:'black',borderRadius:3,visible:false});
	var txtnoaceptado=Ti.UI.createLabel({text:'Servicio no aceptado',center:0,top:'10%',color:'black',font:{fontSize:16}});
	var txtnoaceptado=Ti.UI.createLabel({text:'Le recordamos que debe estar pendiente de sus servicios y aceptarlos',center:0,top:'30%',color:'black',font:{fontSize:13},textAlign:'center'});
	var btnnoaceptado=Ti.UI.createButton({title:'Aceptar',color:'white',backgroundColor:'#a00a1c',bottom:'5%',center:0,borderRadius:4});
		
		noaceptado.add(txtnoaceptado);
		noaceptado.add(txtnoaceptado);
		noaceptado.add(btnnoaceptado);
		

	Ti.App.addEventListener('fbController:moveMarker', function(e){
		
		if (e.error){
        
        	return;
    	}else{
    		Ti.API.info(e.id);
    		Ti.API.info(e.latitude);
    		Ti.API.info(e.longitude);
    		
    		if(e.latitude!=undefined && e.longitude!=undefined && e.id!=undefined ){
    		id_user=e.id_user;	
    		annotations[e.id].setLatitude(e.latitude);
			annotations[e.id].setLongitude(e.longitude);
			mapview.setRegion({latitude:e.latitude,longitude:e.longitude,latitudeDelta:0.003, longitudeDelta:0.003});
    	}
	}	
		
		if (e.statussolicitud == 1 && e.servicioaceptado == 0 && e.statusdisponibilidad == 1) {
			if(Ti.App.Properties.getString('latUs')==''){
			Ti.App.Properties.setString('latUs',e.latitude);	
			}
			if(Ti.App.Properties.getString('longUS')==''){
			Ti.App.Properties.setString('longUS',e.longitude);	
			}
			if(Ti.App.Properties.getString('latD')==''){
			Ti.App.Properties.setString('latD',e.latOrigenUser);
			}
			if(Ti.App.Properties.getString('longD')==''){
			Ti.App.Properties.setString('longD',e.longiOrigenUser);	
			}
			if(Ti.App.Properties.getString('txtdireccion')==''){
			Ti.App.Properties.setString('txtdireccion',e.origen);
			}
			if(Ti.App.Properties.getString('latD2')==''){
			Ti.App.Properties.setString('latD2',e.latDestinoUser);
			}
			if(Ti.App.Properties.getString('longD2')==''){
			Ti.App.Properties.setString('longD2',e.longDestinoUser);	
			}
			if(Ti.App.Properties.getString('txtdireccion2')==''){
			Ti.App.Properties.setString('txtdireccion2',e.destino);
			}
			
			
			
			
		
			if (sdial == false) {
				
					bcenacelado=0;
					canceladoC=0;
					Titanium.Android.NotificationManager.notify(0, 
					 	Titanium.Android.createNotification({
    						contentTitle: 'ALERTA',
				    		contentText : 'Tienes una nueva solicitud de servicio.',
				   			tickerText: 'mensaje de IEGO',
				    		contentIntent: Ti.Android.createPendingIntent({
				    			intent: Ti.Android.createIntent({
 									action: Ti.Android.ACTION_MAIN,
				    				className: 'com.tobe.iegoC.IegochoferActivity',
				    				packageName: 'com.tobe.iegoC'
				    			})
				  			}),
						})
					);	
				
   				updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:1,
                        	status_disponibilidad:1
                        	
                    };
                   	     updateAuReq.send(params);	
                   	     nuevaSolicitud.visible=true;
                   	     player.play();
   			}
   		}else if(e.statussolicitud == 0 && e.noaceptado==1){
   			if (bcenacelado==0){
   				bcenacelado=1;
   			 	sdial =  false;
   			 	nuevaSolicitud.visible=false;
   			 	noaceptado.visible=true;
   			 	Titanium.Android.NotificationManager.cancelAll();	
   			}
   			
   		}else if((e.statussolicitud == 0 && e.servicioaceptado == 1 && e.noaceptado==0) || (e.statussolicitud == 0 && e.servicioaceptado == 1 && e.noaceptado==1)){
   			if(sdialC == false){
   				sdialC =true;
   				my_timer.stop();
   				Titanium.Android.NotificationManager.notify(1, 
					 	Titanium.Android.createNotification({
    						contentTitle: 'ALERTA',
				    		contentText : 'El usuario ha cancelado el viaje',
				   			tickerText: 'mensaje de IEGO',
				    		contentIntent: Ti.Android.createPendingIntent({
				    			intent: Ti.Android.createIntent({
 									action: Ti.Android.ACTION_MAIN,
				    				className: 'com.tobe.iegoC.IegochoferActivity',
				    				packageName: 'com.tobe.iegoC'
				    			})
				  			}),
						})
					);	
   				
   			updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1
                        	
                    };
                updateAuReq.send(params);	
   				viajecancelado.visible=true;
   				player.play();
   				
   			}
   			
   		 }  else if(e.statussolicitud == 0 && e.noaceptado==0)	{	
   		      sdial =  false;
   		 }
   		
   	
   		   		
   		if(e.serviciocancelado == 1){
   				if(sdialC == false){
   				sdialC =true;
   				my_timer.stop();
   				Titanium.Android.NotificationManager.notify(1, 
					 	Titanium.Android.createNotification({
    						contentTitle: 'ALERTA',
				    		contentText : 'El usuario ha cancelado el viaje',
				   			tickerText: 'mensaje de IEGO',
				    		contentIntent: Ti.Android.createPendingIntent({
				    			intent: Ti.Android.createIntent({
 									action: Ti.Android.ACTION_MAIN,
				    				className: 'com.tobe.iegoC.IegochoferActivity',
				    				packageName: 'com.tobe.iegoC'
				    			})
				  			}),
						})
					);	
   				
   			updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1
                        	
                    };
                   	     updateAuReq.send(params);	
   				viajecancelado.visible=true;
   				player.play();
   				
   			}
   		}
   		// if(e.noaceptado==1){
   		 	// noaceptado.visible=true;
   			// Titanium.Android.NotificationManager.cancelAll();	
   		// }
   		if(e.saldoagotado == 1){
   			
   			
   			if(sdialSA == false){
   				my_timer.stop();
   				Titanium.Android.NotificationManager.notify(2, 
					 	Titanium.Android.createNotification({
    						contentTitle: 'ALERTA',
				    		contentText : 'El servicio no pudo ser procesado, debido a que el usuario no cuenta con saldo suficiente',
				   			tickerText: 'mensaje de IEGO',
				    		contentIntent: Ti.Android.createPendingIntent({
				    			intent: Ti.Android.createIntent({
 									action: Ti.Android.ACTION_MAIN,
				    				className: 'com.tobe.iegoC.IegochoferActivity',
				    				packageName: 'com.tobe.iegoC'
				    			})
				  			}),
						})
					);	
   				
   			updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1
                        	
                    };
                updateAuReq.send(params);	
   				saldoinsuficiente.visible=true;
   				player.play();
   				sdialSA = true;
   			}
   		}else if(e.saldoagotado == 0){
   			if(sdialSA == false){
   				my_timer.stop();
   				viewPro.visible=false;
   				abrirmapa();
             	viewText.visibe=true;
             	btnlogin.visible = true;
             	btnlogin.animate(animatellegue);
   				display_lbl.text="00:00";
   				
   				sdialSA = true;
   			}
   		}

	});
   btnviajecancelado.addEventListener('click', function(ed){
   		
    	player.stop();
    	viewPro.visible=false;
    	viajecancelado.visible=false;
    	
    	Titanium.App.Properties.removeProperty('solicitudAceptada');
    	Ti.App.Properties.setString('latUs','');	
		Ti.App.Properties.setString('longUS','');	
		Ti.App.Properties.setString('latD','');
		Ti.App.Properties.setString('longD','');	
		Ti.App.Properties.setString('txtdireccion','');
		Ti.App.Properties.setString('latD2','');
		Ti.App.Properties.setString('longD2','');	
		Ti.App.Properties.setString('txtdireccion2','');
	
		Titanium.App.Properties.removeProperty('yallego');
    		Titanium.Android.NotificationManager.cancelAll();
    		updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1
                        	
                    };
                   	     updateAuReq.send(params);
    	
    	Ti.App.fireEvent('fbController:serviciocancelado', {
			id:id_chofer
		});
		geocoderlbl.text='';
		geocoderlbl.visible=false;
    	 banderaruta=false;
         //sdialC = false;
         sdialSA = false;
         btnlogin.visible = false;
         btnViaje.visible = false;
         btnFinalizar.visible = false;
    	btnlogin.animate(animatellegueEnd);
    	txtdireccion.text='';
		viewText.visible=false;
		

		if(display_lbl.text=="00:00"){
		// mapview.removeRoute(route);
		// mapview.removeAnnotation(startAnnotation);
		// mapview.removeAnnotation(endAnnotation);		
		}
		my_timer.set(00,40);
   		display_lbl.text="00:40";
        
   });
   
   btnsaldoinsuficiente.addEventListener('click', function(ed){
    	player.stop();
    		saldoinsuficiente.visible=false;
    		Ti.App.Properties.setString('latUs','');	
			Ti.App.Properties.setString('longUS','');	
			Ti.App.Properties.setString('latD','');
			Ti.App.Properties.setString('longD','');	
			Ti.App.Properties.setString('txtdireccion','');
			Ti.App.Properties.setString('latD2','');
			Ti.App.Properties.setString('longD2','');	
			Ti.App.Properties.setString('txtdireccion2','');
	
    		Titanium.App.Properties.removeProperty('solicitudAceptada');
			Titanium.App.Properties.removeProperty('yallego');
    		Titanium.Android.NotificationManager.cancelAll();
    		updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1
                        	
                    };
                   	     updateAuReq.send(params);
    	
    	Ti.App.fireEvent('fbController:saldoagotado', {
			id:id_chofer
		});
		viewPro.visible=false;
		geocoderlbl.text='';
		geocoderlbl.visible=false;
    	 banderaruta=false;
        
         sdialC = true;
      viajecancelado.visible=false;
         btnlogin.visible = false;
         btnViaje.visible = false;
         btnFinalizar.visible = false;
    	btnlogin.animate(animatellegueEnd);
    	txtdireccion.text='';
		viewText.visible=false;
		if(display_lbl.text=="00:00"){
		// mapview.removeRoute(route);
		// mapview.removeAnnotation(startAnnotation);
		// mapview.removeAnnotation(endAnnotation);		
		}
		my_timer.set(00,40);
   		display_lbl.text="00:40";
        
   });
   
    btnnoaceptado.addEventListener('click', function(ed){
    noaceptado.visible=false;
    if(canceladoC==1){
    	my_timer4.start();
    }else{
    	Ti.App.fireEvent('fbController:noaceptado', {
			id:id_chofer
		});
    }
    
    });	
	
	btnnuevaSolicitud.addEventListener('click', function(ed){
		var track = Ti.UI.createView({ width: "80%", height: "2%", borderRadius:100, backgroundColor: '#c2c2c2',top:'50%'});
   			var progress = Ti.UI.createView({ borderRadius:40, left: 0, width: '100%', height: "100%", backgroundColor : '#a00a1c'});
 		 track.add(progress); 
    	viewPro.add(track);
			nuevaSolicitud.visible=false;
			sdialSA = false;
			Ti.App.Properties.setInt('solicitudAceptada',1);
			Titanium.Android.NotificationManager.cancelAll();
		  	updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:1,
                        	status_solicitud:1,
                        	status_disponibilidad:1,
                    };
                   	     updateAuReq.send(params);
			
			player.stop();
			viewPro.visible=true;
			sdial = true;
			sdialC =false;
			Ti.App.fireEvent('fbController:servicioaceptado', {
				id:id_chofer
			});
        	
			progress.animate({ width: 0, duration: 40000 });my_timer.start();	
	});
	btnnuevaSolicitud2.addEventListener('click', function(ed){
			canceladoC=1;
			nuevaSolicitud.visible=false;
			player.stop();
			sdial = true;
			Titanium.Android.NotificationManager.cancelAll();
			Ti.App.fireEvent('fbController:solicitud', {
				id:id_chofer
			});
			
			updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1
                        	
                    };
                   	     updateAuReq.send(params);
			
	 });	
	
	btnlogin.addEventListener('click',function(){
		minutos=0;
		Ti.App.Properties.setInt('yallego',1);
    		Ti.App.fireEvent('fbController:yallegue',{
			id:id_chofer
		});
		banderaruta=true;
		btnlogin.animate(animatellegueEnd);
		btnViaje.visible = true;
		btnViaje.animate(animateviaje);
		geocoderlbl.text='';
		geocoderlbl.visible=false;
		txtdireccion.text='';
		viewText.visible=false;
		// mapview.removeRoute(route);
		// mapview.removeAnnotation(startAnnotation);
		// mapview.removeAnnotation(endAnnotation);			
	});
	btnViaje.addEventListener('click',function(){
		
			abrirmapa();
            btnViaje.animate(animateviajeEnd);
            btnFinalizar.visible = true;
            btnFinalizar.animate(animatellegue);
            geocoderlbl.visible=true;
            viewText.visible=true;
            
            Ti.App.fireEvent('fbController:iniciarViaje', {
				id:id_chofer
			});
            
	});
	btnFinalizar.addEventListener('click',function(){
	updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:Ti.App.Properties.getString('placa'),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1,
                        	
                    };
                   	     updateAuReq.send(params);	
		minutos=0;
		Titanium.App.Properties.removeProperty('solicitudAceptada');
		Ti.App.Properties.setString('latUs','');	
		Ti.App.Properties.setString('longUS','');	
		Ti.App.Properties.setString('latD','');
		Ti.App.Properties.setString('longD','');	
		Ti.App.Properties.setString('txtdireccion','');
		Ti.App.Properties.setString('latD2','');
		Ti.App.Properties.setString('longD2','');	
		Ti.App.Properties.setString('txtdireccion2','');
	
		Titanium.App.Properties.removeProperty('yallego');
		// actualizar los servicios
            banderaruta=false;
            sdial =  false;
            sdialC = false;
            sdialSA = false;
            btnlogin.visible = false;
            btnViaje.visible = false;
            btnFinalizar.visible = false;
            btnFinalizar.animate(animatellegueEnd);
            // mapview.removeRoute(route);
			// mapview.removeAnnotation(startAnnotation);
			// mapview.removeAnnotation(endAnnotation);
			geocoderlbl.visible=false;
			viewText.visible=false;
			txtdireccion.text='';
            Ti.App.fireEvent('fbController:finalizarviaje', {
            id_user:id_user,	
			id:id_chofer
			});
			my_timer.set(00,40);
   			display_lbl.text="00:40";
   			id_user='';
	});
	
	
	var contador='';
	var hor='';
	var min='';
	var seg='';
	var my_timer2 = new countDown(480,00,function() {
		if (my_timer2.time.m==475 && my_timer2.time.s==0){
                	
                		contador='5:00';
                		Ti.App.fireEvent('fbController:cronometro',{
						cro:contador,
						id:id_chofer
					});
                		Ti.API.info(my_timer2.time.m+":0"+my_timer2.time.s);
                }else if (my_timer2.time.m==470 && my_timer2.time.s==0){
                	
                		contador='10:00';
                		Ti.App.fireEvent('fbController:cronometro',{
						cro:contador,
						id:id_chofer
					});
                		Ti.API.info(my_timer2.time.m+":0"+my_timer2.time.s);
                }else if (my_timer2.time.m==465 && my_timer2.time.s==0){
                	
                		contador='15:00';
                		Ti.App.fireEvent('fbController:cronometro',{
						cro:contador,
						id:id_chofer
					});
                		Ti.API.info(my_timer2.time.m+":0"+my_timer2.time.s);
                }else if (my_timer2.time.m<480 && my_timer2.time.s<10){
             		    hor=00;
             			min=479-my_timer2.time.m;
             			seg=60-"0"+my_timer2.time.s;
             			if(seg<10){
             			seg='0'+seg;
             			}
             			if(min>=60 && min<120){
             				hor=01;
             				min=min-60;
             				min=min;
             			}else if(min==120 && min<180){
             				hor=02;
             				min=min-120;
             				min=min;
             			}else if(min==180 && min<240){
             				hor=03;
             				min=min-180;
             				min=min;
             			}else if(min==240 && min<300 ){
             				hor=04;
             				min=min-240;
             				min=min;
             			}else if(min==300 && min<360){
             				hor=05;
             				min=min-300;
             				min=min;
             			}else if(min==360 && min<420 ){
             				hor=06;
             				min=min-360;
             				min=min;
             			}else if(min==420 && min<480){
             				hor=07;
             				min=min-420;
             				min=min;
             			}else if(min==480){
             				hor=08;
             				min=min-480;
             				min=min;
             				seg=00;
             			}
             			
             		
             			contador=hor+':'+min +':'+seg;
             		    Ti.API.info(my_timer2.time.m+":0"+my_timer2.time.s);
                }else if (my_timer2.time.m<480 && my_timer2.time.s>=10){
                		
                		hor=00;
                		min=479 - my_timer2.time.m;
             			seg=60- my_timer2.time.s;
             				if(seg<10){
             				seg='0'+seg;
             			}
             			if(min>=60 && min<120){
             				hor=01;
             				min=min-60;
             				min=min;
             			}else if(min==120 && min<180){
             				hor=02;
             				min=min-120;
             				min=min;
             			}else if(min==180 && min<240){
             				hor=03;
             				min=min-180;
             				min=min;
             			}else if(min==240 && min<300 ){
             				hor=04;
             				min=min-240;
             				min=min;
             			}else if(min==300 && min<360){
             				hor=05;
             				min=min-300;
             				min=min;
             			}else if(min==360 && min<420 ){
             				hor=06;
             				min=min-360;
             				min=min;
             			}else if(min==420 && min<480){
             				hor=07;
             				min=min-420;
             				min=min;
             			}else if(min==480){
             				hor=08;
             				min=min-480;
             				min=min;
             				seg=00;
             			}
             			
             			
             			contador=hor+':'+min +':'+seg;
                		Ti.API.info(my_timer2.time.m+":"+my_timer2.time.s);
                }
                    	
               
        	},function() {});
	
	chkTerminos.addEventListener('change',function(e){
			if (chkTerminos.value == true) {
				Ti.Geolocation.getCurrentPosition(function(e) {
				if (e.error) {
					alert('No se puede obtener ubicacion actual');
					return;
				}

				Ti.App.fireEvent('fbController:coordsChofer', {
					id : id_chofer,
					latIn : e.coords.latitude,
					lgnIn : e.coords.longitude
				});
			}); 
				
				Ti.App.Properties.setString("Disponible",1);
				if(contador!=''){
					my_timer2.stop();
					//alert(contador);
					Ti.App.fireEvent('fbController:cronometro',{
						cro:contador,
						id:id_chofer
					});
				}
				
				updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa: Ti.App.Properties.getString("placa"),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1,
                         };
                   	     updateAuReq.send(params);
				
				


				var timeStamp = String(new Date().getTime());
				Ti.App.fireEvent('fbController:login',{
						id:id_chofer,
						id2:Ti.App.Properties.getInt("id_usuario"),
						nomuser:nch,
						timestamp:timeStamp
				});
				Ti.App.fireEvent('fbController:disponibilidad',{
						dis:1,
						id:id_chofer
					});
			if(Ti.App.Properties.getString('placas')==1){
				
			}else{
			placas= require('/interface/placas');
            new placas().open({modal:true});	
			}	
			
				
			
					Ti.Geolocation.getCurrentPosition(function(eggm) {
						if (eggm.error){
        				alert('No se puede obtener ubicacion actual');
        				return;
    					}
						updateChoferReq.open("POST","http://iego.com.mx/webservices/updatechofer.php");
        					params = {
  							id_chofer:Ti.App.Properties.getInt('id_chofer'),
                				latitud:eggm.coords.latitude,
                				longitud:eggm.coords.longitude
            				};
         				updateChoferReq.send(params);
         			    });
         			   			
			}else if (chkTerminos.value == false){
				
	
			Ti.Geolocation.getCurrentPosition(function(e) {
				if (e.error) {
					alert('No se puede obtener ubicacion actual');
					return;
				}

				Ti.App.fireEvent('fbController:coordsChofer', {
					id : id_chofer,
					latIn : e.coords.latitude,
					lgnIn : e.coords.longitude
				});
			}); 

				
				
				
				
				Titanium.App.Properties.removeProperty('placas');
				Titanium.App.Properties.removeProperty('Disponible');
				contador='';
				my_timer2.set(480,00);
					my_timer2.start();
					updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                		params = {
                			id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa: Ti.App.Properties.getString("placa"),
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:0,
                       };
                   	    // updateAuReq.send(params);
				
     				Ti.App.fireEvent('fbController:disponibilidad',{
						dis:0,
						id:id_chofer
					});
					
			
			}
  			Ti.API.info('Switch value: ' + chkTerminos.value);	
	});
	updateChoferReq.onload = function(){
		json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == true){
    			
        	}else{
    			alert(response.message);
    		}
	};
	
	var handleLocation = function(e) {
			if (e.error)
			{
				Ti.API.info("Code Error: " + JSON.stringify(e.error));
				
				return;
			}
			var longitude = e.coords.longitude;
			var latitude = e.coords.latitude;
			
			if (chkTerminos.value == true) {
					
					Ti.App.fireEvent('fbController:coordsChofer',{
						id:id_chofer,
						latIn:latitude,
						lgnIn:longitude
			});
			var timeStamp = String(new Date().getTime());
			Ti.App.fireEvent('fbController:login',{
						id:id_chofer,
						id2:Ti.App.Properties.getInt("id_usuario"),
						nomuser:nch,
						timestamp:timeStamp
		});
			}else if (chkTerminos.value == false) {
				
				
			}
	};
	function gpsStart() {
    			if (Ti.Geolocation.locationServicesEnabled) {
        			Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
        			Ti.Geolocation.distanceFilter = 10;
        			Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
        			if (!locationAdded) {
            			Ti.Geolocation.addEventListener('location', handleLocation);
            			locationAdded = true;
        			}
    			} else {
        			alert('Please enable location services');
    			}
	}
	
   		function abrirmapa(){
   			if(banderaruta==false){
   		if(Ti.App.Properties.getString('latUs')=='' || Ti.App.Properties.getString('longUS')=='' || Ti.App.Properties.getString('latD')=='' || Ti.App.Properties.getString('longD')=='' || Ti.App.Properties.getString('txtdireccion')==''){
   			
   		}else{
   			
   		var rs=Ti.App.Properties.getString('txtdireccion');
	  	var rsS= rs.substring(0,50);		
   		leftButtonOrigen.backgroundImage='/images/green.png';		
		latUs=Ti.App.Properties.getString('latUs');
   		longUS=Ti.App.Properties.getString('longUS');
   		latD=Ti.App.Properties.getString('latD');
   		longD=Ti.App.Properties.getString('longD');
		txtdireccion.text=rsS+'...';				
		
			
			txtvistarutaActual.text='ORIGEN';	
   			txtvistarutaActual2.text=Ti.App.Properties.getString('txtdireccion');	
   		 			
   			var origin = String(latUs + ',' + longUS),
            travelMode = 'driving',
            destination = String(latD + ',' + longD),
            url = "https://maps.googleapis.com/maps/api/directions/xml?mode="
            + travelMode + "&origin="
            + origin + "&destination="
            + destination +"&alternatives=false&avoid=indoor&sensor=false";
            Ti.API.info('url:' + url);
            xhr = Titanium.Network.createHTTPClient();
            xhr.open('GET',url);
             
            Titanium.Geolocation.reverseGeocoder(Ti.App.Properties.getString('latD'),Ti.App.Properties.getString('longD'),function(evt){
					if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							geocoderlbl.text = places[0].address;
						} else {
							geocoderlbl.text = "No address found";
						}
						Ti.API.debug("reverse geolocation result = "+JSON.stringify(evt));
						
					}
					else {
						Ti.UI.createAlertDialog({
							title:'Conexión inestable',
							message:evt.error
						}).show();
				
					}
					
				});
            
        }    
   		}else{
   			
   		if(Ti.App.Properties.getString('latD')=='' || Ti.App.Properties.getString('longD')=='' || Ti.App.Properties.getString('latD2')=='' || Ti.App.Properties.getString('longD2')=='' || Ti.App.Properties.getString('txtdireccion2')==''){
   			
   		}else{
   		var rs=Ti.App.Properties.getString('txtdireccion2');
	  	var rsS= rs.substring(0,50);		
   		leftButtonOrigen.backgroundImage='/images/red.png';	
   		latUs=Ti.App.Properties.getString('latD');
   		longUS=Ti.App.Properties.getString('longD');	
   		latD=Ti.App.Properties.getString('latD2');
   		longD=Ti.App.Properties.getString('longD2');
   		txtdireccion.text=rsS+'...';
   		
   		
			txtvistarutaActual.text='DESTINO';	
   			txtvistarutaActual2.text=Ti.App.Properties.getString('txtdireccion2');	  	
   			
   			var origin = String(latUs + ',' + longUS),
            travelMode = 'DRIVING',
            destination = String(latD + ',' + longD),
            url = "https://maps.googleapis.com/maps/api/directions/xml?mode="
            + travelMode + "&origin="
            + origin + "&destination="
            + destination +"&alternatives=false&avoid=indoor&sensor=false";
            Ti.API.info('url:' + url);
            xhr = Titanium.Network.createHTTPClient();
            xhr.open('GET',url);
           
            Titanium.Geolocation.reverseGeocoder(Ti.App.Properties.getString('latD2'),Ti.App.Properties.getString('longD2'),function(evt){
					if (evt.success) {
						var places = evt.places;
						if (places && places.length) {
							geocoderlbl.text = places[0].address;
						} else {
							geocoderlbl.text = "No address found";
						}
						Ti.API.debug("reverse geolocation result = "+JSON.stringify(evt));
						
					}
					else {
						Ti.UI.createAlertDialog({
							title:'Conexión inestable',
							message:evt.error
						}).show();
					
					}
					
				});
   		}
   	}
   	
   	xhr.send();
   	
   	  xhr.onload = function(e){
        	Ti.App.fireEvent('fbController:saldoagotado2', {
			id:id_chofer
			});
                    var xml = this.responseXML,
                    points = [],
                    steps = xml.documentElement.getElementsByTagName("step"),
                    totalSteps = steps.length;
                    for (var i=0; i < totalSteps; i++) {
                    	var duration = steps.item(i).getElementsByTagName("duration");
                    	durationText = duration.item(0).getElementsByTagName("text").item(0).text;
                    	duration = durationText.split(' ');
                    	var suma=parseInt(duration[0]);
                    	minutos=minutos+suma;
                    	var startLocation = steps.item(i).getElementsByTagName("start_location");
                        startLatitude = startLocation.item(0).getElementsByTagName("lat").item(0).text,
                        startLongitude = startLocation.item(0).getElementsByTagName("lng").item(0).text;     
                        points.push({latitude:startLatitude, longitude:startLongitude});                
                    }
                    minutosT=minutos;
                 //   alert(minutosT);
					var finalLocation = steps.item(totalSteps - 1).getElementsByTagName("end_location"),
                    finalLatitude = finalLocation.item(0).getElementsByTagName("lat").item(0).text,
                    finalLongitude = finalLocation.item(0).getElementsByTagName("lng").item(0).text; 
                    
                    points.push({latitude:finalLatitude, longitude:finalLongitude});
 
                    // route = Map.createRoute({name:"bonVoyage",points:points,color:"#319df0",width:6}), 
                    // startAnnotation = Map.createAnnotation({image:'/images/marcador.png',latitude: points[0].latitude,longitude: points[0].longitude,title: 'Posición actual'}),
                    // endAnnotation = Map.createAnnotation({image:'/images/bandera.png',latitude: points[points.length - 1].latitude,longitude: points[points.length - 1].longitude,title: 'Destino'});
//                  
                    // mapview.addRoute(route);
                    // mapview.addAnnotation(startAnnotation);
                    // mapview.addAnnotation(endAnnotation);	
                     
                    viewText.visible=true;
                   
					mapview.add(viewText);
                    Ti.Platform.openURL('http://maps.google.com/maps?t=m&saddr=' + latUs + ',' + longUS+'&daddr='+ latD + ',' + longD + '&dirflg=d');
			
            };
   }
   		
   		
 
      
      
	window.addEventListener('open', function() {
		var my_timer3 = new countDown(00, 10, function() {
			if (my_timer3.time.s == 0) {
				if (Ti.App.Properties.getInt('solicitudAceptada') == 1) {
					
					if (Ti.App.Properties.getInt('yallego') == 1) {
						banderaruta = true;
						btnlogin.animate(animatellegueEnd);
						btnViaje.visible = true;
						btnViaje.animate(animateviaje);
						geocoderlbl.text = '';
						geocoderlbl.visible = false;
						txtdireccion.text='';
						viewText.visible=false;

					} else {
						abrirmapa();
						txtdireccion.visibe=true;
						btnlogin.visible = true;
						btnlogin.animate(animatellegue);
					}

				}else{
					Ti.App.Properties.setString('latUs','');	
					Ti.App.Properties.setString('longUS','');	
					Ti.App.Properties.setString('latD','');
					Ti.App.Properties.setString('longD','');	
					Ti.App.Properties.setString('txtdireccion','');
					Ti.App.Properties.setString('latD2','');
					Ti.App.Properties.setString('longD2','');	
					Ti.App.Properties.setString('txtdireccion2','');
	
				}
				my_timer3.stop();
				my_timer3.set(00, 10);
			}
		}, function() {
		});
		my_timer3.start();

		var viewChildren = viewWeb.children.slice(0);
		for (var i = 0; i < viewChildren.length; i++) {
			var child = viewChildren[i];
			viewWeb.remove(child);
		}
		if (Ti.App.Properties.hasProperty('Disponible')) {

			Ti.App.fireEvent('fbController:disponibilidad', {
				dis : 1,
				id : Ti.App.Properties.getInt('id_chofer')
			});
			chkTerminos.value = true;
		}

		updateAuReq.open("POST", "http://iego.com.mx/webservices/updateauto.php");
		params = {
			id_chofer : Ti.App.Properties.getInt('id_chofer'),
			placa : Ti.App.Properties.getString("placa"),
			status_login : 1,
			status_ocupado : 0,
			status_solicitud : 0,
			status_disponibilidad : 0,

		};
		updateAuReq.send(params);
		if (chkTerminos.value == false) {
			updateAuReq.open("POST", "http://iego.com.mx/webservices/updateauto.php");
			params = {
				id_chofer : Ti.App.Properties.getInt('id_chofer'),
				placa : Ti.App.Properties.getString("placa"),
				status_login : 1,
				status_ocupado : 0,
				status_solicitud : 0,
				status_disponibilidad : 0,

			};
			updateAuReq.send(params);
			my_timer2.start();
		}

		gpsStart();
		open();
	}); 

	
	function clickMenu(direction){
     if(menuView._toggle === false && (direction==null || direction=='right')){
        centerView.animate({
                right:'-75%' ,
                duration: 100,
                curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
            });
            menuView.animate({
                left: 0,
                duration: 100,
                curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
            });
            
            menuView._toggle=true;
        }else if(direction==null || direction=='left'){
            centerView.animate({
                right:0 ,
                duration: 100,
                curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
            });
            menuView.animate({
                left: '-75%',
                duration: 100,
                curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT
            });        menuView._toggle=false;
        };
    }
    
    viewPro.add(display_lbl);
    viewPro.add(buscando_lbl);
 
    window.add(viewPro);  
    
    header.add(iconHeader);
   // header.add(iconShare);
    header.add(titulo);
    centerView.add(header);
    centerView.add(btnlogin);
	centerView.add(btnViaje);
	centerView.add(btnFinalizar);
	
    window.add(menuView);
   	window.add(centerView);
    menuView.add(logoLeft);
    menuView.add(lblHome);
    menuView.add(lblMisPedidos);
    menuView.add(lblMiPerfil);
    menuView.add(chkTerminos);
	menuView.add(disponible); 
	centerView.add(viewWeb);
	window.add(nuevaSolicitud);
   	window.add(viajecancelado);
   	window.add(saldoinsuficiente);
   	window.add(noaceptado);
	
	return window;
};
module.exports = mapa;