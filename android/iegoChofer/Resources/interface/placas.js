
	function placas(){

	var window,vista,ImgPerfil,btnGuardar,txtPlaca,btnCancelar;

	autoUpdateReq = Titanium.Network.createHTTPClient();
	 
	
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor :'transparent',opacity: 2});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];

	vista=Ti.UI.createView({left:'5%',right:'5%',top:'30%',bottom:'30%',backgroundColor:'white'});
	texto= Ti.UI.createLabel({ center:0,width :'auto', height : 'auto', top : '3%' ,text : 'Escriba una placa',color:'black',font  : { fontSize : '25%'}});
	ImgPerfil= Ti.UI.createImageView({width:'21%',height:'26.5%',borderRadius:50,top:'21.5%',center:0,image:"http://iego.com.mx/images/"+Ti.App.Properties.getString('fotografia'),zIndex:3});
	txtPlaca= Ti.UI.createTextField({hintText:'Escriba su placa aqui',top:'50%',center:0,width:'80%',heigth:'auto',color:'black',hintTextColor:'gray'});
    btnGuardar = Ti.UI.createButton({title:('Aceptar'),bottom:'5%',center:0,width:'auto',heigth:'auto',textAlign:'center',backgroundColor:'#a00a1c',color:'white'});
  	//btnCancelar = Ti.UI.createButton({title:('Cancelar'),bottom:'5%',right:'10%',width:'auto',heigth:'auto',textAlign:'center',backgroundColor:'#a00a1c',color:'white'});
  	
  	  
	vista.add(texto);
    vista.add(ImgPerfil);
    vista.add(txtPlaca);
    vista.add(btnGuardar);
    window.add(vista);
	window.addEventListener('androidback', function(e){
  	Ti.App.placa.value=false;
  	window.close();
   });	
   
	btnGuardar.addEventListener('click', function(e){
		var pla;
		if(txtPlaca.value!=''){
			autoUpdateReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
                          params = {
                            id_chofer:Ti.App.Properties.getInt('id_chofer'),
                			placa:txtPlaca.value,
                    		status_login:1,
                        	status_ocupado:0,
                        	status_solicitud:0,
                        	status_disponibilidad:1,
                          };
                           autoUpdateReq.send(params);
                               
		autoUpdateReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString('placa',response.placa);
            	Ti.App.Properties.setString('placas',1);
            	Ti.App.Properties.setInt("tipo_auto", response.tipo_servicio);
            	alertDialog = Titanium.UI.createAlertDialog({title: 'Alert',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
                alertDialog.addEventListener('click',function(e){
                	Ti.App.fireEvent('fbController:tiposervicio',{
						id:Ti.App.Properties.getInt('id_chofer'),
						tiposervicio:Ti.App.Properties.getInt('tipo_auto')
					});
                	window.close();
                });
            }else{
                    alert(response.message);
             }
    	};
		}else{
			Ti.App.placa.value=false;
			window.close();
		}
		
                               
                               
                               
      

	});
	

	return window;
	};
	module.exports = placas;