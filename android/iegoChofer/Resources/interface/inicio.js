function inicio(){
	var window,logo,btnLogin,scrollableView,navwin,login,winNav,animateWin;
    
    window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar'});
    window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
    video1 = Titanium.Media.createVideoPlayer({top : 0,width : '100%',scalingMode : Titanium.Media.VIDEO_SCALING_MODE_FILL,url:'/images/v1.mp4',mediaControlStyle: Titanium.Media.VIDEO_CONTROL_HIDDEN});
    logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'60%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
    btnLogin = Titanium.UI.createButton({height:'10%',width:'100%',left:0,title:'INICIAR SESIÓN',textAlign:'center',backgroundColor:'#cc4a4a',color:'white',bottom:0,zIndex:3});
    
    window.addEventListener('open',function(){
    	Titanium.App.Properties.removeProperty('placas');
    		function getGeoPosition(_event) {
            
    		}
    		Ti.Geolocation.purpose = "Please allow us to find you.";
    		Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
    		Ti.Geolocation.distanceFilter = 10;
    		function geolocation() {
    			var hasLocationPermissions = Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS);
    			if (hasLocationPermissions) {
        			Titanium.Geolocation.getCurrentPosition(getGeoPosition);
    			}
    			Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_ALWAYS, function(e) {
        			if (e.success) {
        			}else if (Ti.Platform.osname == 'android') {       
            			alert('Debe permitir a la aplicacion acceder a su localización.');
        			}else {
            			Ti.UI.createAlertDialog({title : 'Has denegado el permiso de localizacion.',
                			message : e.error
            			}).show();
        			}
    			});
		}
		geolocation();

		animateLogo = Titanium.UI.createAnimation();
        animateLogo.top = '16%';
        animateLogo.duration = 2000;
        animateLogo.opacity = 1;
        logo.animate(animateLogo);
    });
    video1.addEventListener('complete', function(e){
		video1.play();
	});
	btnLogin.addEventListener('click', function(){
        login = require('/interface/login');
        new login().open({modal:true});
        
    });
    
    window.add(logo);
    window.add(btnLogin);
    window.add(video1);
    
    return window;
};
module.exports = inicio;