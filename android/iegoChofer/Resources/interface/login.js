function login(){
	var window,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	
	window = Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundImage:'/images/bglogin.jpg'});
    window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
    lblUser = Titanium.UI.createLabel({text:'Usuario',center:0,top:'20%',color:'white'});
    txtUser = Titanium.UI.createTextField({color:'#ffffff',hintText:'Introduzca su usuario',opacity:0.3,center:0,top:'26%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
    lblpass= Titanium.UI.createLabel({text:'Contraseña',center:0,top:'36%',color:'white'});
    txtPass = Titanium.UI.createTextField({passwordMask:true,hintText:'***********',top:'42%',color:'#ffffff',opacity:0.3,center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',backgroundFocusedColor:'transparent',font:{fontSize:12}});
    btnlogin = Titanium.UI.createButton({title:'Iniciar Sesión',color:'white',borderRadius:30,borderColor:'white',top:'55%',width:'85%',height:60,backgroundColor:'#757575'});
    lblreset = Titanium.UI.createLabel({text:'Recuperar Contraseña',color:'#c2c2c2',top:'70%',center:0});
    loginReq = Titanium.Network.createHTTPClient();
    resetReq = Titanium.Network.createHTTPClient();
    updateAuReq	= Titanium.Network.createHTTPClient();
    
    
    txtUser.addEventListener('focus',function(){
			txtUser.value = '';
			txtUser.opacity=1;
    	});
    	txtPass.addEventListener('focus',function(){
        	txtPass.value = '';
        	txtPass.opacity=1;
        	
    	});
    	btnlogin.addEventListener('click',function(e){
    		
            if (txtUser.value != '' && txtPass.value != ''){
                    loginReq.open("POST","http://iego.com.mx/webservices/loginChofer.php");
                    params = {
                        username: txtUser.value,
                        password: Ti.Utils.md5HexDigest(txtPass.value)
                    };
                    loginReq.send(params);
            }else{
                    alert("Usuario y password son requeridos");
                }
    	});
    	
    	   	
	lblreset.addEventListener('click', function(){
        	if (txtUser.value == ''){
        		
        		 	alert('Se requiere su usuario para restaurar la contraseña');
        	       
            }else{
                resetReq.open('POST','https://iego.com.mx/webservices/sendemail.php');
            params = {
                    names: txtUser.value
            };
                resetReq.send(params);
            }
    	});
    	
    	
    	
    
    	
    	loginReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setInt("id_chofer", response.id_chofer);
                Ti.App.Properties.setInt("id_usuario", response.id_usuario);
                Ti.App.Properties.setString("id_choferS", response.id_chofer);
                Ti.App.Properties.setString("usuario", response.usuario);
                Ti.App.Properties.setString("email", response.email);
                Ti.App.Properties.setString("telefono", response.telefono);
                 Ti.App.Properties.setString("fotografia", response.fotografia);
                 Ti.App.Properties.setString("placa",response.placa);
                
                
                mapa = require('/interface/mapa');
                new mapa().open({modal:true});
                window.close();
                }else{
                    alert(response.message);
                }
	};
	
	resetReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.mail == true){
                alert('Se le ha enviado un correo para restaurar su contraseña');
            }else{
                alert(response.message);    
            }
    	};
    	resetReq.onerror = function(event){
            alert('Error de conexion: ' + JSON.stringify(event));
    	};
    	window.addEventListener("open", function(evtop) {
        	var theActionBar = window.activity.actionBar; 
        	if (theActionBar != undefined) {
            theActionBar.backgroundColor = '#373136';
        	theActionBar.title = "Inicio de sesion";
         	}
    });
    
    window.add(lblUser);
    	window.add(txtUser);
    	window.add(lblpass);
    	window.add(txtPass);
    	window.add(lblreset);
    	window.add(btnlogin);
    	
    	return window;
};
module.exports = login;