function pedidos(){
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;
	
	    window= Ti.UI.createWindow({theme:'Theme.AppCompat.Translucent.NoTitleBar',backgroundColor:'white'});
	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];
	// Vista Lateral
	menuView = Ti.UI.createView({backgroundImage:'/images/bgmenu.png',height: 'auto',left: '-75%',	top:0,width: '75%',zIndex: 9,	_toggle: false});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'55%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'red',text:'Historial',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	
    disponible = Ti.UI.createLabel({color:'white',text:'Disponible',top:'76%',left:'13%'});

	centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:1});
	header = Ti.UI.createView({backgroundImage: '/images/barra.jpg',height: '10%',right:0,top: 0,width:'100%',zIndex: 10});
	iconHeader = Ti.UI.createButton({	backgroundImage:'/images/menuicon.png', left:'3%',zIndex:2,height:'42%',width:'8.5%'});
	iconEdit = Ti.UI.createButton({	backgroundImage:'/images/editarP.png', left:'88%',zIndex:2,height:'53%',width:'8%'});
  	titulo=Ti.UI.createLabel({text:'Mis Pedidos', color:'#232323',center:0,bottom:'25%',zIndex:2,font:{fontSize:'15.5%'}});
	 fondoP = Ti.UI.createView({backgroundImage: '/images/logo3.png',height: '24%',left:0,top:'10%',width:'auto',zIndex: 2});
	
	lblHome.addEventListener('click',function(){
		mapa = require('/interface/mapa');
		new mapa().open({modal:true});
		window.close();
	});
	
	lblMiPerfil.addEventListener('click',function(){
		perfil = require('/interface/perfil');
		new perfil().open({modal:true});
		window.close();});
		
	args = arguments[0] || {};url = "http://iego.com.mx/webservices/pedidosChofer.php";db = {};
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					alert('Comprueba tu conexion a internet');
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			var params = {
    				id_chofer:Ti.App.Properties.getInt('id_chofer')
    			};
			xhr.send(params);
		}
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				Ti.API.info(db.servicios[i].nombre_chofer);
				row = Ti.UI.createTableViewRow({bottom:'5%',height:"100dp",backgroundColor:"white",backgroundSelectedColor:"blue"});
				horario_inicio = Ti.UI.createLabel({text:db.servicios[i].hora_inicio,color:'#151431',left:'10%',top:'30%',font:{fontSize:18,fontWeight:'bold'}});
				vistaimg= Ti.UI.createView({width:65,height:65,borderRadius:30, top:'15%', bottom:15, left:'35%'});
				imgChofer = Ti.UI.createImageView({image:"http://iego.com.mx/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20'),width:'100%',height:'100%',borderRadius:50});
				fecha =Ti.UI.createLabel({text:db.servicios[i].fecha,color:'#151431',left:'55%',top:'10%'});
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].lugar_origen,color:'#151431',left:'55%',top:'28%',font:{fontSize:12}});
				nombre_chofer = Ti.UI.createLabel({text:'Conductor: '+db.servicios[i].nombre_chofer,color:'#a2a1b8',left:'55%',top:'70%',font:{fontSize:12}});
				status = Ti.UI.createView({width:15,height:15,borderRadius:7,top:'34%', left:'2%'});
				if (db.servicios[i].status_servicio == 1) {
						status.backgroundColor = '#09c863';
				}else if (db.servicios[i].status_servicio == 0){
						status.backgroundColor = '#a00a1c';
				}
				
				vistaimg.add(imgChofer);
				row.add(status);
				row.add(fecha);
				row.add(vistaimg);
				row.add(horario_inicio);
				row.add(lugar_origen);
				row.add(nombre_chofer);
				tableData.push(row);
			}
			tableView = Ti.UI.createTableView({data:tableData,top:'34%',bottom:0,scrollable:true});
			centerView.add(tableView);
		}
		
		
		lblMisPedidos.addEventListener('click',function(event){
			clickMenu(null);
		});
	
		iconHeader.addEventListener('click',function(){
    		clickMenu(null);
		});
		
		centerView.addEventListener('swipe',function(event){
			if(event.direction=='left' || event.direction=='right' ){
			clickMenu(null);
			}	
		});
				
		function clickMenu(direction){
	 		if(menuView._toggle === false && (direction==null || direction=='right')){
				centerView.animate({right:'-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView.animate({left:0,duration: 100,	curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT	});
			
			menuView._toggle=true;
			}else if(direction==null || direction=='left'){
				centerView.animate({right:0,duration:100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView.animate({left: '-75%',duration: 100,curve:Ti.UI.ANIMATION_CURVE_EASE_IN_OUT});
				menuView._toggle=false;
			};
		}
	
			header.add(iconHeader);
			header.add(iconEdit);
	   		header.add(titulo);
	  		centerView.add(fondoP); 
	  		centerView.add(header);
			menuView.add(logoLeft);
			menuView.add(lblHome);
			menuView.add(lblMisPedidos);
			menuView.add(lblMiPerfil);
	
			//menuView.add(disponible);
			window.add(menuView);
	   		window.add(centerView);	
			getData();
			return window;
};
module.exports = pedidos;
