if(Ti.version < 2.1){
	alert('Lo sentimos esta aplicacion requiere una version 2.1 o superior');
}
(function() {
	if (Ti.App.Properties.hasProperty('usuario')) {
		if (Ti.App.Properties.hasProperty('viajeactivo')) {
			inicio = require('interface/chofer');
		}else{
			inicio = require('interface/mapa2');	
		}
	}else{
			inicio = require('interface/inicio');
	}
	if (Ti.App.Properties.hasProperty('viajeactivo')) {
		inicio = new inicio(Ti.App.Properties.getString('latInicio'),Ti.App.Properties.getString('lngInicio'),Ti.App.Properties.getString('latDestino'),Ti.App.Properties.getString('lngDestino'),Ti.App.Properties.getString('geocoderlbl'),Ti.App.Properties.getString('tiposervicio'),Ti.App.Properties.getString('idTipoServicio'),Ti.App.Properties.getString('opcionservicio'),Ti.App.Properties.getString('geocoderDestino'));
	}else{
		inicio = new inicio();
	}
	inicio.open();	
})();