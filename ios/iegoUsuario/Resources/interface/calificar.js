function calificar(){
	var window,vista,ImgPerfil,btnCalificar,texto,valorS,calChofer,calAuto,calSeguridad,calificacion,comentario,id_servicio,id_usuario,id_chofer;
	calificaServ = Titanium.Network.createHTTPClient();
	id_servicio1=Ti.App.Properties.getInt("id_servicio");
  	id_usuario1=Ti.App.Properties.getInt("id_usuario");
  	id_chofer1= Ti.App.Properties.getInt("id_ChoferA");
  	
  	window = Ti.UI.createWindow({backgroundColor :'white',opacity: 1});
 	window .orientationModes = [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT];

	vista=Ti.UI.createView({height:'100%',width:'100%'});
	
  	texto= Ti.UI.createLabel({ center:0,width :'auto', height : 'auto', top : '10%' ,text : 'Califica el servicio',color:'black',font  : { fontSize : '35%'}});
	ImgPerfil= Ti.UI.createImageView({width:'31%',height:'19.5%',borderRadius:50,top:'21.5%',center:0,defaultImage:'/images/foto.png',image:'/images/foto.png',zIndex:3});
	calChofer= Ti.UI.createTextField({ center:0,width :'70%', height : 'auto', top : '40%' ,hintText:'Calificación Chofer',color:'black',hintTextColor:'gray'});
	btnComentario= Ti.UI.createTextField({hintText:'Dejar comentario',top:'50%',center:0,width:'80%',heigth:'auto',color:'black',hintTextColor:'gray'});
    btnCalificar = Ti.UI.createButton({title:('Aceptar'),bottom:'10%',center:0,width:'50%',heigth:25,textAlign:'center',backgroundColor:'#a00a1c',color:'white'});
    var rating;var dialog = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Gracias por calificar este servicio',title: 'Calificación del servicio'});
 			
	ui = require('/interface/ui');
	var ratingView = new ui.RatingView(1, 5);
		ratingView.top = 380;
		ratingView.left = 40;
		ratingView.addEventListener('ratingChanged', function(erb) {
			rating = erb.currentValue;
		});
		btnCalificar.addEventListener('click', function(ed){
			calificaServ.open("POST","http://iego.com.mx/webservices/calificacion.php");
            	params = {
            		calificacion:rating,
                comentario:btnComentario.value,
                id_servicio:id_servicio1,
                id_usuario:id_usuario1,
                id_chofer:id_chofer1
           	};
            calificaServ.send(params);
		});
		
		calificaServ.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            		dialog.show();
            }else{
    			}
        };
        
        dialog.addEventListener('click', function(ed){
        		mapa2 = require('/interface/mapa2');
    			new mapa2().open();
			window.close();
		});
		
		vista.add(texto);
       vista.add(ImgPerfil);
       vista.add(btnComentario);
       vista.add(ratingView);
       vista.add(btnCalificar);
	window.add(vista);
	return window;
};
module.exports = calificar;