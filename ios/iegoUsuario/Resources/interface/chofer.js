function chofer(latInicio,lngInicio,latDestino,lngDestino,geocoderlbl,tiposervicio,idTipoServicio,opcionservicio,geocoderDestino){
	//Variables
	var window,centerView,vistaMapa,viewWeb,horarioTarifa,tarifaInicial,kmCA,Map,viewPro,countDown,my_timer,dialogChofer;var idch = Ti.UI.createLabel({});
	var mapview;	var annotations = [];var annotation;var locationAdded = false;var latitude;var longitude;var idch = Ti.UI.createLabel({});
	var sdial=false;var sdial2=false;var sdial3=false;var sdial4=false;args = arguments[0] || {};url = "http://iego.com.mx/webservices/choferprueba.php";db = {};
	var banderatimer = false;var sa=0,idc,contadorBusqueda=0;tarifaFinal=0;tarifaA=0;var choferactivo;var horaActual,dialogAceptado,dialogYallegue,
		dialogViajeiniciado,dialogFinalizado,dialogCancelado,dialCalif;var costotpay =Titanium.UI.createLabel({});var solicitudTimer= false;var banderacancelar = false;
	
	//Modulos
	Map = require('ti.map');
	
	//Ventana principal
	window = Ti.UI.createWindow({backgroundColor:'white'});
	centerView =Ti.UI.createView({height: 'auto',right:0,top: 0,width:'100%',zIndex:5});
	vistaMapa =Ti.UI.createView({height: '40%',right:0,bottom: 0,width:'100%',zIndex:6});
	viewWeb= Ti.UI.createView({backgroundColor:'WHITE'});
	
	//Alertas
	dialogChofer = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Chofer no encontrado. Favor de intentarlo nuevamente',title: 'Busqueda'});
	dialogAceptado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Tu solicitud ha sido atendida, verifique la ficha',title: 'Ficha técnica'});
	dialogYallegue = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Tu auto ha llegado.',title: '!Hola¡ Ya llegué'});
	dialogViajeiniciado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Tu viaje inició, pongase cómodo.',title: 'Viaje iniciado'});
	dialogFinalizado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Tu viaje ha finalizado.',title: 'Viaje finalizado'});
	dialogCancelado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: 'Se le cobrará un 30% del valor total del viaje, ¿desea continuar?',title: 'Cancelar Viaje'});
	dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Lo sentimos,no cuenta con saldo suficiente para realizar el viaje',title: 'Notificación'});
	
	//Temporizador
	viewPro= Ti.UI.createView({backgroundColor:'WHITE', widht:'100%',height:'100%',zIndex:10});
	logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'60%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
	track = Ti.UI.createView({ width: "80%", height: "2%", borderRadius:10, backgroundColor: '#c9ecfd',top:'50%'});
  	progress = Ti.UI.createView({ borderRadius:10, left: 0, width: '100%', height: "100%", backgroundColor : '#007fff'});
 	display_lbl =  Titanium.UI.createLabel({	text:"03:00",font:{fontSize:'65%'},top:'55%',center:0,color:'#c2c2c2 ',borderRadius:10,textAlign:'center'});
	buscando_lbl =  Titanium.UI.createLabel({text:"Buscando chofer...",font:{fontSize:'26%'},top:'40%',center:0,color:'#c2c2c2 ',	borderRadius:10,textAlign:'center'});
	
	//Webservices
	pagoReq = Titanium.Network.createHTTPClient();
	pagoCan = Titanium.Network.createHTTPClient();
	endServReq = Titanium.Network.createHTTPClient();
	registerServ = Titanium.Network.createHTTPClient();
	recibo = Titanium.Network.createHTTPClient();
	sisno = Titanium.Network.createHTTPClient();var id_usuario;
	
	//Interfaz grafica
	player = Ti.Media.createSound({url:"/sound/sonido.mp3"});
	webview = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false});
	btnCanViaje = Titanium.UI.createButton({title:'Cancelar Viaje', center:0,height:40,width:'50%',bottom:'5%',zIndex:10,enabled:true, backgroundColor:'white'});
	telefono = Ti.UI.createLabel({});keychofer = Ti.UI.createLabel({});
    viewLogo = Titanium.UI.createImageView({width:'100%',height:'auto',image:'/images/logo4.png',top:0,left:0,zIndex:3});
	vistaimg= Ti.UI.createView({width:112,height:112,borderRadius:67, top:'12%', bottom:15, center:0,zIndex:8});
    ImgPerfil= Ti.UI.createImageView({width:112,height:105,image:'/images/foto.png'});
    
    countDown =  function( m , s, fn_tick, fn_end  ) {
		return {
			total_sec:m*60+s,
			timer:this.timer,
			set: function(m,s) {
				this.total_sec = parseInt(m)*60+parseInt(s);
				this.time = {m:m,s:s};
				return this;
			},
			start: function() {
				var self1 = this;
				this.timer = setInterval( function() {
					if (self1.total_sec) {
						self1.total_sec--;
						self1.time = { m : parseInt(self1.total_sec/60), s: (self1.total_sec%60) };
						fn_tick();
					}else {
						self1.stop();
						fn_end();
					}
		   		}, 1000 );
				return this;
			},
			stop: function() {
				clearInterval(this.timer);
				this.time = {m:0,s:0};
				this.total_sec = 0;
				return this;
			}
		};
	};
	
	my_timer = new countDown(03, 00, function() {
		if (my_timer.time.m == 0 && my_timer.time.s == 0) {
			my_timer.stop();
			dialogChofer.show();
			display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
		}else if (my_timer.time.m == 2 && my_timer.time.s == 55) {
			viewWeb.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m <= 3) && (my_timer.time.m == 2 && my_timer.time.s >= 10)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 2 && my_timer.time.s == 9) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
				sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       			params = {
        			idusuario:id_usuario,
            		mensaje: 'No has aceptado un servicio',
            		titulo: 'Servicio no aceptado',
            		statussolicitud:0,
            		viajecancelado:0,
            		saldoinsuficiente:0,
            		noaceptado:1
        		};
        		sisno.send(params); 
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if (my_timer.time.m == 2 && my_timer.time.s == 8) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			}else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 2 && my_timer.time.s >= 4) && (my_timer.time.m == 2 && my_timer.time.s < 8)) {
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 2 && my_timer.time.s == 3) {
			viewWeb.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if (  (my_timer.time.m == 2 && my_timer.time.s < 3 && my_timer.time.m == 2 && my_timer.time.s >=0) || (my_timer.time.m == 1 && my_timer.time.s >= 17 && my_timer.time.m == 1 && my_timer.time.s <= 59)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 1 && my_timer.time.s == 16) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
				sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       			params = {
        			idusuario:id_usuario,
            		mensaje: 'No has aceptado un servicio',
            		titulo: 'Servicio no aceptado',
            		statussolicitud:0,
            		viajecancelado:0,
            		saldoinsuficiente:0,
            		noaceptado:1
        		};
        		sisno.send(params); 
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 1 && my_timer.time.s == 15) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 1 && my_timer.time.s >= 11) && (my_timer.time.m == 1 && my_timer.time.s < 15)) {
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 1 && my_timer.time.s == 10) {
			viewWeb.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if ((my_timer.time.m == 0 && my_timer.time.s >=34 && my_timer.time.m == 0 && my_timer.time.s <=59) || (my_timer.time.m == 1 && my_timer.time.s <10 && my_timer.time.m == 1 && my_timer.time.s >=0)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			}else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 33) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
				sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       			params = {
        			idusuario:id_usuario,
            		mensaje: 'No has aceptado un servicio',
            		titulo: 'Servicio no aceptado',
            		statussolicitud:0,
            		viajecancelado:0,
            		saldoinsuficiente:0,
            		noaceptado:1
        		};
        		sisno.send(params); 
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			}else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		} else if (my_timer.time.m == 0 && my_timer.time.s == 32) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 0 && my_timer.time.s >= 30) && (my_timer.time.m == 0 && my_timer.time.s < 32)) {
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 29) {
			window.add(webview);
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if ((my_timer.time.m == 0 && my_timer.time.s >=3 ) && (my_timer.time.m == 0 && my_timer.time.s < 29)) {
			if (sa == 1) {
				my_timer.stop();
				getData();
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 2) {
			if (sa == 0) {
				Ti.App.fireEvent('fbController:noAceptado', {
					id_chofer : idc
				});
				sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       			params = {
        			idusuario:id_usuario,
            		mensaje: 'No has aceptado un servicio',
            		titulo: 'Servicio no aceptado',
            		statussolicitud:0,
            		viajecancelado:0,
            		saldoinsuficiente:0,
            		noaceptado:1
        		};
        		sisno.send(params); 
        		idc = "";
			}
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}else if (my_timer.time.m == 0 && my_timer.time.s == 1) {
			viewWeb.remove(webview);
			mapview.removeAllAnnotations();
			if (my_timer.time.s < 10) {
				display_lbl.text = '0' + my_timer.time.m + ":0" + my_timer.time.s;
			} else {
				display_lbl.text = '0' + my_timer.time.m + ":" + my_timer.time.s;
			}
		}
	}, function() {
	}); 
	
	//Eventos
	Ti.Geolocation.getCurrentPosition(function(e) {
		if (Ti.App.Properties.hasProperty('viajeactivo')) {
			viewWeb.add(webview);
			webview.addEventListener('load', function() {
				Ti.App.fireEvent('firebase:init', {
					latitude : e.coords.latitude,
					longitude : e.coords.longitude,
					id_chofer:Ti.App.Properties.getInt('id_ChoferA'),
					latInicio:latInicio,
					lngInicio:lngInicio,
					latDestino:latDestino,
					lngDestino:lngDestino,
					idTipoServicio:idTipoServicio,
					viajeactivo:0,
					id_usuario:Ti.App.Properties.getInt('id_usuario_chofer'),
					nombre:Ti.App.Properties.getString('nombrechofer'),
					id_user:Ti.App.Properties.getInt('id_usuario'),
					origen:geocoderlbl,
					destino:geocoderDestino
				});
			});
		}else{
			webview.addEventListener('load', function() {
				Ti.App.fireEvent('firebase:init', {
					latitude : e.coords.latitude,
					longitude : e.coords.longitude,
					id_chofer:0,
					latInicio:latInicio,
					lngInicio:lngInicio,
					latDestino:latDestino,
					lngDestino:lngDestino,
					idTipoServicio:idTipoServicio,
					viajeactivo:1,
					id_user:Ti.App.Properties.getInt('id_usuario'),
					origen:geocoderlbl,
					destino:geocoderDestino
				});
			});
		}
	});
	
	Ti.Geolocation.getCurrentPosition(function(e) {
		mapview = Map.createView({mapType : Map.NORMAL_TYPE,
			region : {latitude:e.coords.latitude,longitude:e.coords.longitude,latitudeDelta:0.009,longitudeDelta:0.009,userLocation:true,visible:true},
			animate : true,
			regionFit : true,
			userLocation : true
		});
		vistaMapa.add(mapview);
		if (!e.success || e.error) {
			Ti.API.info('error:' + JSON.stringify(e.error));
			return;
		}	
	});
	
	Ti.App.addEventListener('fbController:addMarker', function(e){
		annotations[e.id] = Map.createAnnotation({
			driverId : e.id,
			latitude : e.latitude,
			longitude : e.longitude,
			image : '/images/autochofer.png',
			title : 'Chofer: '+ e.nombre
		});
		mapview.addAnnotation(annotations[e.id]);
		mapview.region = {latitude:e.latitude, longitude:e.longitude, latitudeDelta:0.009, longitudeDelta:0.009};
		idc = e.id;Ti.App.Properties.setInt('id_usuario_chofer',e.id_usuario);Ti.App.Properties.setString('nombrechofer',e.nombre);
		if (Ti.App.Properties.hasProperty('viajeactivo')) {
			
		}else{
			sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       		params = {
        		idusuario:e.id_usuario,
            	mensaje: 'Tienes una nueva solicitud de servicio',
            	titulo: 'Solicitud de servicio',
           	 	statussolicitud:1,
            	viajecancelado:0,
            	saldoinsuficiente:0,
            	noaceptado:0
        	};
        	sisno.send(params); 
		}
	});
	
	Ti.App.addEventListener('fbController:moveMarker2', function(est){
		if (est.finalizarviaje == 1) {
   			if (sdial3 == false) {
   				sdial3 = true;
   				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
   				notification = Ti.App.iOS.scheduleLocalNotification({
    					alertAction: "ver",
    					alertBody: dialogFinalizado.message,
    					badge: 1,
    					date: new Date(new Date().getTime() + 3000),
    					sound: "/sound/sonido.mp3",
    					userInfo: {}
				}); 
   				dialogFinalizado.show();	
   				player.play();
   			}
   		}else if (est.finalizarviaje == 0){
			sdial3 = false;
		}
	});
	
	Ti.App.addEventListener('fbController:moveMarker', function(est){
		annotations[est.id].setLatitude(est.latitude);
		annotations[est.id].setLongitude(est.longitude);
		mapview.setRegion({latitude:est.latitude,longitude:est.longitude,latitudeDelta:0.003, longitudeDelta:0.003});
		id_usuario = est.id_usuario;
		if (est.servicioaceptado == 1) {
			sa=1; 	
		}else if (est.servicioaceptado == 0){
			sdial = false;
			sa=0;
		}
		if (est.yallegue == 1) {
			if (sdial2 == false) {
   				sdial2 = true;
   				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
   				notification = Ti.App.iOS.scheduleLocalNotification({
    					alertAction: "ver",
    					alertBody: dialogYallegue.message,
    					badge: 1,
    					date: new Date(new Date().getTime() + 3000),
    					sound: "/sound/sonido.mp3",
    					userInfo: {}
				}); 
   				dialogYallegue.show();
   				player.play();
   			}
		}else if (est.yallegue == 0){
			sdial2 = false;
		}
		if (est.viajeiniciado == 1) {
   			if (sdial4 == false) {
   				sdial4 = true;
   				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
   				notification = Ti.App.iOS.scheduleLocalNotification({
    					alertAction: "ver",
    					alertBody: dialogViajeiniciado.message,
    					badge: 1,
    					date: new Date(new Date().getTime() + 3000),
    					sound: "/sound/sonido.mp3",
    					userInfo: {}
				}); 
   				dialogViajeiniciado.show();
   				player.play();
   			}
   		}else if (est.viajeiniciado == 0){
			sdial4 = false;
		}
	});
	
	dialogYallegue.addEventListener('click', function(ed){
		if (ed.index === 0){
			sdial2 = true;
			player.stop();
			Titanium.App.Properties.removeProperty('yallegue');
			Ti.App.Properties.setString('viajeiniciado',0);
		}
	});
	
	dialogViajeiniciado.addEventListener('click', function(ed){
		if (ed.index === 0){
			sdial4 = true;
			player.stop();
			btnCanViaje.enabled=false;	
			Titanium.App.Properties.removeProperty('viajeiniciado');
			Ti.App.Properties.setString('finalizar',0);
		}
	});
	
	btnCanViaje.addEventListener('click',function(){
		dialogCancelado.show();
  		Ti.App.fireEvent('fbController:cancelarServicio', {
			id_chofer:idch.text
		});
	});
	
	dialogAceptado.addEventListener('click', function(ed){
		Ti.App.fireEvent('fbController:id_user', {
			id_chofer:idch.text,
			id_user:Ti.App.Properties.getInt('id_usuario')
		});
		sdial = true;player.stop();var tipo=1; Ti.App.Properties.setString('viajeactivo',0);
		Ti.App.Properties.setString('yallegue',0);
		Ti.App.Properties.setString('latInicio',latInicio);
		Ti.App.Properties.setString('lngInicio',lngInicio);
		Ti.App.Properties.setString('latDestino',latDestino);
		Ti.App.Properties.setString('lngDestino',lngDestino);
		Ti.App.Properties.setString('geocoderlbl',geocoderlbl);
		Ti.App.Properties.setString('tiposervicio',tiposervicio);
		Ti.App.Properties.setString('idTipoServicio',idTipoServicio);
		Ti.App.Properties.setString('opcionservicio',opcionservicio);
		Ti.App.Properties.setString('geocoderDestino',geocoderDestino);
		
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			registerServ.open("POST","http://iego.com.mx/webservices/createservice.php");
            params = {
            	id_tipo_servicio:idTipoServicio,
                id_usuario:Ti.App.Properties.getInt("id_usuario"),
                id_chofer:idch.text,
                latitud_origen:latInicio,
                longitud_origen:lngInicio,
                latitud_destino:latDestino,
                longitud_destino:lngDestino,
                lugar_origen:geocoderlbl,
                destino:geocoderDestino,
                opcion_servicio:opcionservicio,
                tarifa_inicial:tarifaInicial.text,
                id_tipo_pago:1,
                costo_total:Ti.App.Properties.getDouble('ctD'),
                idtransaccion:Ti.App.Properties.getString('idtransaccion')
           	};
            registerServ.send(params);
       	}
       	registerServ.onload = function(){
			json = this.responseText;
    		response = JSON.parse(json);
  			if (response.logged== true){
       	    	Ti.App.Properties.setInt("id_servicio", response.id_servicio);
    			Ti.App.Properties.setString("id_folio", response.folio_servicio);							
    			endServReq.open("POST","http://iego.com.mx/webservices/endServicios.php");
                params = {
                	id_servicio: Ti.App.Properties.getInt("id_servicio"),
                    km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
                    costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
                    hora_extra: "00:00:00",
                    costo_hora_extra: 0,
                    cargo_hora_pico: 0,
                    tiempo_espera_adicional:"00:00:00",
                    costo_espera_adicional: 0,
                    costo_total: Ti.App.Properties.getDouble('ctD'),
                    km_total_recorridos:Ti.App.Properties.getString('kmt')
             	};
           		endServReq.send(params);
    		}
    	};	
    	
    	registerServ.onerror = function(e) {
			Ti.API.debug("STATUS: " + this.status);
			Ti.API.debug("TEXT:   " + this.responseText);
			Ti.API.debug("ERROR:  " + e.error);
			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Comprueba tu conexión a internet',buttonNames: ['OK']});
    			alertDialog.show();
		};
	});
	
	dialogCancelado.addEventListener('click', function(ed){
		banderacancelar = true;
		recibo.open("POST","http://iego.com.mx/webservices/recibo.php");
       	params = {
        	destino:geocoderDestino,
            horario_precio:horarioTarifa,
            id_usuario: Ti.App.Properties.getInt('id_usuario'),
            id_servicio: Ti.App.Properties.getInt("id_servicio"),
      	};
        recibo.send(params);
        Ti.App.fireEvent('fbController:cancelacion', {
			id_chofer:idch.text
		});
		var Folioformateado=Ti.App.Properties.getString("id_folio").split('-');
		var FolioNum = Folioformateado[1];
		Ti.App.fireEvent('fbController:costo_cancelado',{ 
	 		folio_ser:Ti.App.Properties.getString("id_folio"),
	 		id_user:Ti.App.Properties.getInt('id_usuario'),
	 		fol:FolioNum
    	});
    	var Costo_cancelacion;	
		Costo_cancelacion = Ti.App.Properties.getDouble('ctD') * 0.3;
		Ti.App.Properties.setDouble("ctD2",Costo_cancelacion);
		pagoCan.open("POST","http://iego.com.mx/webservices/updateservices.php");
        params3 = {
        	id_servicio: Ti.App.Properties.getInt("id_servicio"),
            costo_cancelacion: Ti.App.Properties.getDouble('ctD2')
       	};
        pagoCan.send(params3);
	});
	
	dialogFinalizado.addEventListener('click', function(ed){
		sdial3 = true;var a=0;var b =0;var c=0;player.stop();
		Ti.App.fireEvent('fbController:finalizarviaje', { 
			id_chofer:idch.text,
			id_userF:Ti.App.Properties.getInt("id_usuario")
		});
		recibo.open("POST","http://iego.com.mx/webservices/recibo.php");
       	params = {
        	destino:geocoderDestino,
            horario_precio:horarioTarifa,
            id_usuario: Ti.App.Properties.getInt('id_usuario'),
            id_servicio: Ti.App.Properties.getInt("id_servicio"),
     	};
        recibo.send(params);
	});
	
	recibo.onload = function(){
    	json = this.responseText;
        response = JSON.parse(json);
        if (response.logged == true){
        	Titanium.App.Properties.removeProperty('viajeactivo');
        	Titanium.App.Properties.removeProperty('finalizar');
        	Titanium.App.Properties.removeProperty('latInicio');
        	Titanium.App.Properties.removeProperty('lngInicio');
        	Titanium.App.Properties.removeProperty('latDestino');
        	Titanium.App.Properties.removeProperty('lngDestino');
        	Titanium.App.Properties.removeProperty('geocoderlbl');
        	Titanium.App.Properties.removeProperty('tiposervicio');
        	Titanium.App.Properties.removeProperty('idTipoServicio');
        	Titanium.App.Properties.removeProperty('opcionservicio');
        	Titanium.App.Properties.removeProperty('geocoderDestino');

        	endServReq.open("POST","http://iego.com.mx/webservices/endServicios.php");
        	params = {
        		id_servicio: Ti.App.Properties.getInt("id_servicio"),
            	km_adicional: Ti.App.Properties.getDouble("kmAdicional"),
            	costo_km_adicional:Ti.App.Properties.getDouble("kmCostoAdicional"),
            	hora_extra: "00:00:00",
            	costo_hora_extra: 0,
            	cargo_hora_pico: 0,
            	tiempo_espera_adicional:"00:00:00",
            	costo_espera_adicional: 0,
            	costo_total: Ti.App.Properties.getDouble('ctD'),
            	km_total_recorridos:Ti.App.Properties.getString('kmt')
     		};
        	endServReq.send(params);
        	if (banderacancelar == true) {
        		home= require('/interface/mapa2');
        		new home().open();
        		window.close();
        	}else{
        		calificar = require('/interface/calificar');
            	new calificar().open();
            	window.close();
        	}
        }	
    };
    
    recibo.onerror = function(e) {
		Ti.API.debug("STATUS: " + this.status);
		Ti.API.debug("TEXT:   " + this.responseText);
		Ti.API.debug("ERROR:  " + e.error);
		alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Comprueba tu conexión a internet',buttonNames: ['OK']});
    	alertDialog.show();
	};
	
	function getData(){
		var xhr = Ti.Network.createHTTPClient({
			onload: function() {
				db = JSON.parse(this.responseText);
				if(db.servicios){
					setData(db);
				}
			},
			onerror: function(e) {
				Ti.API.debug("STATUS: " + this.status);
				Ti.API.debug("TEXT:   " + this.responseText);
				Ti.API.debug("ERROR:  " + e.error);
				alert('Comprueba tu conexion a internet');
			},
				timeout:5000
		});	
		xhr.open('POST', url);
		if (Ti.App.Properties.hasProperty('viajeactivo')) {
			idc = Ti.App.Properties.getInt('id_ChoferA');
		}
		var params = {
    		id_chofer:idc,
            horario_precio:horarioTarifa
    	};
    	if(sa != 0){
    		xhr.send(params);
    	}
	}
	
	function setData(db){
		for(var i = 0; i < db.servicios.length; i++){
			var kmformateado=Ti.App.Properties.getString("distancias").split(',');
		 	var kmWeb = kmformateado[0] + '.' + kmformateado[1];
		 	var dus= parseFloat(kmWeb);
		 	choferactivo =db.servicios[i].id_chofer;
		 	if (choferactivo != null){
		 		idch.text = db.servicios[i].id_chofer;
		 		Ti.App.Properties.setString("kmt",dus);
				Ti.App.Properties.setInt("id_ChoferA",choferactivo);
				Ti.App.Properties.setString("id_choferS",db.servicios[i].id_chofer);
				
				viewP = Titanium.UI.createView({top:140, width:'100%',height:'auto',backgroundColor:'white',zIndex:4});
				tarifa_inicial = Ti.UI.createLabel({text:'Costo aproximado:',left:'2%',top:'10%',color:'black'});
				modelo = Ti.UI.createLabel({text:'Modelo:',left:'2%',top:'16%',color:'black'});
				conductor = Ti.UI.createLabel({text:'Conductor:',left:'2%',top:'22%',color:'black'});
				placaslbl = Ti.UI.createLabel({text:'Placas:',left:'2%',top:'28%',color:'black'});
				dt = Ti.UI.createLabel({text:'Distancia total:',left:'2%',top:'34%',color:'black'});
				kmadicional = Ti.UI.createLabel({text:'Km adicional:',left:'2%',top:'40%',visible:false,color:'black'});
				costoadicional = Ti.UI.createLabel({text:'Costo adicional:',left:'2%',top:'46%',visible:false,color:'black'});
				costoTOTAL = Ti.UI.createLabel({text:'Costo aproximado:',left:'2%',top:'34%',visible:false,color:'black'});
				horario_inicio = Ti.UI.createLabel({color:'#4ad34d',right:'2%',top:'10%'});
				tarifaInicial=Ti.UI.createLabel({text:db.servicios[i].tarifa_inicial});
				telefono.text = db.servicios[i].num_telefono_chofer;
				ImgPerfil.image = "http://iego.com.mx/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20');
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].modelo,color:'#a2a1b8',right:'2%',top:'16%',font:{fontSize:14}});
				nombre_chofer = Ti.UI.createLabel({text:db.servicios[i].nombre_chofer,color:'#a2a1b8',right:'2%',top:'22%',font:{fontSize:14}});
				placas = Ti.UI.createLabel({text:db.servicios[i].placa,color:'#a2a1b8',right:'2%',top:'28%',font:{fontSize:14}});
				estrellas = Ti.UI.createLabel({right:'2%', top:'34%',color:'#a2a1b8',text:dus.toFixed(1)+' KM',font:{fontSize:14}});
				lblAdicional = Ti.UI.createLabel({right:'2%', top:'40%',color:'#a2a1b8',font:{fontSize:14},visible:false});
				lblcosto = Ti.UI.createLabel({right:'2%', top:'46%',color:'#4ad34d',font:{fontSize:14},visible:false});
				lblTotal = Ti.UI.createLabel({right:'2%', top:'34%',color:'#4ad34d',font:{fontSize:14},visible:false});
				
				var newkmad=dus-db.servicios[i].km_inicial;
                var newCosto;    
                if((parseFloat(dus))<=(parseFloat(db.servicios[i].km_inicial))){
                    newCosto=0;
                }else if((parseFloat(dus))>(parseFloat(db.servicios[i].km_inicial)) && (parseFloat(dus))<=(parseFloat(db.servicios[i].km_adicional_1)) ){
                   
                    newCosto = newkmad * db.servicios[i].precio_km_adicional_1;
                }else if((parseFloat(dus))>(parseFloat(db.servicios[i].km_adicional_1)) && (parseFloat(dus))<=(parseFloat(db.servicios[i].km_adicional_2))){
                   
                    newCosto = newkmad * db.servicios[i].precio_km_adicional_2;
                }else if((parseFloat(dus))>(parseFloat(db.servicios[i].km_adicional_2)) && (parseFloat(dus))<=(parseFloat(db.servicios[i].km_adicional_3))){
                    
                    newCosto = newkmad * db.servicios[i].precio_km_adicional_3;
                }else if((parseFloat(dus))>(parseFloat(db.servicios[i].km_adicional_3 ))){
                  
                    newCosto = newkmad * db.servicios[i].precio_km_adicional_4;
                }               
                if (newkmad < 0){
                        lblAdicional.text=0;
                        Ti.App.Properties.setDouble("kmAdicional",lblAdicional.text);
                }else{
                        lblAdicional.text = newkmad;
                        Ti.App.Properties.setDouble("kmAdicional",lblAdicional.text);
                }
                
                kmadicional.visible = true;
                lblAdicional.visible = true;
                if(newCosto==0){
                    lblcosto.text = '$'+newCosto.toFixed(1)+' MXN';
                    kmCA=newCosto;
                    Ti.App.Properties.setDouble("kmCostoAdicional",kmCA.toFixed(2));
            
                }else{
                 	lblcosto.text = '$'+newCosto.toFixed(1)+' MXN';
                    kmCA=newCosto;
                    Ti.App.Properties.setDouble("kmCostoAdicional",kmCA.toFixed(2));
                }
                
                costoadicional.visible = true;
                lblcosto.visible = true;
                tarifaA=newCosto;
                tarifaFinal=newCosto+db.servicios[i].tarifa_inicial*1;
                costotpay.text = (newCosto+db.servicios[i].tarifa_inicial*1);
                lblTotal.text = '$' + tarifaFinal.toFixed(2) +' MXN';
                Ti.App.Properties.setString("ta",tarifaA.toFixed(2));
                Ti.App.Properties.setString("ct",tarifaFinal.toFixed(2));
                Ti.App.Properties.setDouble("ctD",tarifaFinal.toFixed(2));
                lblTotal.visible = true;
                costoTOTAL.visible = true;
			
				viewP.add(lugar_origen);
				viewP.add(nombre_chofer);
				viewP.add(placas);
				viewP.add(modelo);
				viewP.add(conductor);
				viewP.add(placaslbl);
				
				viewP.add(lblTotal);
				viewP.add(costoTOTAL);
				centerView.add(viewLogo);
				centerView.add(vistaimg);
				vistaimg.add(ImgPerfil);
				centerView.add(viewP);
				centerView.add(vistaMapa);
				
				if (Ti.App.Properties.hasProperty('viajeactivo')) {
					
				}else{
					pagoReq.open("POST","http://iego.com.mx/webservices/paypal/samples/BillingAgreements/DoReferenceTransaction.php");
               		params = {
                		id_biling:Ti.App.Properties.getString("id_biling"),
                    	name: Ti.App.Properties.getString('nombre'),
                    	lastname: Ti.App.Properties.getString('apellido'),
                    	descripcion_viaje: 'Tipo de viaje es: ' + Ti.App.Properties.getString("tipo_servicio") + ', Km recorridos: ' + Ti.App.Properties.getString('kmt'),
                    	costo_total: Ti.App.Properties.getDouble('ctD')
              		};
           			pagoReq.send(params); 	
				}
		 	}
		}
	}
	
	pagoReq.onload = function(){
		json = this.responseText;
        response = JSON.parse(json);
        if (response.logged == true){
        	sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       		params = {
        		idusuario:id_usuario,
            	mensaje: 'Saldo suficiente',
            	titulo: 'Saldo suficiente',
            	statussolicitud:0,
            	viajecancelado:0,
            	saldoinsuficiente:0,
            	noaceptado:0
        	};
        	sisno.send(params);
        	Ti.App.Properties.setString("idtransaccion",response.idtransaccion);
            Ti.App.fireEvent('fbController:saldoinsuficiente2',{ 
				id_chofer:idc,
			});
			Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
			notification = Ti.App.iOS.scheduleLocalNotification({
    			alertAction: "ver",
    			alertBody: dialogAceptado.message,
    			badge: 1,
    			date: new Date(new Date().getTime() + 3000),
    			sound: "/sound/sonido.mp3",
    			userInfo: {}
			});
   			dialogAceptado.show();
   			player.play();
   			my_timer.stop();
			my_timer.set(03,00);
			viewPro.visible=false;
        }else{
        	sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       		params = {
        		idusuario:id_usuario,
            	mensaje: 'El usuario no cuenta con saldo suficiente',
            	titulo: 'Saldo insuficiente',
            	statussolicitud:0,
            	viajecancelado:0,
            	saldoinsuficiente:1,
            	noaceptado:0
        	};
        	sisno.send(params);
            Ti.App.fireEvent('fbController:saldoinsuficiente',{ 
				id_chofer:idc
	 		});
	 		window.remove(webview);
			mapview.removeAllAnnotations();
			my_timer.stop();
			my_timer.set(03,00);
			dialCalif.show();
        }
	};
	
	pagoReq.onerror = function(e) {
		sisno.open("POST","http://iego.com.mx/webservices/enviarnotificacion.php");
       	params = {
        	idusuario:id_usuario,
            mensaje: 'El usuario no cuenta con saldo suficiente',
            titulo: 'Saldo insuficiente',
            statussolicitud:0,
            viajecancelado:0,
            saldoinsuficiente:1,
            noaceptado:0
        };
        sisno.send(params);
        Ti.App.fireEvent('fbController:saldoinsuficiente',{ 
			id_chofer:idc
	 	});
		Ti.API.debug("STATUS: " + this.status);
		Ti.API.debug("TEXT:   " + this.responseText);
		Ti.API.debug("ERROR:  " + e.error);
		alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Comprueba tu conexión a internet',buttonNames: ['OK']});
    	alertDialog.show();
    	
	};
	
	dialCalif.addEventListener('click', function(ed){
        var viewChildren = window.children;
		for (var i = 0; i < viewChildren.length; i++) {
			var child = viewChildren[i];
			window.remove(child);
		}
		window.removeAllChildren();
		window.remove(webview);
		webview = null;
		
		home= require('/interface/mapa2');
        new home().open();
        window.close();
    });
    
    dialogChofer.addEventListener('click', function(ed){
		home= require('/interface/mapa2');
        new home().open();
        window.close();
        var viewChildren = window.children;
		for (var i = 0; i < viewChildren.length; i++) {
			var child = viewChildren[i];
			window.remove(child);
		}
		window.removeAllChildren();
		viewWeb.remove(webview);
		webview = null;
	});
	
	Ti.App.iOS.addEventListener('notification', function(e) {
    	if (e.badge > 0) {
        	Ti.App.iOS.scheduleLocalNotification({
            	date: new Date(new Date().getTime()),
            	badge: -1
        	});
    	}
	});
	
	window.addEventListener('open',function(){
		if (Ti.App.Properties.hasProperty('viajeactivo')) {
			idch.text = Ti.App.Properties.getInt('id_ChoferA');
			var currentTime = new Date();
        	var hours = currentTime.getHours();
        	var minutes = currentTime.getMinutes();    	    
        	if (minutes < 10) {minutes = '0' + minutes;}
        	if (hours < 10) {hours = '0' + hours;}    
        	horaActual = hours + ':' + minutes; 
        	if(horaActual >= '06:00' && horaActual <= '20:00'){horarioTarifa=1; 	
        	}else{horarioTarifa=0;}
			sa = 1;
			viewPro.visible = false;
			getData();
			if (Ti.App.Properties.getInt('yallegue')==0) {
				sdial2 = false;
			}
			if (Ti.App.Properties.getInt('viajeiniciado')==0) {
				sdial4 =  false;
			}
			if (Ti.App.Properties.getInt('finalizar')==0) {
				sdial3 = false;
			}
		}else{
			var currentTime = new Date();
        	var hours = currentTime.getHours();
        	var minutes = currentTime.getMinutes();    	    
        	if (minutes < 10) {minutes = '0' + minutes;}
        	if (hours < 10) {hours = '0' + hours;}    
        	horaActual = hours + ':' + minutes; 
        	if(horaActual >= '06:00' && horaActual <= '20:00'){horarioTarifa=1; 	
        	}else{horarioTarifa=0;}
			animateLogo = Titanium.UI.createAnimation();
        	animateLogo.top = '15%';
        	animateLogo.duration = 1800;
        	animateLogo.opacity = 1;
        	logo.animate(animateLogo);
        	progress.animate({ width: 0, duration: 180000 });my_timer.start();	
		}
	});		
    
    viewPro.add(display_lbl);
	viewPro.add(buscando_lbl);
	track.add(progress);
 	viewPro.add(track);
 	viewPro.add(logo);
 	window.add(viewPro);
 	window.add(viewWeb);
 	centerView.add(vistaMapa);
 	window.add(centerView);
 	centerView.add(btnCanViaje);

	return window;
};
module.exports = chofer;