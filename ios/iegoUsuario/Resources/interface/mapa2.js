function mapa(){
	//Modulos requeridos
	var Map = require('ti.map');
	var Social = require('dk.napp.social');
	var NappSlideMenu = require('dk.napp.slidemenu');
	
	//Variables
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta,tableData = [],tableView,tipoweb,xhrLocationCode;
	//Menu lateral
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'#cc4a4a',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'white',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'});
	lblClose = Titanium.UI.createLabel({color:'white',text:'Cerrar Sesión',top:'94%',left:'13%'});
	//Webservices
	navwin = createCenterNavWindow();
	closeSesion=Titanium.Network.createHTTPClient();
	
	// Funcion principal
	function createCenterNavWindow(){
		//Webservices
		pedirKm= Titanium.Network.createHTTPClient();	
		tipoweb = Titanium.Network.createHTTPClient();
		idPpal = Titanium.Network.createHTTPClient();
		idPpal2 = Titanium.Network.createHTTPClient();
		cotizaAutos= Titanium.Network.createHTTPClient();
		xhrLocationCode = Ti.Network.createHTTPClient();var requestUrl;
		xhrLocationCode.setTimeout(120000);
		
		// Ventana principal
		tc = Ti.UI.createLabel({text:'IEGO',color:'#000000'});
		window = Titanium.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/MENU2.png', left:10,zIndex:2,height:30,width:30});
		
		//Interfaz grafica
		leftButtonOrigen = Titanium.UI.createButton({backgroundImage:'/images/green.png',width:30,height:30});
		leftButtonDestino = Titanium.UI.createButton({backgroundImage:'/images/red.png',width:30,height:30});
		txtOrigen = Titanium.UI.createTextField({borderStyle:Ti.UI.INPUT_BORDERSTYLE_ROUNDED,color:'black',hintText:"Ingrese su Origen",top:20,width:'90%',height:40,leftButton:leftButtonOrigen,leftButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS});
		txtDestino = Titanium.UI.createTextField({borderStyle:Ti.UI.INPUT_BORDERSTYLE_ROUNDED,color:'black',hintText:"Ingrese su Destino",top:65,width:'90%',height:40,leftButton:leftButtonDestino,leftButtonMode:Titanium.UI.INPUT_BUTTONMODE_ALWAYS});
		btnPedir = Titanium.UI.createButton({width:'90%',height:50,title:'Pedir Automóvil',textAlign:'center',center:0,bottom:'5%',color:'white',zIndex:3,backgroundColor:'#2b282b',borderRadius:6});
		mapview = Map.createView({mapType: Map.NORMAL_TYPE,animate:true,regionFit:true,userLocation:true,top:0});
		pinUsuario = Map.createAnnotation({title:"Mi ubicacion",image:'/images/marcador1.png'});
		pinDestino = Map.createAnnotation({title:"Mi destino",image:'/images/bandera1.png'});
		pkrvAuto = Titanium.UI.createView({height:320,bottom:-360,backgroundColor:'white',zIndex:5});
		
		//variables locales
		geocoderlbl =  Titanium.UI.createLabel({});latInicio =  Titanium.UI.createLabel({});lngInicio =Titanium.UI.createLabel({});latDestino =Titanium.UI.createLabel({});
		lngDestino =Titanium.UI.createLabel({});moveDestino =Titanium.UI.createLabel({});bdrBusqueda = 1;pagar =0;var dest,hor,b,ids,desc,costo,kma,ckma;
		var table_data = [];var last_search = null;var timers = [];var ANDROID_API_KEY = 'AIzaSyB4xyTWJ6MAh1mO94AUfLF9WsuOiQt-OsA';var dus;var tiposervicio =  Titanium.UI.createLabel({text:'AVANZA'});
		var idTipoServicio = 5;
		
		//Animaciones
		slideBottom = Ti.UI.createAnimation();slideBottom.top = 0;slideBottom.duration = 450;
		slideTop = Ti.UI.createAnimation();slideTop.top = -60;slideTop.duration = 450;
		slide_in =  Titanium.UI.createAnimation({bottom:0});
		slide_out =  Titanium.UI.createAnimation({bottom:-360});
		
		//Alertas
		dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Pago realizado correctamenta',title: 'Pago realizado'});
		optsServicios = {cancel: 2,options: ['Sencillo', 'Redondo', 'Cancelar','Itinerario','Por hora','Por dia (200Km)'],selectedIndex: 2,destructive: 0,title: 'Tipo de viaje'};
		dialogOpcionServicio = Ti.UI.createOptionDialog(optsServicios);var opcionservicio = 0;var moveDestino =Titanium.UI.createLabel({});
		
		//Piker
		cancel =  Titanium.UI.createButton({title:'Cancelar',style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED});
		done =  Titanium.UI.createButton({title:'Buscar',style:Titanium.UI.iPhone.SystemButtonStyle.DONE});
		spacer =  Titanium.UI.createButton({systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE});
		fixedSpace = Titanium.UI.createButton({systemButton:Titanium.UI.iPhone.SystemButton.FIXED_SPACE,width:25});
		toolbar =  Titanium.UI.iOS.createToolbar({top:0,items:[cancel,spacer,done]});
		info =  Titanium.UI.createButton({backgroundImage:'/images/info.png',width:32,height:32});
		cotizar =  Titanium.UI.createButton({backgroundImage:'/images/cotizar.png',width:40,height:40});
		toolbar2 =  Titanium.UI.iOS.createToolbar({bottom:0,items:[cotizar,spacer,spacer]});
		pkrAuto = Ti.UI.createPicker({top:20,bottom:20});
		arrayAutos = ['AVANZA','SUV-EJECUTIVA','VAN','PICKUP'];
		pkrAuto.selectionIndicator = true;
		column1 = Ti.UI.createPickerColumn();
		
		for(var i=0, ilen=arrayAutos.length; i<ilen; i++){
  			var row = Ti.UI.createPickerRow({title: arrayAutos[i],custom_item_m:arrayAutos[i]});
  			column1.addRow(row);
		}
		pkrAuto.add([column1]);
		
		//Ventana modal
		var t = Titanium.UI.create2DMatrix();t = t.scale(0);
		var w = Titanium.UI.createWindow({backgroundColor:'white',borderWidth:8,borderColor:'#999',height:400,width:300,borderRadius:10,opacity:0.92,transform:t});
		var t1 = Titanium.UI.create2DMatrix();t1 = t1.scale(1.1);var a = Titanium.UI.createAnimation();a.transform = t1;a.duration = 200;
		var imgCar = Ti.UI.createImageView({image:'/images/avanza.png',top:'20%',width:'75%',height:'auto',zIndex:2,center:0});
		var txtInfo = Titanium.UI.createLabel({text:"Vehiculo con capacidad de 5 pasajeros",top:'45%', color:'black',center:0,textAlign:'center',width:'80%'});
		var btnClose = Titanium.UI.createButton({width:'60%',height:40,title:'Cerrar',textAlign:'center',center:0,bottom:'10%',color:'white',zIndex:3,backgroundColor:'#0080fc',borderRadius:6});
		var costoTOTAL = Ti.UI.createLabel({text:'Costo total aproximado:',center:0,top:'60%',color:'black',visible:false});
		var lblTotal = Ti.UI.createLabel({center:0, top:'66%',color:'#4ad34d',visible:false});
		
		tipoweb.open("POST","http://iego.com.mx/webservices/servicioweb.php");
       	params = {id_usuario: Ti.App.Properties.getInt("id_usuario")};
       	
       	Titanium.Geolocation.getCurrentPosition(function(e){
    		if (e.error){
        		alert('No se puede obtener ubicacion actual');
        		return;
    		}
    		pinUsuario.latitude = e.coords.latitude;
    		pinUsuario.longitude = e.coords.longitude;
    		pinDestino.latitude = e.coords.latitude + 0.0009999;
			pinDestino.longitude = e.coords.longitude -0.0009999;
    		mapview.region = {latitude:e.coords.latitude, longitude:e.coords.longitude, latitudeDelta:0.009, longitudeDelta:0.009};	
			mapview.annotations = [pinUsuario,pinDestino];
			mapview.animatedCenter = pinUsuario;
			latInicio.text = e.coords.latitude;
			lngInicio.text = e.coords.longitude;
			Titanium.Geolocation.reverseGeocoder(e.coords.latitude,e.coords.longitude,function(evt){
				if (evt.success) {
					var places = evt.places;
					if (places && places.length) {
						txtOrigen.value = places[0].address;
					}else{
						txtOrigen.value = "No address found";
					}
				}else{
					Ti.UI.createAlertDialog({title:'Reverse geo error',message:evt.error}).show();
				}
			});
		});
		
		cancel.addEventListener('click',function() {
			pkrvAuto.animate(slide_out);
		});
		
		done.addEventListener('click',function(){
			dialogOpcionServicio.show();
		});
		
		a.addEventListener('complete', function(){
			var t2 = Titanium.UI.create2DMatrix();
			t2 = t2.scale(1.0);
			w.animate({transform:t2, duration:200});
		});
		
		pkrAuto.addEventListener('change',function(e){
			var currentTime = new Date();var horarioTarifa;var hours = currentTime.getHours();var minutes = currentTime.getMinutes();
			if (minutes < 10) { minutes = '0' + minutes;}
            if (hours < 10) {hours = '0' + hours;}
            horaActual = hours + ':' + minutes;
            if(horaActual >= '06:00' && horaActual <= '20:00'){
           		horarioTarifa=1;
            }else{
          		horarioTarifa=0;
          	}
          	if (e.row.title == 'AVANZA') {
          		tiposervicio.text = e.row.title;
				idTipoServicio = 5;
				imgCar.image='/images/avanza.png';
				txtInfo.text = "Vehiculo con capacidad de 5 pasajeros";
          	}else if (e.row.title == 'SUV-EJECUTIVA'){
          		tiposervicio.text = e.row.title;
				idTipoServicio = 2;
				imgCar.image='/images/crv.png';
				txtInfo.text = "Vehiculo con capacidad de 4 pasajeros";
          	}else if (e.row.title == 'VAN') {
          		tiposervicio.text = e.row.title;
				idTipoServicio = 3;
				imgCar.image='/images/hiace.png';
				txtInfo.text = "Vehiculo con capacidad de 14 pasajeros";
          	}else if (e.row.title == 'PICKUP') {
          		tiposervicio.text = e.row.title;
				idTipoServicio = 4;
				imgCar.image='/images/hilux.png';
				txtInfo.text = "Vehiculo con capacidad de 4 pasajeros";
          	}
          	cotizaAutos.open("POST","http://iego.com.mx/webservices/cotizarauto.php");
           	params = {
            	id_tipo_servicio: idTipoServicio,
                horario_precio:horarioTarifa
          	};
       		cotizaAutos.send(params);		
       		pedirKm.open("POST","http://iego.com.mx/webservices/distanciag.php");
        	params = {
        		lat1:latInicio.text ,
       			lng1:lngInicio.text,
       			lat2:latDestino.text,
       			lng2:lngDestino.text
        	};
       		pedirKm.send(params);
		});
		
		cotizaAutos.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setDouble("tarifa_inicialC",response.tarifa_inicial);
            	Ti.App.Properties.setDouble("km_inicialC",response.km_inicial);
            	Ti.App.Properties.setDouble("precio_km_adicional_1C",response.precio_km_adicional_1);
            	Ti.App.Properties.setDouble("km_adicional_1C",response.km_adicional_1);
            	Ti.App.Properties.setDouble("precio_km_adicional_2C",response.precio_km_adicional_2);
            	Ti.App.Properties.setDouble("km_adicional_2C",response.km_inicial);
            	Ti.App.Properties.setDouble("precio_km_adicional_3C",response.precio_km_adicional_3);
            	Ti.App.Properties.setDouble("km_adicional_3C",response.km_adicional_3);
            	Ti.App.Properties.setDouble("precio_km_adicional_4C",response.precio_km_adicional_4);
           	}else{
           		Ti.UI.createAlertDialog({title:'Mensaje',message:response.message}).show();
           	}
    	};
		
		cotizar.addEventListener('click',function() {
			w.open(a);
			var kmformateado=Ti.App.Properties.getString("distancias").split(',');
		 	var kmWeb = kmformateado[0] + '.' + kmformateado[1];
    		var dus= parseFloat(kmWeb);
       		var kmAdicional=dus-Ti.App.Properties.getDouble("km_inicialC");
      		var newCosto;
      	
      		if((parseFloat(dus))<=(parseFloat(Ti.App.Properties.getDouble("km_inicialC")))){
				newCosto=0;
			}else if((parseFloat(dus))>(parseFloat(Ti.App.Properties.getDouble("km_inicialC"))) && (parseFloat(dus))<=(parseFloat(Ti.App.Properties.getDouble("km_adicional_1C")))){
				Ti.API.info('km_adicional: ' +Ti.App.Properties.getDouble("km_adicional_1C"));
				newCosto = kmAdicional * Ti.App.Properties.getDouble("precio_km_adicional_1C");
			}else if((parseFloat(dus))>(parseFloat(Ti.App.Properties.getDouble("km_adicional_1C"))) && (parseFloat(dus))<=(parseFloat(Ti.App.Properties.getDouble("km_adicional_2C")))){
				Ti.API.info('km_adicional: ' +Ti.App.Properties.getDouble("km_adicional_2C"));
				newCosto = kmAdicional * Ti.App.Properties.getDouble("precio_km_adicional_2C");
			}else if((parseFloat(dus))>(parseFloat(Ti.App.Properties.getDouble("km_adicional_2C"))) && (parseFloat(dus))<=(parseFloat(Ti.App.Properties.getDouble("km_adicional_3C")))){
					Ti.API.info('km_adicional: ' +Ti.App.Properties.getDouble("km_adicional_3C"));
					newCosto = kmAdicional * Ti.App.Properties.getDouble("precio_km_adicional_3C");
			}else if((parseFloat(dus))>(parseFloat(Ti.App.Properties.getDouble("km_adicional_3C")))){
					Ti.API.info('km_adicional: ' +Ti.App.Properties.getDouble("km_adicional_4C"));
					newCosto = kmAdicional * Ti.App.Properties.getDouble("precio_km_adicional_4C");
			}	
			var precioCarro=newCosto+Ti.App.Properties.getDouble("tarifa_inicialC")*1;
			lblTotal.text='$'+precioCarro.toFixed(2)+' MXN';
			lblTotal.visible = true;
			costoTOTAL.visible = true;
		});
		
		btnClose.addEventListener('click',function() {
			var t3 = Titanium.UI.create2DMatrix();
			t3 = t3.scale(0);
			w.close({transform:t3,duration:300});
			lblTotal.visible = false;
			costoTOTAL.visible = false;
		});
		window.addEventListener('open', function(e){
			Titanium.App.Properties.removeProperty("id_tipo_servicio");
			Titanium.App.Properties.removeProperty("tarifa_inicial");
			Titanium.App.Properties.removeProperty("num_telefono_chofer");
			Titanium.App.Properties.removeProperty("fotografiaC");
			Titanium.App.Properties.removeProperty("modelo");
			Titanium.App.Properties.removeProperty("nombre_chofer");
			Titanium.App.Properties.removeProperty("placa");
    		Titanium.App.Properties.removeProperty("horarioTarifa");
       		Titanium.App.Properties.removeProperty('viajeiniciado');
   	 		Titanium.App.Properties.removeProperty('yallegue');
   	 		Titanium.App.Properties.removeProperty('vistaActiva');
    		Titanium.App.Properties.removeProperty('txtorigen');
    		Titanium.App.Properties.removeProperty('txtdestino');
    		Titanium.App.Properties.removeProperty('ctD');
    		Titanium.App.Properties.removeProperty('ct');
    		Titanium.App.Properties.removeProperty('ta');
    		Titanium.App.Properties.removeProperty('kmCostoAdicional');
    		Titanium.App.Properties.removeProperty('kmAdicional');
    		Titanium.App.Properties.removeProperty('id_choferS');
    		Titanium.App.Properties.removeProperty('id_ChoferA');
    		Titanium.App.Properties.removeProperty('kmt');
    		Titanium.App.Properties.removeProperty('id_payment');
    		Titanium.App.Properties.removeProperty('id_servicio');
    		Titanium.App.Properties.removeProperty('latIN');
    		Titanium.App.Properties.removeProperty('longIN');
    		Titanium.App.Properties.removeProperty('latDES');
    		Titanium.App.Properties.removeProperty('longDES');
    	
    		Ti.App.iOS.registerUserNotificationSettings({
	    		types: [
            		Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
            		Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
            		Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
        		]
    		});
    	
    		if (Ti.App.Properties.hasProperty('id_usuario')) {
    			idPpal.open("POST","http://iego.com.mx/webservices/bilingusuario.php");
        		params = {id_usuario: Ti.App.Properties.getInt("id_usuario")};
       			idPpal.send(params);
				tipoweb.send(params);
  			}
		});
		
		Ti.App.iOS.addEventListener('notification', function(e) {
    		if (e.badge > 0) {
        		Ti.App.iOS.scheduleLocalNotification({
            		date: new Date(new Date().getTime()),
            		badge: -1
        		});
    		}
		});
		
		idPpal.onload = function(){
    		json = this.responseText;
        	response = JSON.parse(json);
        	if (response.logged == true){
        		
        		  if(Ti.App.Properties.getString("identificador")!=response.identificador){
        		  	 inicio = require('/interface/inicio');
                     new inicio().open();
                     window.close();
        		  }else{
        		  	Ti.App.Properties.setString("id_biling",response.id_biling);
            			if(Ti.App.Properties.getString("id_biling")==''){
    						paypal= require('/interface/paypal');
        					new paypal().open();
        					window.close();
    					}
        		  }
     		}
 		};
 		
 		idPpal.onerror = function(e) {
			Ti.UI.createAlertDialog({title:'Internet',message:'Comprueba tu conexión a internet'}).show();					
		};
		
    	function omitirAcentos(text) {
			var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
			var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
			for (var i=0; i<acentos.length; i++) {
				text = text.replace(acentos.charAt(i), original.charAt(i));
			}
			return text;
		}
		
		//Predictivo
		var container = Ti.UI.createView({layout : 'vertical',height : Ti.UI.SIZE,top:'18%'});
		var autocomplete_table = Ti.UI.createTableView({height: Ti.UI.SIZE});
		
		txtDestino.addEventListener('change', function(e) {
			if (txtDestino.value.length > 2 && txtDestino.value !=  last_search) {
				clearTimeout(timers['autocomplete']);
				timers['autocomplete'] = setTimeout(function() {
					last_search =txtDestino.value;
					auto_complete(txtDestino.value);
				}, 300);
			}
			return false;
		});
		
		autocomplete_table.addEventListener('click', function(e) {
			get_place(e.row.place_id);
		});
		
		function get_place(place_id) {
			var url = "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + place_id + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     				var obj = JSON.parse(this.responseText);
					var result = obj.result;
					container.place_id           = place_id;
					container.formatted_address  = result.formatted_address;
					container.latitude           = result.geometry.location.lat;
					container.longitude          = result.geometry.location.lng;
					txtDestino.value                  = container.formatted_address;				
					table_data = [];
					autocomplete_table.setData(table_data);
					autocomplete_table.setHeight(0);
 				},	
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		};
		
		function get_locations(query) {
			var url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + query + "&location=16.8633600,-99.8901000&key=" + ANDROID_API_KEY;		
			var client = Ti.Network.createHTTPClient({
				onload : function(e) {
     				var obj = JSON.parse(this.responseText);
     				var predictions = obj.predictions;     		
     				table_data = [];            
            		autocomplete_table.removeAllChildren();
					autocomplete_table.setHeight(Ti.UI.SIZE);var dest;
            		for (var i = 0; i < predictions.length; i++) {
            			var desc = omitirAcentos(predictions[i].description);
            			var row = Ti.UI.createTableViewRow({
							height: 40,
							title: desc,
							place_id: predictions[i].place_id,
							hasDetail:false,
							postId:desc,            		
            			});                        	
            			table_data.push(row);  	     	
            		}	 
            		autocomplete_table.setData(table_data);
            		txtDestino.focus();
 				},
 				onerror : function(e) {
					Ti.API.debug(e.error);
					alert('error');
				},
				timeout : 5000
			});
			client.open("GET", url);
			client.send();	
		};
		
		function auto_complete(search_term){
			if (search_term.length > 2) {
				get_locations(search_term);   	
   			}
		}
		
		autocomplete_table.addEventListener("click", function(e){
			txtDestino.focus();
		});
		
		txtOrigen.addEventListener('return', function(e){
  			txtOrigen.blur();
  			bdrBusqueda = 0;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtOrigen.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
		
		txtDestino.addEventListener('return', function(e){
  			txtDestino.blur();
  			bdrBusqueda = 1;
  			requestUrl = "http://maps.google.com/maps/api/geocode/json?address=" + txtDestino.value.replace(' ', '+');
			requestUrl += "&sensor=" + (Ti.Geolocation.locationServicesEnabled == true);
			xhrLocationCode.open("GET", requestUrl);
			xhrLocationCode.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
			xhrLocationCode.send();		
		});
		
		xhrLocationCode.onerror = function(e) {};
		xhrLocationCode.onload = function(e) {
			mapview.region = {latitude:latInicio.text, longitude:lngInicio.text, latitudeDelta:0.03, longitudeDelta:0.03};	
			var response = JSON.parse(this.responseText);
			if (response.status == 'OK' && response.results != undefined && response.results.length > 0) {
				if (bdrBusqueda == 0) {
					pinUsuario.latitude =response.results[0].geometry.location.lat;
					pinUsuario.longitude = response.results[0].geometry.location.lng;
					latInicio.text = response.results[0].geometry.location.lat;
					lngInicio.text = response.results[0].geometry.location.lng;
				}else if (bdrBusqueda == 1) {
					moveDestino.text = '1';
					pinDestino.latitude =response.results[0].geometry.location.lat;
					pinDestino.longitude = response.results[0].geometry.location.lng;
					latDestino.text = response.results[0].geometry.location.lat;
					lngDestino.text = response.results[0].geometry.location.lng;
					
					pedirKm.open("POST","http://iego.com.mx/webservices/distanciag.php");
        			params = {
        				lat1:latInicio.text ,
       					lng1:lngInicio.text,
       					lat2:latDestino.text,
       					lng2:lngDestino.text
        			};
       				pedirKm.send(params);
       				Titanium.Geolocation.reverseGeocoder(response.results[0].geometry.location.lat,response.results[0].geometry.location.lng,function(evt){
						if (evt.success) {
							var places = evt.places;
							if (places && places.length) {
								geocoderlbl.text = places[0].address;
							}else{
								geocoderlbl.text = "No address found";
							}
						}else{
							Ti.UI.createAlertDialog({
								title:'Reverse geo error',
								message:evt.error
							}).show();
						}
					});
				}
			}
			response = null;
		};
		
		pedirKm.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            	Ti.App.Properties.setString("distancias",response.km_total);
            }
        };
		
		//Eventos
		btnPedir.addEventListener('click',function(){
			if (moveDestino.text=='1'){
        		pkrvAuto.animate(slide_in);
				pkrAuto.show();
			}else{
				Ti.UI.createAlertDialog({title:'Mensaje',message:'Ingrese la dirección de su destino'}).show();
			}
		});
		
		dialogOpcionServicio.addEventListener('click', function(e){
			if (e.index === 0){
				if (Ti.App.Properties.hasProperty('id_usuario')) {
					idPpal2.open("POST","http://iego.com.mx/webservices/bilingusuario.php");
               		params = {
                       id_usuario: Ti.App.Properties.getInt("id_usuario")
               		};
             	 	idPpal2.send(params);
	   				opcionservicio = 1;
				}else{
					alert('Solo usuarios registrados');
				}
			}
		});
		
		 idPpal2.onload = function(){
		 	json = this.responseText;
           	response = JSON.parse(json);
           	 if (response.logged == true){
           	 	  if(Ti.App.Properties.getString("identificador")!=response.identificador){
           	 	  	 	var alert= Titanium.UI.createAlertDialog({message:'Has iniciado sesión en otro dispositivo',buttonNames: ['Aceptar'],title:'Mensaje'});
                        alert.show();   
                        alert.addEventListener('click',function(){
                           inicio = require('/interface/inicio');
                           new inicio().open({modal:true});
                           window.close();
                       	});
           	 	  }else{
           	 	  	chofer= require('/interface/chofer');
					new chofer(latInicio.text,lngInicio.text,latDestino.text,lngDestino.text,txtOrigen.value,tiposervicio.text,idTipoServicio,opcionservicio,txtDestino.value).open();	
          			window.close();
           	 	  }
           	 }
		 };
		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchDisabledWithTapToClose");
		});
		
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.add(btnPedir);
		window.add(mapview);
		window.add(txtOrigen);
		window.add(txtDestino);
		pkrvAuto.add(toolbar);
		pkrvAuto.add(toolbar2);
		pkrvAuto.add(pkrAuto);
		window.add(pkrvAuto);
		container.add(autocomplete_table);
		window.add(container);
		w.add(imgCar);
		w.add(txtInfo);
		w.add(btnClose);
		w.add(lblTotal);
		w.add(costoTOTAL);
		
		return navwin;
	}
	
	lblHome.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	
	lblPagos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			paypal= require('/interface/paypal');
			winNav = new paypal();
			winNav.open();
		}else{
			alert('Solo usuarios registrados');
		}
	});
	
	lblPromociones.addEventListener('click',function(){
		Ti.UI.createAlertDialog({title:'Mensaje',message:"Esperalo muy pronto"}).show();
	});
	
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			winNav = new pedidos();
			winNav.open();
		}else{
			alert('Solo usuarios registrados');
		}
	});
	
	lblMiPerfil.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			perfil = require('/interface/perfil');
			winNav = new perfil();
			winNav.open();
		}else{
			alert('Solo usuarios registrados');
		}
	});
	
	if (Ti.App.Properties.hasProperty('id_usuario')) {
		
	}else{
		lblClose.text = 'Regresar';
	}
	
	lblClose.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			closeSesion.open("POST","http://iego.com.mx/webservices/statususuario.php");
            params = {
             	id_usuario:Ti.App.Properties.getInt('id_usuario'),
            };
            closeSesion.send(params);
			Titanium.App.Properties.removeProperty('id_usuario');
			Titanium.App.Properties.removeProperty('usuario');
			Titanium.App.Properties.removeProperty('email');
			Titanium.App.Properties.removeProperty('telefono');
			Titanium.App.Properties.removeProperty('rfc');
			Titanium.App.Properties.removeProperty('razon');
		}else{
			inicio = require('/interface/inicio');
			winNav = new inicio();
			winNav.open();
			window.close();
		}
	});
	
	closeSesion.onload = function(){
   		json = this.responseText;
    	response = JSON.parse(json);
    	if (response.logged == true){
    		inicio = require('/interface/inicio');
			winNav = new inicio();
			winNav.open();
			window.close();
    	}else{
        	alert(response.message);
    	}
	};
	
	closeSesion.onerror = function(e) {
		Ti.API.debug("STATUS: " + this.status);
		Ti.API.debug("TEXT:   " + this.responseText);
		Ti.API.debug("ERROR:  " + e.error);
		alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Comprueba tu conexión a internet',buttonNames: ['OK']});
    	alertDialog.show();
	};
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblClose);
	
	return mainwin;
};
module.exports = mapa;