function autos(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window;
	
	navwin = createCenterNavWindow();
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'SELECCIONAR VEHICULO',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({modal: true,window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/closeWin.png', left:10,zIndex:2,height:25,width:25});	
		
		menu.addEventListener('click',function(){
			navwin.close();
		});
			
		window.navwin = navwin;
		window.setLeftNavButton(menu);
			
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	return mainwin;
};
module.exports = autos;