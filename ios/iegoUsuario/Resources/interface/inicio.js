function inicio(){
	var window,logo,btnLogin,btnRegistrar,lblOmitir,navwin,login,winNav,registro,mapa,animateWin;
	
	window = Ti.UI.createWindow({navBarHidden:true});
	logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'60%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
	video1 = Titanium.Media.createVideoPlayer({top : 0,autoplay : true,width : '100%',scalingMode : Titanium.Media.VIDEO_SCALING_MODE_FILL,url:'/images/v1.mp4',mediaControlStyle: Titanium.Media.VIDEO_CONTROL_HIDDEN,repeatMode:Titanium.Media.VIDEO_REPEAT_MODE_ONE});	
	btnLogin = Titanium.UI.createButton({height:'10%',width:'50%',left:0,title:'INICIAR SESIÓN',textAlign:'center',backgroundColor:'#cc4a4a',color:'white',bottom:0,zIndex:3});
	btnRegistrar = Titanium.UI.createButton({height:'10%',width:'50%',right:0,title:'REGISTRARSE',textAlign:'center',backgroundColor:'#a00a1c',color:'white',bottom:0,zIndex:3});
	lblOmitir = Titanium.UI.createButton({title:'Omitir',top:'5%',right:15,color:'white',backgroundColor:'transparent',zIndex:3});
	navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
	
	window.addEventListener('open', function(e) {
		animateLogo = Titanium.UI.createAnimation();
		animateLogo.top = '16%';
		animateLogo.duration = 2000;
		animateLogo.opacity = 1;
		logo.animate(animateLogo);
	});
	
	btnLogin.addEventListener('click', function(){
		login = require('/interface/login');
		winNav = new login('#373136','Iniciar Sesión', navwin);
		winNav.containingNav = navwin;
		navwin.openWindow(winNav);
	});
	
	btnRegistrar.addEventListener('click', function(){
		registro = require('/interface/registro');
		winNav = new registro('#373136','Registrar', navwin);
		winNav.containingNav = navwin;
		navwin.openWindow(winNav);
	});
	
	lblOmitir.addEventListener('click', function(){
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
		
		mapa2 = require('/interface/mapa2');
		winNav = new mapa2();
		winNav.containingNav = navwin;
		navwin.openWindow(winNav);
	});
	
	window.containingNav = navwin;
	window.barColor = 'transparent';
	window.navTintColor = 'transparent';
	window.add(logo);
	window.add(lblOmitir);
	window.add(btnLogin);
	window.add(btnRegistrar);
	window.add(video1);
	
	return navwin;
};
module.exports = inicio;