function factura(color, _title, _nav){
	var window,tc,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,loginReq,json,response,resetReq,mapa,registerReq;
	
	tc = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg',navTintColor:'white', nav:_nav, titleControl: tc, title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	lblUser = Titanium.UI.createLabel({text:'RFC',center:0,top:'5%',color:'white'});
	txtUser = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'CRTP891004BR4',center:0,top:'10%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center'});
	lblpass= Titanium.UI.createLabel({text:'Razón Social',center:0,top:'18%',color:'white'});
	txtPass = Titanium.UI.createTextField({hintText:'Manzur y Asociados SA de CV',center:0,top:'23%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center'});
	
	lblDireccion= Titanium.UI.createLabel({text:'Direccion fiscal',center:0,top:'31%',color:'white'});
	txtDireccion = Titanium.UI.createTextField({hintText:'Direccion fiscal',center:0,top:'36%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center'});
	
	lblCp= Titanium.UI.createLabel({text:'Codigo Postal',center:0,top:'44%',color:'white'});
	txtCp = Titanium.UI.createTextField({hintText:'Codigo Postal',center:0,top:'52%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center'});
	
	lblCorreo= Titanium.UI.createLabel({text:'Correo de facturación',center:0,top:'60%',color:'white'});
	txtCorreo = Titanium.UI.createTextField({hintText:'Correo de facturación',center:0,top:'65%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center'});

	
	btnlogin = Titanium.UI.createButton({title:'Registrarme',color:'white',borderRadius:30,borderColor:'white',top:'85%',width:'85%',height:60,backgroundColor:'#a00a1c'});
	chkFactura = Ti.UI.createSwitch({value:false,top:'73%',onTintColor:'#62D337',tintColor:'#D90019',left:'10%'});
	lblFactura = Titanium.UI.createLabel({text:'Acepto términos y condiciones',right:'10%',top:'74%',color:'white'});
	registerReq = Titanium.Network.createHTTPClient();
	
	chkFactura.addEventListener('change',function(e){
		if (e.value == true) {
			lblFactura.borderColor='#a00a1c';
		}else{
			lblFactura.borderColor='transparent';
		}
	});
	
	window.addEventListener('foo', function(e){
		registerReq.onload = function(){
			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.message == "Insert failed" || response.message == 'El usuario o email ya existen'){
    				alertDialog = Titanium.UI.createAlertDialog({title: 'Terminos y condiciones',message: response.message,buttonNames: ['OK']});
    				alertDialog.show();
    			}else{
    				alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
    				alertDialog.show();
    				alertDialog.addEventListener('click',function(e){
    					Ti.App.Properties.setInt("id_usuario", response.id_usuario);
    					Ti.App.Properties.setInt("id_cliente", response.id_cliente);
    					Ti.App.Properties.setString("usuario", response.usuario);
    					Ti.App.Properties.setString("email", response.email);
    					Ti.App.Properties.setString("telefono", response.telefono);
    					Ti.App.Properties.setString("nombre", response.nombre);
    					Ti.App.Properties.setString("apellido", response.apellido);
    					Ti.App.Properties.setString("rfc", response.rfc);
    					Ti.App.Properties.setString("razon", response.razon);
    					Ti.App.Properties.setString("identificador",Titanium.Platform.id);
    					mapa2 = require('/interface/mapa2');
					winNav = new mapa2();
					winNav.open();
					window.close();
    				});
    			}
		};
	
		btnlogin.addEventListener('click',function(){
			if (txtUser.value != '' && txtPass.value != '' && txtDireccion.value != '' && txtCp.value != '' && txtCorreo.value != ''){
				if (chkFactura.value == false) {
					alertDialog = Titanium.UI.createAlertDialog({title: 'Terminos y condiciones',message: 'Debe de aceptar los terminos y condiciones',buttonNames: ['OK']});
    					alertDialog.show();
				}else{
					registerReq.open("POST","http://iego.com.mx/webservices/registerResquest.php");
                		params = {
                    		username: e.username,
                    		password: Ti.Utils.md5HexDigest(e.password),
                    		correo: e.correo,
                    		telefono:e.telefono,
                    		nombre:e.nombre,
                    		apellido_p:e.apellido_p,
                    		apellido_m:e.apellido_m,
                    		rfc:txtUser.value,
                    		razon_social:txtPass.value,
                    		direccion_fiscal:txtDireccion.value,
                    		email_factura:txtCorreo.value,
                    		cp:txtCp.value,
                    		identificador:Titanium.Platform.id
                		};
                		registerReq.send(params);
				}
			}else{
				alertDialog = Titanium.UI.createAlertDialog({title: 'Validación',message: 'Todos los campos son obligatorios',buttonNames: ['OK']});
    			alertDialog.show();
			}
		});
	});
	
	window.addEventListener('open', function(){
		txtUser.focus();
	});
	
	scrollView.add(lblUser);
	scrollView.add(txtUser);
	scrollView.add(lblpass);
	scrollView.add(txtPass);
	scrollView.add(chkFactura);
	scrollView.add(lblFactura);
	scrollView.add(lblDireccion);
	scrollView.add(txtDireccion);
	scrollView.add(lblCp);
	scrollView.add(txtCp);
	scrollView.add(lblCorreo);
	scrollView.add(txtCorreo);
	scrollView.add(btnlogin);
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(scrollView);
	
	return window;
};
module.exports = factura;