function perfil(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;var hidden=false;
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'white',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'#cc4a4a',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'});
	lblClose = Titanium.UI.createLabel({color:'white',text:'Cerrar Sesión',top:'94%',left:'13%'});
	closeSesion=Titanium.Network.createHTTPClient();
	pb = Ti.UI.createActivityIndicator({style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,message:'Cargando tus pedidos...',width:'auto',
        left:'100dp',height:'80dp',indicatorColor:'#007aff',color: '#007aff',
    });
	
	navwin = createCenterNavWindow();
	
	lblHome.addEventListener('click',function(){
		mapa2 = require('/interface/mapa2');
		winNav = new mapa2();
		winNav.open();
	});
	lblPagos.addEventListener('click',function(){
		paypal = require('/interface/paypal');
		winNav = new paypal();
		winNav.open();
	});
	lblPromociones.addEventListener('click',function(){
		Ti.UI.createAlertDialog({title:'Mensaje',message:"Esperalo muy pronto"}).show();
	});
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			winNav = new pedidos();
			winNav.open();
		}else{
			alert('Solo usuarios registrados');
		}
	});
	lblMiPerfil.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	
	lblClose.addEventListener('click',function(){
		closeSesion.open("POST","http://iego.com.mx/webservices/statususuario.php");
            params = {
             	id_usuario:Ti.App.Properties.getInt('id_usuario'),
            };
            closeSesion.send(params);	
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
		
	});
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Mi Perfil',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/MENU2.png', left:10,zIndex:2,height:30,width:30});
		share = Ti.UI.createButton({backgroundImage:'/images/editar.png', right:50,height:28,width:28,zIndex:5});
		done =  Titanium.UI.createButton({backgroundImage:'/images/palomita.png', right:50,height:28,width:28,zIndex:5});
		viewLogo = Titanium.UI.createImageView({width:'100%',height:'auto',image:'/images/logo4.png',top:0,left:0,zIndex:3});
		vistaimg= Ti.UI.createView({width:124,height:124,borderRadius:65, top:'12%', bottom:15, center:0,zIndex:3});
		imgUser = Ti.UI.createImageView({image:"/images/usuario.jpg",width:124,height:120});
		scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
		lblUsuario = Ti.UI.createLabel({text:'Usuario',color:'#aeaec1',top:'40%',left:'2%'});
		txtUsuario = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',right:'2%',top:'40%',editable:false,width:'60%', textAlign:'right'});
		separador = Ti.UI.createView({width:'100%',height:1,top:'47%',backgroundColor:'#eeeeee'});
		lblCorreo = Ti.UI.createLabel({text:'Email factura',color:'#aeaec1',top:'50%',left:'2%'});
		txtCorreo = Ti.UI.createTextField({value:Ti.App.Properties.getString('email'),color:'#151431',right:'2%',top:'50%',editable:false, width:'60%', textAlign:'right'});
		separador2 = Ti.UI.createView({width:'100%',height:1,top:'57%',backgroundColor:'#eeeeee'});
		lblTelefono = Ti.UI.createLabel({text:'Teléfono',color:'#aeaec1',top:'60%',left:'2%'});
		txtTelefono = Ti.UI.createTextField({value:Ti.App.Properties.getString('telefono'),color:'#151431',right:'2%',top:'60%',editable:false});
		separador3 = Ti.UI.createView({width:'100%',height:1,top:'67%',backgroundColor:'#eeeeee'});
		lblRfc= Ti.UI.createLabel({text:'RFC',color:'#aeaec1',top:'60%',left:'2%'});
		txtRfc = Ti.UI.createTextField({value:Ti.App.Properties.getString('rfc'),color:'#151431',right:'2%',top:'60%',editable:false});
		separador4 = Ti.UI.createView({width:'100%',height:1,top:'77%',backgroundColor:'#eeeeee'});
		lblRazon= Ti.UI.createLabel({text:'Razón Social',color:'#aeaec1',top:'70%',left:'2%'});
		txtRazon = Ti.UI.createTextField({value:Ti.App.Properties.getString('razon'),color:'#151431',right:'2%',top:'70%',editable:false,width:'60%', textAlign:'right'});
		separador5 = Ti.UI.createView({width:'100%',height:1,top:'87%',backgroundColor:'#eeeeee'});
		lblCerrar = Ti.UI.createLabel({text:'Cerrar Sesión',color:'#aeaec1',top:'82%',left:'2%'});
		btnCerrar = Ti.UI.createButton({backgroundImage:'/images/exit.png',right:'2%',zIndex:2,height:38,width:38,top:'80%'});
		updateClienteReq = Titanium.Network.createHTTPClient();
		if (Ti.App.Properties.hasProperty('telefono')) {
			scrollView.add(lblTelefono);
			scrollView.add(txtTelefono);
			scrollView.add(separador5);
			lblRfc.top = '70%';
			txtRfc.top = '70%';
			lblRazon.top = '80%';
			txtRazon.top = '80%';
			lblCerrar.top = '90%';
			btnCerrar.top = '90%';
		}
		
		btnCerrar.addEventListener('click',function(){
			closeSesion.open("POST","http://iego.com.mx/webservices/statususuario.php");
            params = {
             	id_usuario:Ti.App.Properties.getInt('id_usuario'),
            };
            closeSesion.send(params);	
			Titanium.App.Properties.removeProperty('id_usuario');
			Titanium.App.Properties.removeProperty('usuario');
			Titanium.App.Properties.removeProperty('email');
			Titanium.App.Properties.removeProperty('telefono');
			Titanium.App.Properties.removeProperty('rfc');
			Titanium.App.Properties.removeProperty('razon');
		});
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		share.addEventListener('click',function(){
			if (!hidden) {
				window.setRightNavButton(done);
				txtCorreo.editable = true;
				txtCorreo.focus();
				txtRfc.editable = true;
				txtRazon.editable = true;
				txtTelefono.editable = true;
				hidden = true;
			}else{
				window.setRightNavButton(share);
				
				hidden = false;
			}
		});
		done.addEventListener('click',function(){
			if (!hidden) {
				window.setRightNavButton(done);
				hidden = true;
			}else{
				txtRfc.editable = false;
				txtRazon.editable = false;
				txtCorreo.editable = false;
				txtTelefono.editable = false;
				window.setRightNavButton(share);
				hidden = false;
			}
			updateClienteReq.open("POST","http://iego.com.mx/webservices/updateclientes.php");
        			params = {
            			id_cliente:Ti.App.Properties.getInt('id_cliente'),
            			rfc: txtRfc.value,
            			razon_social: txtRazon.value,
            			correo_facturacion:txtCorreo.value,
            			telefono:txtTelefono.value
        			};
        		updateClienteReq.send(params);
		});
		updateClienteReq.onload = function(){
    		json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == true){
    			Ti.App.Properties.setString("rfc", txtRfc.value);
    			Ti.App.Properties.setString("razon", txtRazon.value);
    			alert(response.message);
    		}else{
        		alert(response.message);
    		}
		};
		
		closeSesion.onload = function(){
    			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.logged == true){
    				inicio = require('/interface/inicio');
				winNav = new inicio();
				winNav.open();
				window.close();
    			}else{
        			alert(response.message);
    			}
		};
		
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.setRightNavButton(share);
		window.add(viewLogo);
		window.add(vistaimg);
		vistaimg.add(imgUser);
		scrollView.add(lblUsuario);
		scrollView.add(txtUsuario);
		scrollView.add(separador);
		scrollView.add(lblCorreo);
		scrollView.add(txtCorreo);
		scrollView.add(separador2);
		scrollView.add(separador3);
		scrollView.add(lblRfc);
		scrollView.add(txtRfc);
		scrollView.add(separador4);
		scrollView.add(lblRazon);
		scrollView.add(txtRazon);
		scrollView.add(lblCerrar);
		scrollView.add(btnCerrar);
		window.add(scrollView);
		
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblClose);
	
	return mainwin;
};
module.exports = perfil;