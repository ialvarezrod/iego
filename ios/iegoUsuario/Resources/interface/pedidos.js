function pedidos(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;var hidden=false;
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'#cc4a4a',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'});
	lblClose = Titanium.UI.createLabel({color:'white',text:'Cerrar Sesión',top:'94%',left:'13%'});
	closeSesion=Titanium.Network.createHTTPClient();
	
	pb = Ti.UI.createActivityIndicator({style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,message:'Cargando tus pedidos...',width:'auto',
        left:'100dp',height:'80dp',indicatorColor:'#007aff',color: '#007aff',
    });
	
	navwin = createCenterNavWindow();
	
	lblHome.addEventListener('click',function(){
		mapa2 = require('/interface/mapa2');
		winNav = new mapa2();
		winNav.open();
	});
	lblPagos.addEventListener('click',function(){
		paypal = require('/interface/paypal');
		winNav = new paypal();
		winNav.open();
	});
	lblPromociones.addEventListener('click',function(){
		Ti.UI.createAlertDialog({title:'Mensaje',message:"Esperalo muy pronto"}).show();
	});
	lblMisPedidos.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	lblMiPerfil.addEventListener('click',function(){
		perfil = require('/interface/perfil');
		winNav = new perfil();
		winNav.open();
	});
	
	lblClose.addEventListener('click',function(){
		closeSesion.open("POST","http://iego.com.mx/webservices/statususuario.php");
            params = {
             	id_usuario:Ti.App.Properties.getInt('id_usuario'),
            };
            closeSesion.send(params);	
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
		
	});
	
	closeSesion.onload = function(){
    			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.logged == true){
    				inicio = require('/interface/inicio');
				winNav = new inicio();
				winNav.open();
				window.close();
    			}else{
        			alert(response.message);
    			}
		};
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Mis pedidos',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/MENU2.png', left:10,zIndex:2,height:30,width:30});
		share = Ti.UI.createButton({backgroundImage:'/images/editar.png', right:50,height:28,width:28,zIndex:5});
		done =  Titanium.UI.createButton({backgroundImage:'/images/check.png', right:50,height:28,width:28,zIndex:5});
		viewLogo = Titanium.UI.createImageView({width:'100%',height:160,image:'/images/logo3.png',top:0});
		args = arguments[0] || {};url = "http://iego.com.mx/webservices/pedidos.php";db = {};
		deleteServiciosReq = Titanium.Network.createHTTPClient();
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					alert('Comprueba tu conexion a internet');
					pb.hide();
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			var params = {
    				id_usuario:Ti.App.Properties.getInt('id_usuario')
    			};
			xhr.send(params);
		}
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				row = Ti.UI.createTableViewRow({height:"105dp",backgroundColor:"white",editing:false,moving:true,postIdServicio:db.servicios[i].id_servicio});
				horario_inicio = Ti.UI.createLabel({text:db.servicios[i].hora_inicio,color:'#151431',left:'5%',top:'38%',font:{fontSize:14}});
				fecha = Ti.UI.createLabel({text:db.servicios[i].fecha,color:'#151431',left:'5%',top:'20%',font:{fontSize:14},height:30});
				precio = Ti.UI.createLabel({text:'$ '+db.servicios[i].costo + ' MXN',color:'#151431',left:'5%',top:'52%',font:{fontSize:14}});
				
				vistaimg= Ti.UI.createView({width:65,height:65,borderRadius:31, top:'5%', center:0});
				imgChofer = Ti.UI.createImageView({image:"http://iego.com.mx/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20'),width:65,height:97});
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].lugar_origen,color:'#151431',right:'5%',top:'33%',font:{fontSize:13},height:30,width:'33%'});
				folio = Ti.UI.createLabel({text:db.servicios[i].folio,color:'#151431',right:'8%',top:'52%',font:{fontSize:13}});
				nombre_chofer = Ti.UI.createLabel({text:db.servicios[i].nombre_chofer,color:'#a2a1b8',right:'5%',top:'20%',font:{fontSize:14},height:30,width:'33%'});			
				status = Ti.UI.createView({width:10,height:10,borderRadius:10,top:'34%', left:'2%'});
				if (db.servicios[i].status_servicio == 1) {
						status.backgroundColor = 'blue';
				}else if (db.servicios[i].status_servicio == 0){
						status.backgroundColor = '#a00a1c';
				}
				
				vistaimg.add(imgChofer);
				row.add(vistaimg);
				row.add(fecha);
				row.add(precio);
				row.add(horario_inicio);
				row.add(lugar_origen);
				row.add(nombre_chofer);
				row.add(folio);
				tableData.push(row);
			}
			tableView = Ti.UI.createTableView({data:tableData,top:0,bottom:0,scrollable:true,editable:true});
			window.add(tableView);
			
			tableView.addEventListener('delete',function(e){
				deleteServiciosReq.open("POST","http://iego.com.mx/webservices/deleteservicios.php");
        			params = {
            			id_servicio:e.rowData.postIdServicio
        			};
        			deleteServiciosReq.send(params);
			});	
			
		}
		deleteServiciosReq.onload = function(){
    			json = this.responseText;
    			response = JSON.parse(json);
    			if (response.logged == true){
    				
    			}else{
        			alert(response.message);
    			}
		};
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		share.addEventListener('click',function(){
			if (!hidden) {
				window.setRightNavButton(done);
				tableView.editing = true;
				hidden = true;
			}else{
				window.setRightNavButton(share);
				
				hidden = false;
			}
		});
		done.addEventListener('click',function(){
			if (!hidden) {
				window.setRightNavButton(done);
				hidden = true;
			}else{
				tableView.editing = false;
				window.setRightNavButton(share);
				hidden = false;
			}
		});
		
		window.add(pb);
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.setRightNavButton(share);
		pb.show();
		getData();
		
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblClose);
	
	return mainwin;
};
module.exports = pedidos;
