function promociones(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'white',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'#cc4a4a',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'});
	lblClose = Titanium.UI.createLabel({color:'white',text:'Cerrar Sesión',top:'94%',left:'13%'});
	
	pb = Ti.UI.createActivityIndicator({style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,message:'Cargando promociones...',width:'auto',
        left:'100dp',height:'80dp',indicatorColor:'#007aff',color: '#007aff',
    });
	
	navwin = createCenterNavWindow();
	
	lblHome.addEventListener('click',function(){
		mapa = require('/interface/mapa');
		winNav = new mapa();
		winNav.open();
	});
	lblPagos.addEventListener('click',function(){
		paypal = require('/interface/paypal');
		winNav = new paypal();
		winNav.open();
	});
	lblPromociones.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			winNav = new pedidos();
			winNav.open();
		}else{
			alert('Solo usuarios registrados');
		}
	});
	lblMiPerfil.addEventListener('click',function(){
		perfil = require('/interface/perfil');
		winNav = new perfil();
		winNav.open();
	});
	
	lblClose.addEventListener('click',function(){
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
		inicio = require('/interface/inicio');
		winNav = new inicio();
		winNav.open();
		window.close();
	});
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Promociones',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:10,zIndex:2,height:23,width:30});
		share = Ti.UI.createButton({backgroundImage:'/images/shareicon.png', right:50,height:28,width:20,zIndex:5});
		viewTab = Titanium.UI.createView({width:'100%',height:60,backgroundColor:'#f7f7f8',top:0});
		tb5 = Titanium.UI.iOS.createTabbedBar({labels:['Todas', 'Nuevas'],index:0,backgroundColor:'#007aff',style:Titanium.UI.iPhone.SystemButtonStyle.BAR});
		args = arguments[0] || {};url = "http://iego.com.mx/webservices/promociones.php";db = {};
		
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					alert('Comprueba tu conexion a internet');
					pb.hide();
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			var params = {
    				id_cliente:Ti.App.Properties.getInt('id_cliente')
    			};
			xhr.send(params);
		}
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				row = Ti.UI.createTableViewRow({bottom:'5%',height:"390dp",backgroundColor:"white",backgroundSelectedColor:"blue"});
				logo = Ti.UI.createImageView({image:'/images/logo2.png',width:'15%',height:'auto',left:'2%',top:'2%'});
				titulo = Ti.UI.createLabel({text:db.servicios[i].titulo_promocion,color:'#566673',left:'18%',top:'2%'});
				fecha = Ti.UI.createLabel({text:db.servicios[i].fecha_inicio,color:'#bac1c8',right:'2%',top:'2%'});
				politica = Ti.UI.createLabel({text:db.servicios[i].politicas_promocion,color:'#a00a1c',left:'18%',top:'8%'});
				imgPromo = Ti.UI.createImageView({image:"http://iego.com.mx/images/"+db.servicios[i].img_promocion.replace(/\s/g, '%20'),width:'95%',height:230,top:'20%',center:0});
				descPromo = Ti.UI.createLabel({text:db.servicios[i].desc_promocion,color:'#657582',left:'2%',top:'80%',right:'2%'});
				
				row.add(logo);
				row.add(titulo);
				row.add(fecha);
				row.add(politica);
				row.add(imgPromo);
				row.add(descPromo);
				tableData.push(row);
			}
			tableView = Ti.UI.createTableView({data:tableData,top:60,bottom:0,scrollable:true});
			window.add(tableView);
		}
		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		window.add(pb);
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.add(viewTab);
		viewTab.add(tb5);
		pb.show();
		getData();
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblClose);
	
	return mainwin;
};
module.exports = promociones;
