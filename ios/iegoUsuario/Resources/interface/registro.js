function registro(color, _title, _nav){
	var window,tc,lblUser,lblCorreo,lblPass,lblPass2,txtUser,txtCorreo,txtPass,txtPass2,chkFactura,chkTerminos,btnRegistrar,lblTermino,lblFactura,scrollView,
		registerReq,json,response,alertDialog;
		
	tc = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg',navTintColor:'white', nav:_nav, titleControl: tc, title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	var view = Ti.UI.createView({top: 0,height: 560});
	var view2 = Ti.UI.createView({top: '70%',height: 320});
	lblUser = Titanium.UI.createLabel({text:'Usuario',center:0,top:'5%',color:'white'});
	txtUser = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'',center:0,top:'10%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',keyboardType:Titanium.UI.KEYBOARD_DEFAULT,
    returnKeyType:Titanium.UI.RETURNKEY_DEFAULT});
	
	lblNombre = Titanium.UI.createLabel({text:'Nombre',center:0,top:'5%',color:'white'});
	txtNombre = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Nombre de usuario',center:0,top:'10%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true});
	lblApellidoP = Titanium.UI.createLabel({text:'Apellido Paterno',center:0,top:'18%',color:'white'});
	txtApellidoP = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Apellido Paterno',center:0,top:'23%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true});
	lblApellidoM = Titanium.UI.createLabel({text:'Apellido Materno',center:0,top:'31%',color:'white'});
	txtApellidoM = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Apellido Materno',center:0,top:'36%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true});
	
	lblCorreo = Titanium.UI.createLabel({text:'Correo electrónico',center:0,top:'44%',color:'white'});
	txtCorreo = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Correo',center:0,top:'49%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true});
	lblPass = Titanium.UI.createLabel({text:'Contraseña',center:0,top:'57%',color:'white'});
	txtPass = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'*********',center:0,top:'62%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',passwordMask:true,clearOnEdit:true});	
	lblPass2 = Titanium.UI.createLabel({text:'Telefono',center:0,top:'70%',color:'white'});
	txtPass2 = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'0000000000',center:0,top:'75%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true});
	chkTerminos = Ti.UI.createSwitch({value:false,top:'0%',onTintColor:'#62D337',tintColor:'#D90019',left:'20%'});
	lblTermino = Titanium.UI.createLabel({text:'Deseo facturar',right:'20%',top:'1%',color:'white'});
	chkFactura = Ti.UI.createSwitch({value:false,top:'13%',onTintColor:'#62D337',tintColor:'#D90019',left:'10%'});
	lblFactura = Titanium.UI.createLabel({text:'Aceptar términos',right:'10%',top:'14%',color:'white'});
	btnRegistrar = Titanium.UI.createButton({title:'Registrarme',color:'white',borderRadius:30,borderColor:'white',backgroundColor:'#a00a1c',top:'28%',width:'85%', height:60});
	registerReq = Titanium.Network.createHTTPClient();
	
	function checkemail(emailAddress){
    		var str = emailAddress;
    		var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    		if (filter.test(str)){
        		testresults = true;
    		}else{
        		testresults = false;
    		}
    		return (testresults);
	};
	
	lblFactura.addEventListener('click',function(e){
		terminos = require('/interface/terminos');
		winNav = new terminos('#373136','Terminos y condiciones', _nav);					
		window.nav.openWindow(winNav,{animate:true});
	});
	
	chkFactura.addEventListener('change',function(e){
		if (e.value == true) {
			lblFactura.borderColor='#a00a1c';
		}else{
			lblFactura.borderColor='transparent';
		}
	});
	
	registerReq.onload = function(){
		json = this.responseText;
    		response = JSON.parse(json);
    		if (response.message == "Insert failed" || response.message == 'El usuario o email ya existen'){
        		btnRegistrar.enabled = true;
        		btnRegistrar.opacity = 1;
        		alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    		}else{
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    			alertDialog.addEventListener('click',function(e){
    					Ti.App.Properties.setInt("id_usuario", response.id_usuario);
    					Ti.App.Properties.setInt("id_cliente", response.id_cliente);
    					Ti.App.Properties.setString("usuario", response.usuario);
    					Ti.App.Properties.setString("email", response.email_factura);
    					Ti.App.Properties.setString("telefono", response.telefono);
    					Ti.App.Properties.setString("nombre", response.nombre);
    					Ti.App.Properties.setString("apellido", response.apellido);
    					Ti.App.Properties.setString("identificador",Titanium.Platform.id);
    				mapa2 = require('/interface/mapa2');
				winNav = new mapa2();
				winNav.open();
				window.close();
    			});
    		}
	};
	btnRegistrar.addEventListener('click',function(e){
		Ti.Geolocation.getCurrentPosition(function(evt){
			if (evt.error) {
				alertDialog = Titanium.UI.createAlertDialog({title: 'GPS',message: 'No se puede obtener ubicación actual',buttonNames: ['OK']});
    				alertDialog.show();
				return;
			}
			if (txtNombre.value != '' && txtCorreo.value != '' && txtPass.value != '' && txtPass2.value != ''){
				if (chkFactura.value == false) {
					alertDialog = Titanium.UI.createAlertDialog({title: 'Terminos y condiciones',message: 'Debe de aceptar los terminos y condiciones',buttonNames: ['OK']});
    					alertDialog.show();
				}else{
					if (!checkemail(txtCorreo.value)){
						alertDialog = Titanium.UI.createAlertDialog({title: 'Validación',message: 'Introduce un email valido',buttonNames: ['OK']});
    						alertDialog.show();
					}else{
						if (chkTerminos.value == true) {
							factura = require('/interface/factura');
							winNav = new factura('#373136','Factura', _nav);
							winNav.fireEvent('foo',{
								username: txtCorreo.value,
                    				password: txtPass.value,
                    				correo: txtCorreo.value,
                    				telefono:txtPass2.value,
                    				nombre:txtNombre.value,
                    				apellido_p:txtApellidoP.value,
                    				apellido_m:txtApellidoM.value
							});
							window.nav.openWindow(winNav,{animate:true});
						}else{
							registerReq.open("POST","http://iego.com.mx/webservices/registerResquest.php");
                				params = {
                    				username: txtCorreo.value,
                    				password: Ti.Utils.md5HexDigest(txtPass.value),
                    				correo: txtCorreo.value,
                    				telefono:txtPass2.value,
                    				nombre:txtNombre.value,
                    				apellido_p:txtApellidoP.value,
                    				apellido_m:txtApellidoM.value,
                    				rfc:'',
                    				razon_social:'',
                    				direccion_fiscal:'',
                    				email_factura:'',
                    				cp:'',
                    				identificador:Titanium.Platform.id
                				};
                				registerReq.send(params);
						}
					}
				}
			}else{
				alertDialog = Titanium.UI.createAlertDialog({title: 'Validación',message: 'Todos los campos son obligatorios',buttonNames: ['OK']});
    				alertDialog.show();
    			}
		});
	});
	
	window.addEventListener('open', function(){
		txtNombre.focus();
	});
	
	view.add(lblNombre);
	view.add(txtNombre);
	view.add(lblApellidoP);
	view.add(txtApellidoP);
	view.add(lblApellidoM);
	view.add(txtApellidoM);
	
	view.add(lblCorreo);
	view.add(txtCorreo);
	view.add(lblPass);
	view.add(txtPass);
	view.add(lblPass2);
	view.add(txtPass2);
	view2.add(chkTerminos);
	view2.add(lblTermino);
	view2.add(chkFactura);
	view2.add(lblFactura);
	view2.add(btnRegistrar);
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(scrollView);
	scrollView.add(view);
	scrollView.add(view2);	
	return window;
};
module.exports = registro;