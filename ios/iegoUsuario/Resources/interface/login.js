function login(color, _title, _nav){
	var window,tc,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	
	tc = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg',navTintColor:'white', nav:_nav, titleControl: tc, title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	lblUser = Titanium.UI.createLabel({text:'Correo electrónico',center:0,top:'20%',color:'white'});
	txtUser = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'',center:0,top:'26%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"",textAlign:'center',clearOnEdit:true});
	lblpass= Titanium.UI.createLabel({text:'Contraseña',center:0,top:'36%',color:'white'});
	txtPass = Titanium.UI.createTextField({hintText:'',center:0,top:'42%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"",textAlign:'center',passwordMask:true,clearOnEdit:true});
	btnlogin = Titanium.UI.createButton({title:'Iniciar Sesión',color:'white',borderRadius:30,borderColor:'white',top:'55%',width:'85%',height:60,backgroundColor:'#757575'});
	lblreset = Titanium.UI.createLabel({text:'Recuperar Contraseña',color:'#c2c2c2',top:'70%',center:0});
	btnFacebook = Titanium.UI.createButton({title:'Iniciar con Facebook', center:0, bottom:'10%',backgroundImage:'/images/facebook.png',width:'75%',height:45,borderRadius:2,textAlign:'center'});
	loginReq = Titanium.Network.createHTTPClient();
	resetReq = Titanium.Network.createHTTPClient();
	
	btnlogin.addEventListener('click',function(e){
    		if (txtUser.value != '' && txtPass.value != ''){
        			loginReq.open("POST","http://iego.com.mx/webservices/loginResquest.php");
        			params = {
            			username: txtUser.value,
            			password: Ti.Utils.md5HexDigest(txtPass.value),
            			identificador:Titanium.Platform.id
        			};
        			loginReq.send(params);
    		}else{
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Usuario y password son requeridos',buttonNames: ['OK']});
    			alertDialog.show();
    			}
	});
	
	loginReq.onload = function(){
    		json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == true){
    			Ti.App.Properties.setInt("id_usuario", response.id_usuario);
    			Ti.App.Properties.setInt("id_cliente", response.id_cliente);
    			Ti.App.Properties.setString("usuario", response.usuario);
    			Ti.App.Properties.setString("email", response.email_factura);
    			Ti.App.Properties.setString("telefono", response.telefono);
    			Ti.App.Properties.setString("nombre", response.nombre);
    			Ti.App.Properties.setString("apellido", response.apellido);
    			Ti.App.Properties.setString("rfc", response.rfc);
    			Ti.App.Properties.setString("razon", response.razon);
    			Ti.App.Properties.setString("identificador",Titanium.Platform.id);
    			
    			mapa2 = require('/interface/mapa2');
			winNav = new mapa2();
			winNav.open();
			window.close();
    		}else{
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    		}
	};
	
	loginReq.onerror = function(e) {
		Ti.API.debug("STATUS: " + this.status);
		Ti.API.debug("TEXT:   " + this.responseText);
		Ti.API.debug("ERROR:  " + e.error);
		alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Comprueba tu conexión a internet',buttonNames: ['OK']});
    		alertDialog.show();
	};
	
	lblreset.addEventListener('click', function(){
		if (txtUser.value == ''){
			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Se requiere su usuario para restaurar la contraseña',buttonNames: ['OK']});
    			alertDialog.show();
        		
    		}else{
        		Ti.Platform.openURL("https://iego.com.mx/landing/restablecer.php");
    		}
	});
	
	resetReq.onload = function(){
    		json = this.responseText;
    	 	response = JSON.parse(json);
    		if (response.mail == true){
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Se le ha enviado un correo para restaurar su contraseña',buttonNames: ['OK']});
    			alertDialog.show();
    		}else{
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    		}
	};
	
	resetReq.onerror = function(event){
    		alert('Error de conexion: ' + JSON.stringify(event));
	};
	
	window.addEventListener('open', function(){
		txtUser.focus();
	});
	
	
	scrollView.add(lblUser);
	scrollView.add(txtUser);
	scrollView.add(lblpass);
	scrollView.add(txtPass);
	scrollView.add(lblreset);
	scrollView.add(btnlogin);
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(scrollView);
	
	return window;
};
module.exports = login;