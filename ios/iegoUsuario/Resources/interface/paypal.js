function paypal(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	var tableData = [],tableView;var hidden=false;
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'#cc4a4a',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'white',text:'Pagos',top:'84%',left:'13%'});
	lblClose = Titanium.UI.createLabel({color:'white',text:'Cerrar Sesión',top:'94%',left:'13%'});
	
	pb = Ti.UI.createActivityIndicator({style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,message:'Cargando tus pedidos...',width:'auto',
        left:'100dp',height:'80dp',indicatorColor:'#007aff',color: '#007aff',
    });
	
	navwin = createCenterNavWindow();
	
	
	lblMisPedidos.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	lblMiPerfil.addEventListener('click',function(){
		perfil = require('/interface/perfil');
		winNav = new perfil();
		winNav.open();
	});
	
	lblClose.addEventListener('click',function(){
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
		inicio = require('/interface/inicio');
		winNav = new inicio();
		winNav.open();
		window.close();
	});
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Registro Paypal',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		var name=Ti.App.Properties.getString('nombre');
  		var lastname=Ti.App.Properties.getString("apellido");
  		var usuario= Ti.App.Properties.getInt("id_usuario");
  		var cerrar = Ti.UI.createButton({title:'Cerrar',widht:'auto',height:'auto',top:0,right:'5%',color:'black'});
  		var cerrarS = Ti.UI.createButton({title:'Cancelar',widht:'auto',height:'auto',top:0,left:'5%',color:'black'});
  		
  		var webview = Titanium.UI.createWebView({top:'10%',url:'http://iego.com.mx/webservices/paypal/samples/setEC.php?name='+name+'&lastname='+lastname+'&id_usuario='+usuario});
   		RequestPaypal = Titanium.Network.createHTTPClient();
                   
    		cerrar.addEventListener('click',function(){
        		mapa2= require('/interface/mapa2');
     		new mapa2().open();
    			window.close();	
        });
        cerrarS.addEventListener('click',function(){
        if(Ti.App.Properties.getString("id_biling")==''){
        	dialCalif = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: 'Recuerda que al cancelar, no podras registrar tu tarjeta.',title: 'Cancelar'});
			dialCalif.show();
			dialCalif.addEventListener('click',function(e){
				if (e.index == 0) {
					Titanium.App.Properties.removeProperty('id_usuario');
					Titanium.App.Properties.removeProperty('usuario');
					Titanium.App.Properties.removeProperty('nombre');
					Titanium.App.Properties.removeProperty('apellido');
					Titanium.App.Properties.removeProperty('email');
					Titanium.App.Properties.removeProperty('telefono');
					Titanium.App.Properties.removeProperty('rfc');
					Titanium.App.Properties.removeProperty('razon');
					Titanium.App.Properties.removeProperty('numero_tarjeta');
					inicio = require('/interface/inicio');
					new inicio().open();
					window.close();		
				}else{
					
				}
			});
        	}else{
        		mapa2 = require('/interface/mapa2');
				new mapa2().open();
				window.close();	
        	}
        });
   		window.containingNav = navwin;
		window.setRightNavButton(cerrar);
		window.setLeftNavButton(cerrarS);
		window.add(webview);
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblClose);
	
	return mainwin;
};
module.exports = paypal;
