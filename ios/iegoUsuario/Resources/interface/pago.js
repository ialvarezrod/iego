/**
 * @alexismh
 */
function pagos(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var cardio = require('com.likelysoft.cardio');
	var window,lblCuenta,lblVencimiento,lblTarjeta,txtCuenta,txtVencimiento,txtTarjeta,cmbTarjeta,cmbVencimiento,btnCuenta;
	
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblMisPedidos = Titanium.UI.createLabel({color:'white',text:'Mis Pedidos',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	lblPromociones = Titanium.UI.createLabel({color:'white',text:'Promociones',top:'76%',left:'13%'});
	lblPagos = Titanium.UI.createLabel({color:'#cc4a4a',text:'Pagos',top:'84%',left:'13%'});
	lblClose = Titanium.UI.createLabel({color:'white',text:'Cerrar Sesión',top:'94%',left:'13%'});
	
	navwin = createCenterNavWindow();
	
	lblHome.addEventListener('click',function(){
		mapa = require('/interface/mapa');
		winNav = new mapa();
		winNav.open();
	});
	lblPagos.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	lblPromociones.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			promociones = require('/interface/promociones');
			winNav = new promociones();
			winNav.open();
		}else{
			alert('Solo usuarios registrados');
		}
	});
	lblMisPedidos.addEventListener('click',function(){
		if (Ti.App.Properties.hasProperty('id_usuario')) {
			pedidos = require('/interface/pedidos');
			winNav = new pedidos();
			winNav.open();
		}else{
			alert('Solo usuarios registrados');
		}
	});
	lblMiPerfil.addEventListener('click',function(){
		perfil = require('/interface/perfil');
		winNav = new perfil();
		winNav.open();
	});
	
	lblClose.addEventListener('click',function(){
		Titanium.App.Properties.removeProperty('id_usuario');
		Titanium.App.Properties.removeProperty('usuario');
		Titanium.App.Properties.removeProperty('email');
		Titanium.App.Properties.removeProperty('telefono');
		Titanium.App.Properties.removeProperty('rfc');
		Titanium.App.Properties.removeProperty('razon');
		inicio = require('/interface/inicio');
		winNav = new inicio();
		winNav.open();
		window.close();
	});
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Pagos',color:'#000000'});
		window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/menuicon.png', left:10,zIndex:2,height:23,width:30});
		share = Ti.UI.createButton({backgroundImage:'/images/shareicon.png', right:50,height:28,width:20,zIndex:5});
		scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
		lblCuenta = Ti.UI.createLabel({text:'Número de cuenta',center:0,top:'25%',color:'white'});
		txtCuenta = Ti.UI.createTextField({color:'#c2c2c2',hintText:'64531-4300-8710-9922',center:0,top:'31%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"64531-4300-8710-9922",textAlign:'center',editable:false});
		lblVencimiento = Ti.UI.createLabel({text:'Vencimiento',center:0,top:'40%',color:'white'});
		txtVencimiento = Ti.UI.createTextField({color:'#c2c2c2',hintText:'05/19',center:0,top:'46%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"05/19",textAlign:'center',editable:false});
		lblTarjeta = Ti.UI.createLabel({text:'Tipo de tarjeta',center:0,top:'55%',color:'white'});
		txtTarjeta = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Seleccionar',center:0,top:'61%',width:'80%',borderRadius:6,height:40,textAlign:'center',backgroundColor:'white',editable:false});
		lblNCuenta = Ti.UI.createLabel({});
		lblCVV = Ti.UI.createLabel({});
		lblMes = Ti.UI.createLabel({});
		lblYear = Ti.UI.createLabel({});
		btnCuenta = Titanium.UI.createButton({title:'Añadir metodo de pago',color:'white',borderRadius:10,borderColor:'white',backgroundColor:'#a00a1c',top:'80%',width:'75%',height:50});
		pkrvTarjeta = Titanium.UI.createView({height:211,bottom:-251,backgroundColor:'white'});
		slide_in =  Titanium.UI.createAnimation({bottom:0});
		slide_out =  Titanium.UI.createAnimation({bottom:-251});
		cancel =  Titanium.UI.createButton({title:'Cancel',style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED});
		done =  Titanium.UI.createButton({title:'Done',style:Titanium.UI.iPhone.SystemButtonStyle.DONE});
		spacer =  Titanium.UI.createButton({systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE});
		toolbar =  Titanium.UI.iOS.createToolbar({top:0,items:[cancel,spacer,done]});
		
		pkrTarjeta = Ti.UI.createPicker({top:40});
		mount = ['amex','visa','mastercard'];
		pkrTarjeta.selectionIndicator = true;
		column1 = Ti.UI.createPickerColumn();
		for(var i=0, ilen=mount.length; i<ilen; i++){
  			var row = Ti.UI.createPickerRow({title: mount[i],custom_item_m:mount[i]});
  			column1.addRow(row);
		}
		pkrTarjeta.add([column1]);
		
		cancel.addEventListener('click',function() {
			pkrvTarjeta.animate(slide_out);
		});
		done.addEventListener('click',function() {
			pkrvTarjeta.animate(slide_out);
		});
		pkrTarjeta.addEventListener('change',function(e){
			txtTarjeta.value = e.row.title;
		});
		txtTarjeta.addEventListener('click',function() {
			pkrvTarjeta.animate(slide_in);
			pkrTarjeta.show();
			txtTarjeta.blur();
		});
		txtCuenta.addEventListener('click', function() {
    			cardio.scanCard(function(data){
				if(data.success == 'true') {
					entradaC = data.expiryYear.split('0');
					mes = data.expiryMonth;
  					salida = entradaC[1];
  					if (salida < 10) {
  						salida = salida+'0';
  					}
  					if (mes < 10) {
  						mes = '0'+mes;
  					}
  					lblMes.text = mes;
  					lblYear.text = data.expiryYear;
					txtCuenta.value = data.redactedCardNumber;
					txtVencimiento.value = mes+'/'+salida;
					lblNCuenta.text = data.cardNumber;
					lblCVV.text = data.cvv;
				}else {		
				}
			});
		});
		txtVencimiento.addEventListener('click', function() {
    			cardio.scanCard(function(data){
				if(data.success == 'true') {
					entradaC = data.expiryYear.split('0');
					mes = data.expiryMonth;
  					salida = entradaC[1];
  					if (salida < 10) {
  						salida = salida+'0';
  					}
  					if (mes < 10) {
  						mes = '0'+mes;
  					}
					txtCuenta.value = data.redactedCardNumber;
					txtVencimiento.value = mes+'/'+salida;
					lblNCuenta.text = data.cardNumber;
					lblCVV.text = data.cvv;
				}else {		
				}
			});
		});
		
		btnCuenta.addEventListener('click',function(){
			if (txtTarjeta.value != ''){
				Ti.App.Properties.setString("numero_tarjeta", lblNCuenta.text);
				Ti.App.Properties.setString("fv", txtVencimiento.value);
				Ti.App.Properties.setString("month_expire", lblMes.text);
				Ti.App.Properties.setString("year_expire", lblYear.text);
				Ti.App.Properties.setString("cvv", lblCVV.text);
				Ti.App.Properties.setString("tipo_tarjeta", txtTarjeta.value);
				mapa = require('/interface/mapa');
				winNav = new mapa();
				winNav.open();
			}else{
				alert('Seleccione un tipo de tarjeta');
			}  	
		});
		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.setRightNavButton(share);
		window.add(scrollView);
		pkrvTarjeta.add(toolbar);
		pkrvTarjeta.add(pkrTarjeta);
		window.add(pkrvTarjeta);
		scrollView.add(lblCuenta);
		scrollView.add(txtCuenta);
		scrollView.add(lblVencimiento);
		scrollView.add(txtVencimiento);
		scrollView.add(lblTarjeta);
		scrollView.add(txtTarjeta);
		scrollView.add(btnCuenta);
		
		return navwin;
	}
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblMisPedidos);
	winLeft.add(lblMiPerfil);
	winLeft.add(lblPromociones);
	winLeft.add(lblPagos);
	winLeft.add(lblClose);
	
	return mainwin;
};
module.exports = pagos;
