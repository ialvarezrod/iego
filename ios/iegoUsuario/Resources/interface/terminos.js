function terminos(color, _title, _nav){
	
	var window,tc,lblUser,lblpass,btnlogin,scrollView,txtUser,txtPass,lblreset,btnFacebook,loginReq,json,response,resetReq,mapa;
	
	tc = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bgLogin.png',navTintColor:'white', nav:_nav, titleControl: tc, title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	webview = Ti.UI.createWebView({url : 'http://iego.com.mx/terminosycondiciones/index.html',width:'100%',height:'auto',top:0,left:0,visible:true});
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(webview);
	
	return window;
};
module.exports = terminos;