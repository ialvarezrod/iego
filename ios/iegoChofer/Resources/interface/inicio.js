function inicio(){
	var window,navwin,logo,video,btnEntrar;
	
	window = Ti.UI.createWindow({navBarHidden:true});
	logo = Titanium.UI.createImageView({image:'/images/logo.png',width:'60%',height:'auto',zIndex:2,center:0,top:'0%',opacity:0.1});
	video = Titanium.Media.createVideoPlayer({top : 0,autoplay : true,width : '100%',scalingMode : Titanium.Media.VIDEO_SCALING_MODE_FILL,url:'/images/v1.mp4',mediaControlStyle: Titanium.Media.VIDEO_CONTROL_HIDDEN,repeatMode:Titanium.Media.VIDEO_REPEAT_MODE_ONE});	
	btnEntrar = Titanium.UI.createButton({height:'10%',width:'100%',left:0,title:'INICIAR SESIÓN',textAlign:'center',backgroundColor:'#a00a1c',color:'white',bottom:0,zIndex:3});
	navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
	
	window.addEventListener('open', function(e) {
		animateLogo = Titanium.UI.createAnimation();
		animateLogo.top = '18%';
		animateLogo.duration = 2000;
		animateLogo.opacity = 1;
		logo.animate(animateLogo);
	});
	
	btnEntrar.addEventListener('click', function(){
		login = require('/interface/login');
		winNav = new login('#373136','Iniciar Sesión', navwin);
		winNav.containingNav = navwin;
		navwin.openWindow(winNav);
	});
	
	window.containingNav = navwin;
	window.barColor = 'transparent';
	window.navTintColor = 'transparent';
	window.add(logo);
	window.add(btnEntrar);
	window.add(video);
	
	return navwin;
};
module.exports = inicio;