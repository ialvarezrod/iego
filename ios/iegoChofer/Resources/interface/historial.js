function historial(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,winLeft,logoLeft,lblHome,lblMiPerfil,lblHistorial,lblDisponible,chkDisponible;
	var tableData = [],tableView;var hidden=false;
	
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblHistorial = Titanium.UI.createLabel({color:'#cc4a4a',text:'Historial',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
	chkDisponible = Ti.UI.createSwitch({value:false,top:'77%',onTintColor:'#62D337',tintColor:'#D90019',left:'13%'});
	lblDisponible = Titanium.UI.createLabel({color:'white',text:'Disponible',top:'78%',left:'30%'});
	navwin = createCenterNavWindow();
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Historial',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		pb = Ti.UI.createActivityIndicator({style: Ti.UI.ActivityIndicatorStyle.BIG,message:'Cargando historial...',width:'auto',
        		center:0,height:'80dp',indicatorColor:'#007aff',color: '#007aff',
    		});
		menu = Ti.UI.createButton({backgroundImage:'/images/MENU2.png', left:10,zIndex:2,height:23,width:30});
		viewLogo = Titanium.UI.createImageView({width:'100%',height:140,image:'/images/logo3.png',top:0});
		args = arguments[0] || {};url = "http://iego.com.mx/webservices/pedidosChofer.php";db = {};
		function getData(){
			var xhr = Ti.Network.createHTTPClient({
				onload: function() {
					db = JSON.parse(this.responseText);
					if(db.servicios){
						setData(db);
					}
				},
				onerror: function(e) {
					Ti.API.debug("STATUS: " + this.status);
					Ti.API.debug("TEXT:   " + this.responseText);
					Ti.API.debug("ERROR:  " + e.error);
					alert('Comprueba tu conexion a internet');
					pb.hide();
				},
				timeout:5000
			});	
			xhr.open('POST', url);
			var params = {
    				id_chofer:Ti.App.Properties.getInt('id_chofer')
    			};
			xhr.send(params);
		}
		function setData(db){
			for(var i = 0; i < db.servicios.length; i++){
				row = Ti.UI.createTableViewRow({bottom:'3%',height:"110dp",backgroundColor:"white",backgroundSelectedColor:"blue",editing:false,moving:true,postIdServicio:db.servicios[i].id_servicio});
				horario_inicio = Ti.UI.createLabel({text:db.servicios[i].hora_inicio,color:'#151431',left:'9%',top:'30%',font:{fontSize:14,fontWeight:'bold'}});
				vistaimg= Ti.UI.createView({width:65,height:65,borderRadius:30, top:'15%', bottom:15, left:'31%'});
				imgChofer = Ti.UI.createImageView({image:"http://iego.com.mx/images/"+db.servicios[i].fotografia.replace(/\s/g, '%20'),width:65,height:97});
				lugar_origen = Ti.UI.createLabel({text:db.servicios[i].lugar_origen,color:'#151431',left:'53%',top:'25%',font:{fontSize:13}});
				nombre_chofer = Ti.UI.createLabel({text:db.servicios[i].nombre_chofer,color:'#a2a1b8',left:'53%',top:'61%',font:{fontSize:13}});
				status = Ti.UI.createView({width:15,height:15,borderRadius:7,top:'32%', left:'2%'});
				if (db.servicios[i].status_servicio == 1) {
						status.backgroundColor = '#09c863';
				}else if (db.servicios[i].status_servicio == 0){
						status.backgroundColor = '#a00a1c';
				}
				
				vistaimg.add(imgChofer);
				row.add(status);
				row.add(vistaimg);
				row.add(horario_inicio);
				row.add(lugar_origen);
				row.add(nombre_chofer);
				tableData.push(row);
			}
			tableView = Ti.UI.createTableView({data:tableData,top:160,bottom:0,scrollable:true,editable:true});
			window.add(tableView);
		}
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		
		window.add(pb);
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.add(viewLogo);
		pb.show();
		getData();
		
		return navwin;
	}
	
	lblHome.addEventListener('click',function(){
		mapa = require('/interface/mapa');
		winNav = new mapa();
		winNav.open();
	});
	lblHistorial.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	lblMiPerfil.addEventListener('click',function(){
		perfil = require('/interface/perfil');
		winNav = new perfil();
		winNav.open();
	});
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblHistorial);
	winLeft.add(lblMiPerfil);
	//winLeft.add(chkDisponible);
	//winLeft.add(lblDisponible);
	
	return mainwin;
};
module.exports = historial;
