function perfil(){
	var NappSlideMenu = require('dk.napp.slidemenu');
	var window,winLeft,logoLeft,lblHome,lblMiPerfil,lblHistorial,lblDisponible,chkDisponible;
	
	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
	lblHome = Titanium.UI.createLabel({color:'white',text:'Home',top:'52%',left:'13%'});
	lblHistorial = Titanium.UI.createLabel({color:'white',text:'Historial',top:'60%',left:'13%'});
	lblMiPerfil = Titanium.UI.createLabel({color:'#cc4a4a',text:'Mi Perfil',top:'68%',left:'13%'});
	chkDisponible = Ti.UI.createSwitch({value:false,top:'77%',onTintColor:'#62D337',tintColor:'#D90019',left:'13%'});
	lblDisponible = Titanium.UI.createLabel({color:'white',text:'Disponible',top:'78%',left:'30%'});
	navwin = createCenterNavWindow();
	
	function createCenterNavWindow(){
		tc = Ti.UI.createLabel({text:'Mi Perfil',color:'#000000'});
		window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
		navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
		menu = Ti.UI.createButton({backgroundImage:'/images/MENU2.png', left:10,zIndex:2,height:23,width:30});
		viewLogo = Titanium.UI.createImageView({width:'100%',height:'auto',image:'/images/logo4.png',top:0,left:0,zIndex:3});
		vistaimg= Ti.UI.createView({width:124,height:124,borderRadius:65, top:'12%', bottom:15, center:0,zIndex:3});
		imgUser = Ti.UI.createImageView({image:"http://iego.com.mx/images/"+Ti.App.Properties.getString('fotografia').replace(/\s/g, '%20'),width:124,height:120});
		scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
		lblUsuario = Ti.UI.createLabel({text:'Usuario',color:'#aeaec1',top:'40%',left:'2%'});
		txtUsuario = Ti.UI.createTextField({value:Ti.App.Properties.getString('usuario'),color:'#151431',right:'2%',top:'40%',editable:false});
		separador = Ti.UI.createView({width:'100%',height:1,top:'47%',backgroundColor:'#eeeeee'});
		lblCorreo = Ti.UI.createLabel({text:'Email',color:'#aeaec1',top:'50%',left:'2%'});
		txtCorreo = Ti.UI.createTextField({value:Ti.App.Properties.getString('email'),color:'#151431',right:'2%',top:'50%',editable:false});
		separador2 = Ti.UI.createView({width:'100%',height:1,top:'57%',backgroundColor:'#eeeeee'});
		lblTelefono = Ti.UI.createLabel({text:'Teléfono',color:'#aeaec1',top:'60%',left:'2%'});
		txtTelefono = Ti.UI.createTextField({value:Ti.App.Properties.getString('telefono'),color:'#151431',right:'2%',top:'60%',editable:false});
		separador3 = Ti.UI.createView({width:'100%',height:1,top:'67%',backgroundColor:'#eeeeee'});
		lblPlaca= Ti.UI.createLabel({text:'Placa',color:'#aeaec1',top:'70%',left:'2%'});
		txtPlaca = Ti.UI.createTextField({value:Ti.App.Properties.getString('placa'),color:'#151431',right:'2%',top:'70%',editable:false});
		separador4 = Ti.UI.createView({width:'100%',height:1,top:'77%',backgroundColor:'#eeeeee'});
		lblCerrar = Ti.UI.createLabel({text:'Cerrar Sesión',color:'#aeaec1',top:'90%',left:'2%'});
		btnCerrar = Ti.UI.createButton({backgroundImage:'/images/iconClose.png',right:'2%',zIndex:2,height:38,width:43,top:'90%'});
		updateAuReq = Titanium.Network.createHTTPClient();
		
		btnCerrar.addEventListener('click',function(){
			updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
            params = {
            		id_chofer:Ti.App.Properties.getInt('id_chofer'),
                placa:Ti.App.Properties.getString('placa'),
                status_login:0,
                status_ocupado:0,
                status_solicitud:0,
                status_disponibilidad:0
           };
           updateAuReq.send(params);
           
           Titanium.App.Properties.removeProperty('id_usuario');
			Titanium.App.Properties.removeProperty('usuario');
			Titanium.App.Properties.removeProperty('email');
			Titanium.App.Properties.removeProperty('telefono');
			Titanium.App.Properties.removeProperty('id_chofer');
			Titanium.App.Properties.removeProperty('placa');
			Titanium.App.Properties.removeProperty('chkDis');
		});
		
		updateAuReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
            		inicio = require('/interface/inicio');
				winNav = new inicio();
				winNav.open();
				window.close();
            }else{
            		alert(response.message);
            }
    		};
		
		menu.addEventListener('click',function(){
			mainwin.toggleLeftView();
			mainwin.setCenterhiddenInteractivity("TouchEnabled");
		});
		
		window.containingNav = navwin;
		window.setLeftNavButton(menu);
		window.add(viewLogo);
		window.add(vistaimg);
		vistaimg.add(imgUser);
		scrollView.add(lblUsuario);
		scrollView.add(txtUsuario);
		scrollView.add(separador);
		scrollView.add(lblCorreo);
		scrollView.add(txtCorreo);
		scrollView.add(separador2);
		scrollView.add(lblTelefono);
		scrollView.add(txtTelefono);
		scrollView.add(separador3);
		scrollView.add(lblPlaca);
		scrollView.add(txtPlaca);
		scrollView.add(separador4);
		scrollView.add(lblCerrar);
		scrollView.add(btnCerrar);
		window.add(scrollView);
		
		return navwin;
	}
	
	lblHome.addEventListener('click',function(){
		mapa = require('/interface/mapa');
		winNav = new mapa();
		winNav.open();
	});
	lblHistorial.addEventListener('click',function(){
		historial = require('/interface/historial');
		winNav = new historial();
		winNav.open();
	});
	lblMiPerfil.addEventListener('click',function(){
		mainwin.closeOpenView();
	});
	
	mainwin = NappSlideMenu.createSlideMenuWindow({
		centerWindow: navwin,
		leftWindow:winLeft,
		statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
		leftLedge:100
	});
	
	winLeft.add(logoLeft);
	winLeft.add(lblHome);
	winLeft.add(lblHistorial);
	winLeft.add(lblMiPerfil);
	//winLeft.add(chkDisponible);
	//winLeft.add(lblDisponible);
	
	return mainwin;
};
module.exports = perfil;
