function login(color, _title, _nav){
	var window,titleControl,lblUser,lblPass,txtUser,txtPass,scrollView,btnLogin,loginReq;
	
	titleControl = Ti.UI.createLabel({text: _title,color:'white'});
	window = Ti.UI.createWindow({backgroundImage:'/images/bglogin.jpg',navTintColor:'white',nav: _nav,titleControl:titleControl,title: _title});
	scrollView = Titanium.UI.createScrollView({contentWidth:'auto',contentHeight:'auto',top:0,showVerticalScrollIndicator:true,showHorizontalScrollIndicator:false});
	lblUser = Titanium.UI.createLabel({text:'Usuario',center:0,top:'20%',color:'white'});
	txtUser = Titanium.UI.createTextField({color:'#c2c2c2',hintText:'Introduzca su usuario',center:0,top:'26%',width:'80%',leftButtonPadding:10,borderRadius:6,height:40,value:"Introduzca su usuario",textAlign:'center',clearOnEdit:true});
	lblPass= Titanium.UI.createLabel({text:'Contraseña',center:0,top:'36%',color:'white'});
	txtPass = Titanium.UI.createTextField({hintText:'***********',center:0,top:'42%',color:'#c2c2c2',center:0,width:'80%',height:40,leftButtonPadding:10,borderRadius:6,value:"***********",textAlign:'center',passwordMask:true,clearOnEdit:true});
	btnLogin = Titanium.UI.createButton({title:'Iniciar Sesión',color:'white',borderRadius:30,borderColor:'white',top:'55%',width:'85%',height:60,backgroundColor:'#757575'});
	loginReq = Titanium.Network.createHTTPClient();
	lblreset = Titanium.UI.createLabel({text:'Recuperar Contraseña',color:'#c2c2c2',top:'70%',center:0});
	resetReq = Titanium.Network.createHTTPClient();
	
	btnLogin.addEventListener('click',function(e){
    		if (txtUser.value != '' && txtPass.value != ''){
        			loginReq.open("POST","http://iego.com.mx/webservices/loginChofer.php");
        			params = {
            			username: txtUser.value,
            			password: Ti.Utils.md5HexDigest(txtPass.value)
        			};
        			loginReq.send(params);
    		}else{
        			alert("Usuario y password son requeridos");
    			}
	});
	
	loginReq.onload = function(){
    		json = this.responseText;
    		response = JSON.parse(json);
    		if (response.logged == true){
    			Ti.App.Properties.setInt("id_usuario", response.id_usuario);
    			Ti.App.Properties.setInt("id_chofer", response.id_chofer);
    			Ti.App.Properties.setString("usuario", response.usuario);
    			Ti.App.Properties.setString("email", response.email);
    			Ti.App.Properties.setString("telefono", response.telefono);
    			Ti.App.Properties.setString("fotografia", response.fotografia);
    			Ti.App.Properties.setString("placa", response.placa);
    			Ti.App.Properties.setInt("tipo_servicio", response.tipo_servicio);
    			
    			mapa = require('/interface/mapa');
			winNav = new mapa();
			winNav.open();
			window.close();
    		}else{
        		alert(response.message);
    		}
	};
	
	loginReq.onerror = function(e) {
		Ti.API.debug("STATUS: " + this.status);
		Ti.API.debug("TEXT:   " + this.responseText);
		Ti.API.debug("ERROR:  " + e.error);
		alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Comprueba tu conexión a internet',buttonNames: ['OK']});
    		alertDialog.show();
	};
	
	lblreset.addEventListener('click', function(){
		if (txtUser.value == ''){
			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Se requiere su usuario para restaurar la contraseña',buttonNames: ['OK']});
    			alertDialog.show();
        		
    		}else{
        		resetReq.open('POST','http://iego.com.mx/webservices/sendemail.php');
         		params = {
            		names: txtUser.value
        		};
        		resetReq.send(params);
    		}
	});
	
	resetReq.onload = function(){
    		json = this.responseText;
    	 	response = JSON.parse(json);
    		if (response.mail == true){
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: 'Se le ha enviado un correo para restaurar su contraseña',buttonNames: ['OK']});
    			alertDialog.show();
    		}else{
    			alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
    			alertDialog.show();
    		}
	};
	
	resetReq.onerror = function(event){
    		alert('Error de conexion: ' + JSON.stringify(event));
	};
	
	window.addEventListener('open', function(){
		txtUser.focus();
	});
	
	txtUser.addEventListener('return', function(){
		txtPass.focus();
	});
	
	scrollView.add(lblUser);
	scrollView.add(txtUser);
	scrollView.add(lblPass);
	scrollView.add(txtPass);
	scrollView.add(lblreset);
	scrollView.add(btnLogin);
	
	window.barColor = color;
	window.backButtonTitle = '';
	window.add(scrollView);
	
	return window;
};
module.exports = login;
