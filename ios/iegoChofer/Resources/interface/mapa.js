function mapa(){
	//Variables
 	var window,winLeft,logoLeft,lblHome,lblMiPerfil,lblHistorial,lblDisponible,chkDisponible,latitudOrigen,longitudOrigen,latitudDestino,longitudDestino,obtenerruta,id_user;
  
  	//Modulos
  	var Map = require('ti.map');
  	var NappSlideMenu = require('dk.napp.slidemenu');
  	
  	//Menu lateral
  	winLeft = Ti.UI.createWindow({backgroundImage:'/images/bgmenu.png'});
  	logoLeft = Titanium.UI.createImageView({image:'/images/logo.png',width:'40%',height:'auto',zIndex:2,left:'15%',top:'20%',opacity:1});
  	lblHome = Titanium.UI.createLabel({color:'#cc4a4a',text:'Home',top:'52%',left:'13%'});
  	lblHistorial = Titanium.UI.createLabel({color:'white',text:'Historial',top:'60%',left:'13%'});
  	lblMiPerfil = Titanium.UI.createLabel({color:'white',text:'Mi Perfil',top:'68%',left:'13%'});
  	if (Ti.App.Properties.hasProperty('chkDis')) {
    	chkDisponible = Ti.UI.createSwitch({value:true,top:'77%',onTintColor:'#62D337',tintColor:'#D90019',left:'13%'});
  	}else{
    	chkDisponible = Ti.UI.createSwitch({value:false,top:'77%',onTintColor:'#62D337',tintColor:'#D90019',left:'13%'});
  	}
  	lblDisponible = Titanium.UI.createLabel({color:'white',text:'Disponible',top:'78%',left:'30%'});
  	navwin = createCenterNavWindow();
  	
  	function createCenterNavWindow(){
  		function deviceTokenSuccess(e) {
      		deviceToken = e.deviceToken;
      		var requestParams = {
        		'user_id' : Ti.App.Properties.getInt('id_usuario'),
        		'device_token' : deviceToken,
        		'device_type' : 'iOS'
      		};
      		var url = 'http://iego.com.mx/webservices/registrodispositivo.php';
      		var xhr = Ti.Network.createHTTPClient({
        		timeout: 10000,
        		enableKeepAlive: false
      		});
      		xhr.validateSecureCertificate = false;
      		xhr.onload = function() {
        		try {
         	 		var jsonObject = JSON.parse(this.responseText);
          			if (jsonObject.status == 'success') {
            			Ti.App.Properties.setString('deviceToken', deviceToken);
          			}
        		} catch(e) {
          			Ti.API.error("Caught: " + e);
          			var error_dialog = Ti.UI.createAlertDialog({
            			cancel : 0,
            			buttonNames : ['Ok'],
            			message : e.message,
            			title : 'Error'
          			});
          			error_dialog.show();
        		}
      		};
      		xhr.onerror = function(e) {
        		Ti.API.info(e);
        		var error_dialog = Ti.UI.createAlertDialog({
          			cancel : 0,
          			buttonNames : ['Ok'],
          			message : e.error,
          			title : 'Error'
        		});
        		error_dialog.show();
      		};
      		xhr.open('POST', url);
      		xhr.send(requestParams);
      		Ti.API.info("Push notification device token is: " + deviceToken);
      		Ti.API.info("Push notification types: " + Ti.Network.remoteNotificationTypes);
      		Ti.API.info("Push notification enabled: " + Ti.Network.remoteNotificationsEnabled);
    	}

    	function deviceTokenError(e) {
      		var error_dialog = Ti.UI.createAlertDialog({
        		cancel : 0,
        		buttonNames : ['Ok'],
        		message : 'There was an error registering you device: ' + e.error,
        		title : 'Error'
      		});
      		error_dialog.show();
    	}
    	
    	tc = Ti.UI.createLabel({text:'Mapa principal',color:'#000000'});
    	window = Ti.UI.createWindow({backgroundColor:'white',titleControl:tc});
    	navwin = Titanium.UI.iOS.createNavigationWindow({window: window});
    	menu = Ti.UI.createButton({backgroundImage:'/images/MENU2.png', left:10,zIndex:2,height:23,width:30});
    
    	//Firebase
    	viewWeb= Ti.UI.createView({backgroundColor:'WHITE',visible:false});
    	var mapview;var annotations = [];var annotation;var locationAdded = false;var latitude;var longitude;var bandera = false;var sdial = false;var banderaruta=false;var minutos=0;var sdialC=false;var sdialSA = false;
     	 webview = Ti.UI.createWebView({url : '/firebase/index.html',width:0,height:0,top:0,left:0,visible:false});
     	 webview.addEventListener('load', function() {
      		Ti.Geolocation.getCurrentPosition(function(e) {
        		Ti.App.fireEvent('firebase:init', {
          			latitude : e.coords.latitude,
          			longitude : e.coords.longitude,
          			id_chofer: Ti.App.Properties.getInt('id_chofer')
        		});
      		});
    	});
    
    	//Webservices
    	updateChoferReq = Titanium.Network.createHTTPClient();
      	updateAuReq = Titanium.Network.createHTTPClient();
      	autoUpdateReq = Titanium.Network.createHTTPClient();
      	obtenerruta = Titanium.Network.createHTTPClient();
      	
      	//Alertas
      	nuevaSolicitud = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar','Cancelar'],message: 'Tienes una nueva solicitud de servicio.',title: 'Nuevo servicio'});
    	saldoinsuficiente = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'El servicio no pudo ser procesado, debido a que el usuario no cuenta con saldo suficiente',title: 'Notificación del servicio'});
    	viajecancelado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'El usuario a cancelado el viaje',title: 'Viaje Cancelado'});
    	noaceptado = Ti.UI.createAlertDialog({cancel: 1,buttonNames: ['Aceptar'],message: 'Le recordamos que debe estar pendiente de sus servicios y aceptarlos',title: 'Servicio no aceptado'});
    	dialogPlaca = Ti.UI.createAlertDialog({title: 'Introduzca la placa',style:Ti.UI.iPhone.AlertDialogStyle.PLAIN_TEXT_INPUT,buttonNames: ['Entrar']});
    	
    	//Direccion
    	viewOrigenMain = Ti.UI.createView({width:'100%',visible:false, height:Ti.UI.SIZE,bottom:'25%'});
    	viewText = Ti.UI.createView({width:'77%', borderRadius:5, height:45, layout:'horizontal', left:10, backgroundColor:'white'});
    	viewIconMap = Ti.UI.createView({backgroundColor:'WHITE', width:'16%',borderRadius:5, height:45, right:10, borderColor:'#eaeaea'});
    	leftButtonOrigen = Titanium.UI.createButton({backgroundImage:'/images/circuloverde.png',width:25,height:25,left:5});
    	lblGeoText = Titanium.UI.createLabel({color:'#a4a3a3',text:'Mi Perfil calle rio nuevo numero 104 espejo 2 villahemosa, Tabasco Mexico C.P 86100',textAlign:'left', left:10, height:45});
    	iconMap = Titanium.UI.createButton({backgroundImage:'/images/flecha.png',width:30,height:28});
    	
  		 //Interfaz
    	mapview = Map.createView({mapType: Map.NORMAL_TYPE,animate:true,regionFit:true,userLocation:true,top:0});
    	btnlogin = Titanium.UI.createButton({title:'Ya llegué',color:'white',borderRadius:6,borderColor:'white',bottom:'8%',width:'85%',height:60,backgroundColor:'#a00a1c',visible:false,zIndex:6,opacity:0.1});
    	btnViaje = Titanium.UI.createButton({title:'Iniciar viaje',color:'white',borderRadius:6,borderColor:'white',top:'-50%',width:'85%',height:60,backgroundColor:'#a00a1c',visible:false,zIndex:6,opacity:0.1});
    	btnFinalizar = Titanium.UI.createButton({title:'Finalizar viaje',color:'white',borderRadius:6,borderColor:'white',bottom:'-50%',width:'85%',height:60,backgroundColor:'#a00a1c',visible:false,zIndex:6,opacity:0.1});
    	animatellegue = Titanium.UI.createAnimation();animatellegue.bottom = '8%';animatellegue.duration = 2000;animatellegue.opacity = 1;
    	animatellegueEnd = Titanium.UI.createAnimation();animatellegueEnd.bottom = '-50%';animatellegueEnd.duration = 2000;animatellegueEnd.opacity = 0.1;
    	animateviaje = Titanium.UI.createAnimation();animateviaje.top = '80%';animateviaje.duration = 2000;animateviaje.opacity = 1;
    	animateviajeEnd = Titanium.UI.createAnimation();animateviajeEnd.top = '-50%';animateviajeEnd.duration = 2000;animateviajeEnd.opacity = 0.1;
    	player = Ti.Media.createSound({url:"/sound/sonido.mp3"});
    	
    	//Ventana modal
    	var t = Titanium.UI.create2DMatrix();t = t.scale(0);
		var w = Titanium.UI.createWindow({backgroundColor:'white',borderWidth:5,borderColor:'#999',height:400,width:300,borderRadius:10,opacity:0.92,transform:t});
		var t1 = Titanium.UI.create2DMatrix();t1 = t1.scale(1.1);var a = Titanium.UI.createAnimation();a.transform = t1;a.duration = 200;
		var txtTitulo = Titanium.UI.createLabel({text:"Vehiculo con capacidad de 5 pasajeros",top:'20%', color:'black',center:0,textAlign:'center',width:'80%',font:{fontSize:'25dp'}});
		var txtInfo = Titanium.UI.createLabel({text:"Vehiculo con capacidad de 5 pasajeros",top:'35%', color:'black',center:0,textAlign:'center',width:'80%',font:{fontSize:'20dp'}});
		var btnClose = Titanium.UI.createButton({width:'60%',height:40,title:'Cerrar',textAlign:'center',center:0,bottom:'10%',color:'white',zIndex:3,backgroundColor:'#0080fc',borderRadius:6});
    	
    	var viewPro= Ti.UI.createView({backgroundColor:'WHITE', widht:'100%',height:'100%',zIndex:10,visible:false});
        var display_lbl =  Titanium.UI.createLabel({  text:"00:40",font:{fontSize:'65%'},top:'55%',center:0,color:'#c2c2c2 ',borderRadius:10,textAlign:'center'});
    	var buscando_lbl =  Titanium.UI.createLabel({text:"Trazando ruta...",font:{fontSize:'26%'},top:'40%',center:0,color:'#c2c2c2 ', borderRadius:10,textAlign:'center'});
    	
    	w.add(txtTitulo);
    	w.add(txtInfo);
		w.add(btnClose);
    	
    	viewText.addEventListener('click',function() {
			w.open(a);
		});
		
		btnClose.addEventListener('click',function() {
			var t3 = Titanium.UI.create2DMatrix();
			t3 = t3.scale(0);
			w.close({transform:t3,duration:300});
		});
    	
    	var countDown =  function(m,s,fn_tick,fn_end){
          return {
          total_sec:m*60+s,timer:this.timer,
                  set: function(m,s) {
                    this.total_sec = parseInt(m)*60+parseInt(s);this.time = {m:m,s:s};
                  return this;
              },
              start: function() {
                var self1 = this;
                    this.timer = setInterval( function() {
                      if (self1.total_sec) {
                          self1.total_sec--;
                          self1.time = { m : parseInt(self1.total_sec/60), s: (self1.total_sec%60) };
                          fn_tick();
                      }else {
                          self1.stop();
                          fn_end();
                      }
                    }, 1000 );
                  return this;
              },
              stop: function() {
                clearInterval(this.timer);
                this.time = {m:0,s:0};
                this.total_sec = 0;
                return this;
              }
         };
      };
      
      var my_timer = new countDown(00,40,function() {
        if (my_timer.time.s==0){
              viewPro.visible=false;
              obtenerruta.send();
              btnlogin.visible = true;
              btnlogin.animate(animatellegue);
              viewOrigenMain.visible = true;	
              Titanium.App.Properties.setInt('vistaactiva',0);                   
              display_lbl.text = '0'+my_timer.time.m+":0"+my_timer.time.s;
            }else if (my_timer.time.s<10){    
              display_lbl.text = '0'+my_timer.time.m+":0"+my_timer.time.s;
            }else{
              display_lbl.text = '0'+my_timer.time.m+":"+my_timer.time.s;
            }
       },function() {});
       
       var my_timer2 = new countDown(00,18,function() {
        if (my_timer2.time.s==0){
        	if (Titanium.App.Properties.hasProperty('vistaactiva')) {
        		obtenerruta.send();
              	btnlogin.visible = true;
              	btnlogin.animate(animatellegue);
              	viewOrigenMain.visible = true;	
              	Titanium.App.Properties.setInt('vistaactiva',0);
        	}
        	if (Titanium.App.Properties.hasProperty('viajeiniciado')) {
        		banderaruta=true;
      			btnlogin.animate(animatellegueEnd);
      			btnViaje.visible = true;
      			btnViaje.animate(animateviaje);
        	}
        	if (Titanium.App.Properties.hasProperty('finalizar')) {
        		btnFinalizar.visible = true;
            	btnFinalizar.animate(animatellegue);
        	}
                                 
              display_lbl.text = '0'+my_timer2.time.m+":0"+my_timer2.time.s;
            }else if (my_timer2.time.s<10){    
              display_lbl.text = '0'+my_timer2.time.m+":0"+my_timer2.time.s;
            }else{
              display_lbl.text = '0'+my_timer2.time.m+":"+my_timer2.time.s;
            }
       },function() {});
       
       Titanium.Geolocation.getCurrentPosition(function(e){
        	if (e.error){
            	alert('No se puede obtener ubicacion actual');
            	return;
        	}
        	mapview.region = {latitude:e.coords.latitude,longitude:e.coords.longitude,latitudeDelta:0.009,longitudeDelta:0.009};
    	});
    	var banderaNa =  false;
    	Ti.App.addEventListener('fbController:addMarker', function(e){
      		annotations[e.id] = Map.createAnnotation({
        		driverId : e.id,
        		latitude : e.latitude,
        		longitude : e.longitude,
        		image : '/images/autochofer.png',
        		title : 'Chofer: ' + e.nombreUser
      		});
      		mapview.addAnnotation(annotations[e.id]);
    	});
    	
    	 Ti.App.addEventListener('fbController:moveMarker', function(e){
    	 	if (e.error){
            	alert('Se perdio señal gps');
            	return;
        	}else{
				id_user=e.id_user;
          		annotations[e.id].setLatitude(e.latitude);
        		annotations[e.id].setLongitude(e.longitude);
        		mapview.setRegion({latitude:e.latitude,longitude:e.longitude,latitudeDelta:0.003, longitudeDelta:0.003});
        	}
        	if (e.noaceptado == 1) {
        		banderaNa =true;
        	}else if (e.noaceptado == 0){
        		banderaNa =false;
        	}
        	
        	if (banderaNa == true) {
        		
        	}else if (banderaNa == false){
        		
        	}
        	
        	if(banderaruta==false){
        		txtTitulo.text = 'Origen';
        		txtInfo.text = e.textOrigen;
        		lblGeoText.text = e.textOrigen;
        		Titanium.App.Properties.setInt('latitude',e.latitude);Titanium.App.Properties.setInt('longitude',e.longitude);
              	Titanium.App.Properties.setInt('latOrigenUser',e.latOrigenUser);Titanium.App.Properties.setInt('longiOrigenUser',e.longiOrigenUser);
              	Titanium.App.Properties.setInt('latDestinoUser',e.latDestinoUser);Titanium.App.Properties.setInt('longDestinoUser',e.longDestinoUser);
          		var origin = String(Ti.App.Properties.getInt('latitude') + ',' + Ti.App.Properties.getInt('longitude')),travelMode = 'driving',destination = String(Ti.App.Properties.getInt('latOrigenUser') + ',' + Ti.App.Properties.getInt('longiOrigenUser')),
                url = "https://maps.googleapis.com/maps/api/directions/xml?mode="
                + travelMode + "&origin="
                + origin + "&destination="
                + destination +"&alternatives=false&avoid=indoor&sensor=false";
                Ti.API.info('url:' + url);
                obtenerruta.open('GET',url);
               	latitudOrigen = e.latitude; longitudOrigen = e.longitude; latitudDestino = e.latOrigenUser; longitudDestino = e.longiOrigenUser; 
        	}else{
        		txtTitulo.text = 'Destino';
        		txtInfo.text = e.textDestino;
        		lblGeoText.text = e.textDestino;
        		leftButtonOrigen.backgroundImage = '/images/circulorojo.png';
          		var origin = String(Ti.App.Properties.getInt('latOrigenUser') + ',' + Ti.App.Properties.getInt('longiOrigenUser')),travelMode = 'DRIVING',destination = String(Ti.App.Properties.getInt('latDestinoUser') + ',' + Ti.App.Properties.getInt('longDestinoUser')),
                url = "https://maps.googleapis.com/maps/api/directions/xml?mode="
                + travelMode + "&origin="
                + origin + "&destination="
                + destination +"&alternatives=false&avoid=indoor&sensor=false";
                Ti.API.info('url:' + url);
                obtenerruta.open('GET',url);
                latitudOrigen = e.latOrigenUser; longitudOrigen = e.longiOrigenUser; latitudDestino = e.latDestinoUser; longitudDestino = e.longDestinoUser;
        	}
        	obtenerruta.onload = function(e){
            	var xml = this.responseXML,points = [],steps = xml.documentElement.getElementsByTagName("step"),totalSteps = steps.length;
                for (var i=0; i < totalSteps; i++) {
                	var duration = steps.item(i).getElementsByTagName("duration");
                    durationText = duration.item(0).getElementsByTagName("text").item(0).text;
                    duration = durationText.split(' ');
                    var suma=parseInt(duration[0]);
                    minutos=minutos+suma;
                    var startLocation = steps.item(i).getElementsByTagName("start_location");
                    startLatitude = startLocation.item(0).getElementsByTagName("lat").item(0).text,
                    startLongitude = startLocation.item(0).getElementsByTagName("lng").item(0).text;     
                    points.push({latitude:startLatitude, longitude:startLongitude});                
             	}
              	minutosT=minutos;
          		var finalLocation = steps.item(totalSteps - 1).getElementsByTagName("end_location"),
                finalLatitude = finalLocation.item(0).getElementsByTagName("lat").item(0).text,
                finalLongitude = finalLocation.item(0).getElementsByTagName("lng").item(0).text;     
              	points.push({latitude:finalLatitude, longitude:finalLongitude});
             	route = Map.createRoute({name:"bonVoyage",points:points,color:"#319df0",width:6}), 
               	startAnnotation = Map.createAnnotation({image:'/images/marcador.png',latitude: points[0].latitude,longitude: points[0].longitude,title: 'Posición actual'}),
             	endAnnotation = Map.createAnnotation({image:'/images/bandera.png',latitude: points[points.length - 1].latitude,longitude: points[points.length - 1].longitude,title: 'Destino'}); 
            	mapview.addRoute(route);
            	mapview.addAnnotation(startAnnotation);
             	mapview.addAnnotation(endAnnotation); 
          		Ti.Platform.openURL('http://maps.apple.com/maps?saddr=' + latitudOrigen + ',' + longitudOrigen +'&daddr='+ latitudDestino + ',' + longitudDestino + '&dirflg=d');
          		viewIconMap.addEventListener('click', function(){
          			Ti.Platform.openURL('http://maps.apple.com/maps?saddr=' + latitudOrigen + ',' + longitudOrigen +'&daddr='+ latitudDestino + ',' + longitudDestino + '&dirflg=d');
          		}); 
            };
    	 });
    	 
    	 function receivePush(e) {
      		Ti.API.info("push" + JSON.stringify(e));
      		var payload = e.data.payload;
      		var json = JSON.stringify(e);
      		if (payload.statussolicitud == 1 && payload.saldoinsuficiente == 0) {
      			if (Ti.App.Properties.hasProperty('finalizar') || Ti.App.Properties.hasProperty('viajeiniciado') || Ti.App.Properties.hasProperty('vistaactiva')) {
      				
      			}else{
      				if (banderaNa == false) {
      					nuevaSolicitud.show();
        				Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
        				player.play();	
      				}else{
      					
      				}
      			}
      		}else if (payload.viajecancelado == 1) {
      			my_timer.stop();
        		sdialC =true;
        		viajecancelado.show();
        		Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
        		player.play();
      		}else if (payload.saldoinsuficiente == 1 && payload.statussolicitud == 0) {
        		my_timer.stop();
        		sdialSA = true;
        		saldoinsuficiente.show();
        		Titanium.Media.audioSessionCategory = Ti.Media.AUDIO_SESSION_CATEGORY_PLAYBACK;
        		player.play();
      		}else if (payload.noaceptado == 1){
      			banderaNa = true;
        		nuevaSolicitud.hide();
        		noaceptado.show();
      		}
      		Ti.App.iOS.cancelAllLocalNotifications();
    	}
    	
    	noaceptado.addEventListener('click', function(){
    		banderaNa = false;
       		Ti.App.fireEvent('fbController:noaceptado', {
          		id:Ti.App.Properties.getInt('id_chofer')
          	});
      	});
    
    	saldoinsuficiente.addEventListener('click', function(){
      		my_timer.stop();
      		player.stop();
      		Ti.App.fireEvent('fbController:saldoagotado', {
        		id:Ti.App.Properties.getInt('id_chofer')
      		});
      		banderaruta=false;
          	sdial =  false;
          	sdialC = false;
          	sdialSA = false;
          	btnlogin.visible = false;
          	btnViaje.visible = false;
          	btnFinalizar.visible = false;
        	btnlogin.animate(animatellegueEnd);
        	viewOrigenMain.visible = false;	
        	viewPro.visible=false;
        	my_timer.set(00,40);
        	display_lbl.text="00:40";
    	});
    	
    	nuevaSolicitud.addEventListener('click', function(ed){
      		if (ed.index == 0) {
      			if (banderaNa == false) {
      				var track = Ti.UI.createView({ width: "80%", height: "2%", borderRadius:10, backgroundColor: '#c9ecfd',top:'50%'});
          			var progress = Ti.UI.createView({ borderRadius:10, left: 0, width: '100%', height: "100%", backgroundColor : '#007fff'});
        			track.add(progress); 
          			viewPro.add(track);
        			player.stop();
        			viewPro.visible=true;
        			sdial = true;
        			Ti.App.fireEvent('fbController:servicioaceptado', {
          				id:Ti.App.Properties.getInt('id_chofer')
        			});
            		progress.animate({ width: 0, duration: 40000 });
            		my_timer.start(); 
      			}else{
      				
      			}
      		}else{
         		Ti.App.fireEvent('fbController:solicitud', {
         			id:Ti.App.Properties.getInt('id_chofer')
         		});
      		}
    	});
    	
    	 btnlogin.addEventListener('click',function(){
    	 	Titanium.App.Properties.setInt('viajeiniciado',0); 
    	 	Titanium.App.Properties.removeProperty('vistaactiva');
      		minutos=0;
          	Ti.App.fireEvent('fbController:yallegue',{
        		id:Ti.App.Properties.getInt('id_chofer')
      		});
      		banderaruta=true;
      		btnlogin.animate(animatellegueEnd);
      		btnViaje.visible = true;
      		btnViaje.animate(animateviaje);
      		mapview.removeRoute(route);
      		mapview.removeAnnotation(startAnnotation);
      		mapview.removeAnnotation(endAnnotation);   
    	});
    	
    	btnViaje.addEventListener('click',function(){
    		Titanium.App.Properties.removeProperty('viajeiniciado');
    	 	Titanium.App.Properties.setInt('finalizar',0); 
      		obtenerruta.send();
            btnViaje.animate(animateviajeEnd);
            btnFinalizar.visible = true;
            btnFinalizar.animate(animatellegue);
            Ti.App.fireEvent('fbController:iniciarViaje', {
        		id:Ti.App.Properties.getInt('id_chofer')
      		});    
    	});
    	
    	 btnFinalizar.addEventListener('click',function(){
    	 	viewOrigenMain.visible = false;	
    	 	Ti.App.fireEvent('fbController:finalizarviaje', {
        		id:Ti.App.Properties.getInt('id_chofer'),
        		id_user:id_user,    
      		});
       		Ti.App.fireEvent('fbController:remover', {
        		id:Ti.App.Properties.getInt('id_chofer')
      		});
    	 	Titanium.App.Properties.removeProperty('finalizar');
      		minutos=0;
            banderaruta=false;
            sdial =  false;
            sdialC = false; sdialSA = false;
            btnlogin.visible = false;
            btnViaje.visible = false;
            btnFinalizar.visible = false;
            btnFinalizar.animate(animatellegueEnd);
            mapview.removeRoute(route);
      		mapview.removeAnnotation(startAnnotation);
      		mapview.removeAnnotation(endAnnotation);
      		my_timer.set(00,40);
    	});
    	
    	viajecancelado.addEventListener('click', function(ed){
        		my_timer.stop();
      			player.stop();
          		Ti.App.fireEvent('fbController:serviciocancelado', {
          			id:Ti.App.Properties.getInt('id_chofer')
        		});
          		banderaruta=false;
            	sdial =  false;
            	sdialC = false;
           	 	btnlogin.visible = false;
            	btnViaje.visible = false;
            	btnFinalizar.visible = false;
          		btnlogin.animate(animatellegueEnd);
          		if(display_lbl.text=="00:00"){
          			mapview.removeRoute(route);
        			mapview.removeAnnotation(startAnnotation);
        			mapview.removeAnnotation(endAnnotation);  	
          		}
        		viewPro.visible=false;
        		my_timer.set(00,40);
        		display_lbl.text="00:40";
      	});
    	
    	window.addEventListener('open',function(){
      		if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
        		Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
          			Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
          			Ti.Network.registerForPushNotifications({
            			success : deviceTokenSuccess,
            			error : deviceTokenError,
            			callback : receivePush
          			});
        		});

        		Ti.App.iOS.registerUserNotificationSettings({
          			types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
        		});
      		}else {
        		Ti.Network.registerForPushNotifications({
        			types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
        			success : deviceTokenSuccess,
        			error : deviceTokenError,
        			callback : receivePush
        		});
      		}
      		Ti.App.iOS.registerUserNotificationSettings({
	    		types: [
            		Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
            		Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
            		Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
        		]
    		});
      		gpsStart();
      		if (Ti.App.Properties.getInt('vistaactiva')==0) {
      			sdial = true;
      			my_timer2.start();
      		}
      		if (Ti.App.Properties.getInt('viajeiniciado')==0) {
      			my_timer2.start();
      		}
      		if (Ti.App.Properties.getInt('finalizar')==0) {
      			my_timer2.start();
      		}
    	});
  		chkDisponible.addEventListener('change',function(e){
      	if (chkDisponible.value == true) {
        	Ti.App.Properties.setInt("chkDis", Ti.App.Properties.getInt('id_chofer'));
        	dialogPlaca.show();
        	Ti.App.fireEvent('fbController:disponibilidad',{
          		dis:1,
          		id:Ti.App.Properties.getInt('id_chofer')
        	});
        	Ti.Geolocation.getCurrentPosition(function(eggm) {
          		Ti.App.fireEvent('fbController:login',{
            		id:Ti.App.Properties.getInt('id_chofer'),
            		nomuser:Ti.App.Properties.getString('usuario')
          		});
          		updateChoferReq.open("POST","http://iego.com.mx/webservices/updatechofer.php");
                params = {
              		id_chofer:Ti.App.Properties.getInt('id_chofer'),
                    latitud:eggm.coords.latitude,
                    longitud:eggm.coords.longitude
             	};
              	updateChoferReq.send(params);
            });
            updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
           		params = {
                	id_chofer:Ti.App.Properties.getInt('id_chofer'),
                    placa: Ti.App.Properties.getString("placa"),
                    status_login:1,
                    status_ocupado:0,
                    status_solicitud:0,
                    status_disponibilidad:1      
               	};
                updateAuReq.send(params);
      	}else if (chkDisponible.value == false){
      		Ti.Geolocation.getCurrentPosition(function(eggm) {
      			Ti.App.fireEvent('fbController:coordsChofer',{
        			id:Ti.App.Properties.getInt('id_chofer'),
        			latIn:eggm.coords.latitude,
        			lgnIn:eggm.coords.longitude
      			});
      		});
        	Titanium.App.Properties.removeProperty('chkDis');
        	Ti.App.fireEvent('fbController:disponibilidad',{
          		dis:0,
          		id:Ti.App.Properties.getInt('id_chofer')
        	});
        	updateAuReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
            params = {
            	id_chofer:Ti.App.Properties.getInt('id_chofer'),
                placa: Ti.App.Properties.getString("placa"),
                status_login:1,
                status_ocupado:0,
                status_solicitud:0,
                status_disponibilidad:0          
         	};
            updateAuReq.send(params);
      	}
    });
    
    dialogPlaca.addEventListener('click', function(e){
      autoUpdateReq.open("POST","http://iego.com.mx/webservices/updateauto.php");
      params = {
      		id_chofer:Ti.App.Properties.getInt('id_chofer'),
        	placa:e.text,
        	status_login:1,
        	status_ocupado:0,
        	status_solicitud:0,
        	status_disponibilidad:1,
    	};
       	autoUpdateReq.send(params);
      });
      autoUpdateReq.onload = function(){
            json = this.responseText;
            response = JSON.parse(json);
            if (response.logged == true){
                Ti.App.Properties.setString('placa',response.placa);
                Ti.App.Properties.setInt("tipo_servicio", response.tipo_servicio);
                alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
            alertDialog.show();
            alertDialog.addEventListener('click',function(e){
              Ti.App.fireEvent('fbController:tiposervicio',{
            id:Ti.App.Properties.getInt('id_chofer'),
            tiposervicio:Ti.App.Properties.getInt('tipo_servicio')
          });
                    mainwin.toggleLeftView();
          mainwin.setCenterhiddenInteractivity("TouchEnabled");
                });
            }else{
            	alertDialog = Titanium.UI.createAlertDialog({title: 'Mensaje',message: response.message,buttonNames: ['OK']});
            	alertDialog.show();
            	alertDialog.addEventListener('click',function(e){
            		dialogPlaca.show();
          		});
            }
      };
      
      var handleLocation = function(e) {
      		if (!e.success || e.error){
        		Ti.API.info("Code Error: " + JSON.stringify(e.error));  
        		return;
      		}
      		var longitude = e.coords.longitude;
      		var latitude = e.coords.latitude;
      		var timeStamp = String(new Date().getTime());
      		Ti.App.fireEvent('fbController:login',{
            	id:Ti.App.Properties.getInt('id_chofer'),
            	nomuser:Ti.App.Properties.getString('usuario'),
            	timestamp:timeStamp,
            	id_usuario:Ti.App.Properties.getInt('id_usuario')
      		});
      
      		Ti.App.fireEvent('fbController:coordsChofer',{
        		id:Ti.App.Properties.getInt('id_chofer'),
        		latIn:latitude,
        		lgnIn:longitude
      		});
      		if (chkDisponible.value == true) {
        		Ti.App.fireEvent('fbController:disponibilidad',{
          			dis:1,
          			id:Ti.App.Properties.getInt('id_chofer')
        		});
        		Ti.App.fireEvent('fbController:coordsChofer',{
          			id:Ti.App.Properties.getInt('id_chofer'),
          			latIn:latitude,
          			lgnIn:longitude
        		});
      		}else if (chkDisponible.value == false) {
        		Ti.App.fireEvent('fbController:disponibilidad',{
          			dis:0,
          			id:Ti.App.Properties.getInt('id_chofer')
        		});
      		}
    	};
    
    	function gpsStart() {
          	if (Ti.Geolocation.locationServicesEnabled) {
              	Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_BEST;
              	Ti.Geolocation.distanceFilter = 10;
              	Ti.Geolocation.preferredProvider = Ti.Geolocation.PROVIDER_GPS;
              	if (!locationAdded) {
                  	Ti.Geolocation.addEventListener('location', handleLocation);
                  	locationAdded = true;
              	}
          	}else {
           		alert('Please enable location services');
       		}
    	}
    
    	Ti.App.addEventListener('close', function(e) {  
      		Ti.App.fireEvent('fbController:disponibilidad',{dis:0,id:Ti.App.Properties.getInt('id_chofer')});
      		Ti.App.fireEvent('fbController:removerGeo',{id:Ti.App.Properties.getInt('id_chofer')});
    	});
    
    	menu.addEventListener('click',function(){
      		mainwin.toggleLeftView();
      		mainwin.setCenterhiddenInteractivity("TouchEnabled");
    	});
    
    	viewPro.add(display_lbl);
      	viewPro.add(buscando_lbl);   
      	window.add(viewPro);  
    
    	window.containingNav = navwin;
    	window.setLeftNavButton(menu);
    	window.add(btnlogin);
    	window.add(btnViaje);
    	window.add(btnFinalizar);
    	window.add(mapview);
    	viewWeb.add(webview);
    	window.add(viewWeb);
    	window.add(viewOrigenMain);
    	
    	viewOrigenMain.add(viewText);
    	viewOrigenMain.add(viewIconMap);
    	
    	viewText.add(leftButtonOrigen);
    	viewText.add(lblGeoText);
    	viewIconMap.add(iconMap);
    
    	return navwin;
  	}
  
  	lblHome.addEventListener('click',function(){
    	mainwin.closeOpenView();
  	});
  
  	lblHistorial.addEventListener('click',function(){
    	var viewChildren = viewWeb.children;
    	for (var i=0; i < viewChildren.length; i++) {
      		var child = viewChildren[i];
      		viewWeb.remove(child);  
    	}
    	historial = require('/interface/historial');
    	winNav = new historial();
    	winNav.open();
  	});
  	
  	lblMiPerfil.addEventListener('click',function(){
    	var viewChildren = viewWeb.children;
    	for (var i=0; i < viewChildren.length; i++) {
      		var child = viewChildren[i];
      		viewWeb.remove(child);  
    	}
    	perfil = require('/interface/perfil');
    	winNav = new perfil();
    	winNav.open();
  	});
  
  	mainwin = NappSlideMenu.createSlideMenuWindow({
    	centerWindow: navwin,
    	leftWindow:winLeft,
    	statusBarStyle: NappSlideMenu.STATUSBAR_WHITE,
    	leftLedge:100
  	});
  
  	winLeft.add(logoLeft);
  	winLeft.add(lblHome);
  	winLeft.add(lblHistorial);
  	winLeft.add(lblMiPerfil);
  	winLeft.add(chkDisponible);
  	winLeft.add(lblDisponible);
	return mainwin;
};
module.exports = mapa;